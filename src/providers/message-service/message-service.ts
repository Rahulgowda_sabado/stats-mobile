import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { Globals } from "../../app/Globals";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Headers, Http, Response, RequestOptions, Jsonp } from "@angular/http";
import { HTTP, HTTPResponse } from "@ionic-native/http";
//import { Http } from '@angular/http';
//import { ConfigUrl } from '../utils/app-config';
import { AlertController, LoadingController } from "ionic-angular";

@Injectable()
export class MessageServiceProvider {
  j: string;
  globals: Globals;
  dataa: any;
  local_authorisation: string;
  noramlData: any;
  httpOptions: any;
  authorization_val: any;
  refresh_token: any;
  constructor(
    public loading: LoadingController,
    private httpcl: HttpClient,
    public http: Http,
    public storage: Storage,
    private httpNative: HTTP
  ) {
    this.globals = Globals.getInstance();
    this.storage.get(this.globals.authorization_token).then(authorization => {
      this.authorization_val = authorization;
      console.log("authorization_val ID at Final" + this.authorization_val);
    });
    this.storage.get(this.globals.refresh_token).then(refresh => {
      this.refresh_token = refresh;
      console.log("Refresh ID at Final" + this.refresh_token);
    });
  }

  httpGetRequest(url: string) {
    return this.http.get(url).map(res => res.json());
  }

  httpPostRequest(url: string, data: string) {
    return this.http.post(url, data).map(res => res.json());
  }

  //Generate Refresh token incase if session is expired
  generateRefreshToken() {
    //alert("old refresh token:"+this.globals.getRefresh())
    console.log("old refresh token" + this.globals.getRefresh());

    let Url = "https://fssfed.stage.ge.com/fss/as/token.oauth2?";
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append("grant_type", "refresh_token");
    urlSearchParams.append("refresh_token", this.globals.getRefresh());
    urlSearchParams.append("client_id", "STATS_STAGE_APP");
    urlSearchParams.append(
      "client_secret",
      "68d82ebba1c4545bbb42edfba019c3b40394a7c1"
    );
    urlSearchParams.append(
      "redirect_uri",
      "http://stage.stats.ge.com/redirect"
    );

    // let Url  = "https://fssfed.ge.com/fss/as/token.oauth2?";
    // let urlSearchParams = new URLSearchParams();
    // urlSearchParams.append('grant_type', 'refresh_token');
    // urlSearchParams.append('refresh_token', this.globals.getRefresh());
    // urlSearchParams.append('client_id', "STATS");
    // urlSearchParams.append('client_secret', "860b08bbb8ab7249bcf250a6e45a884ff2aab422");
    // urlSearchParams.append('redirect_uri', 'http://stats.ge.com/redirect');

    //   let body = urlSearchParams.toString();
    Url = Url + urlSearchParams.toString();
    console.log("before SERIALIZE");
    //alert("before SERIALIZE");
    //this.httpNative.setDataSerializer("json");
    console.log("url:" + Url);
    //alert("Url at refresh"+Url);
    return new Promise((resolve, reject) => {
      //alert('hii');

      this.httpNative
        .post(Url, {}, {})
        .then((response: HTTPResponse) => {
          //alert("generating new access token with the refresh token");
          let data: any = response.data;
          console.log("Json String" + data);
          //alert("Json String"+data);
          var body = JSON.parse(data);
          console.log("json data  inside" + JSON.stringify(data));
          //alert("new access token:"+body.access_token);
          //alert("new access token:"+body.refresh_token);
          this.storage.set("authorization_token", body.access_token);
          this.storage.set("refresh_token", body.refresh_token);
          this.globals.setAccess(body.access_token);
          this.globals.setRefresh(body.refresh_token);
          console.log("new aceess token" + this.globals.getAccess());
          console.log("new referesh token" + body.refresh_token);
          //alert("new aceess token"+this.globals.getAccess());
          //alert("new referesh token"+this.globals.getRefresh());
          //return body;
          resolve(body);
        })
        .catch((error: any) => {
          console.log(JSON.stringify(error));
          alert("Sorry Your Refresh token got expirred Please login again");
          console.log(
            "Sorry Your Refresh token got expirred Please login again"
          );
          console.log("error at refresh token generation");
          //return(error)
          reject(error);
        });
    });
  }
  notifCount(sso_id: string) {
    var url = this.globals.getUrl() + "alerts";
    var data = JSON.stringify({ sso_id: sso_id });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(JSON.stringify(data));
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    console.log("access token " + this.globals.getAccess());
    console.log("inside notif header" + headers);
    console.log("inside notif header" + JSON.stringify(headers));
    //console.log("http headers"+JSON.stringify(this.httpOptions));
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //alert(response.data)
        console.log(" u nitish:" + JSON.stringify(response.data));
        return response.data;
      })
      .catch(error => {
        //alert("mess error"+JSON.stringify(error));
        console.log("inside catch at msg" + JSON.stringify(error));
        return error;
      });
  }

  login(value: { uname: string; pswd: string }) {
    var url = this.globals.getUrl() + "login";
    var data = JSON.stringify({ sso_id: value.uname });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    //let headers = { 'Accept': 'application/json' , 'Authorization':  bearer };
    let headers = { "Content-Type": "application/json" };
    this.httpNative.setDataSerializer("json");

    //alert(url);
    //alert( JSON.stringify(body));
    //alert(JSON.stringify(headers));

    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, {})
      .then((response: HTTPResponse) => {
        //alert(response.data)
        //var body = JSON.parse(response.data);
        // alert("body"+body)
        return response.data;
      })
      .catch(error => {
        //alert('nitish');
        //alert(JSON.stringify(error));
        return error;
      });
  }

  notifDetails(sso_id: string) {
    var url = this.globals.getUrl() + "alerts_list";
    //var sso=sso_id
    var data = JSON.stringify({ sso_id: sso_id });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    console.log("access token " + this.globals.getAccess());
    console.log("inside notif header" + headers);
    console.log("inside notif header" + JSON.stringify(headers));
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //alert(response.data)
        //var body = JSON.parse(response.data);

        return response.data;
      })
      .catch(error => {
        //alert(JSON.stringify(error));
        return error;
      });
  }

  shipmentRequest(sso_id: string) {
    // var url = this.globals.getUrl() + "shipment";
    var url = "assets/shipment_request.json";
    return this.httpGetRequest(url);
  }

  feMyOrderList(sso_id: string, country_id: string) {
    var url = this.globals.getUrl() + "open_orderM";
    var data = JSON.stringify({ sso_id: sso_id, country_id: country_id });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(data);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        //alert(JSON.stringify(error));
        return error;
      });
  }

  dueInReturn(sso_id: string, country_id: string) {
    var url = this.globals.getUrl() + "crossed_return_date_ordersM";
    var data = JSON.stringify({ sso_id: sso_id, country_id: country_id });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(data);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // var body = JSON.parse(response.data);

        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  dueInDelivery(sso_id: string, country_id: string) {
    var url = this.globals.getUrl() + "crossed_expected_delivery_dateM";
    var data = JSON.stringify({ sso_id: sso_id, country_id: country_id });
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(data);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  adminNotification(sso_id: string, alertType: string) {
    console.log("inisde admin notification");
    var url = "";
    if (alertType == "5") {
      url = "assets/due_tool_return.json";
    } else if (alertType == "6") {
      url = "assets/due_deliveries.json";
    }

    console.log(this.httpGetRequest(url));
    return this.httpGetRequest(url);
  }

  getToolsList(
    sso_id: string,
    transactionCountry: string,
    query: string,
    segment: number
  ) {
    console.log("segement ");
    var url = this.globals.getUrl() + "search_tools";
    var data = JSON.stringify({
      sso_id: sso_id,
      transactionCountry: transactionCountry,
      search_tools: query,
      segment: segment
    });
    console.log(data);
    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        //alert(JSON.stringify(error));
        return error;
      });
  }

  confirmTools(jsonData) {
    var url = this.globals.getUrl() + "confirm_tools";

    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  orderTools(jsonData) {
    var url = this.globals.getUrl() + "submit_order";

    var body = JSON.parse(jsonData);
    // console.log(" json data"+JSON.stringify(jsonData));
    // console.log("data"+JSON.stringify(jsonData));
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //alert(response.data)
        // return response.data;
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getCustomerAvailability(custId, ssoId, country_id) {
    var url = this.globals.getUrl() + "chackSystemIDAvailability";
    var jsonData = JSON.stringify({
      system_id: custId,
      transactionUser: ssoId,
      transactionCountry: country_id
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //return response.data;
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getCustomerList() {
    var url = "assets/customer.json";
    return this.httpGetRequest(url);
  }

  getWarehouseDet() {
    var url = "assets/warehouse.json";

    return this.httpGetRequest(url);
  }

  getOrderIdAvailability(
    orderNum,
    transactionUser,
    transactionCountry,
    transactionPage,
    itemIdWithQty
  ) {
    var url = this.globals.getUrl() + "check_order_number_availability";
    var jsonData = JSON.stringify({
      order_number: orderNum,
      transactionUser: transactionUser,
      transactionCountry: transactionCountry,
      transactionPage: transactionPage,
      itemIdWithQty: itemIdWithQty
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getReturnOrderIdAvailability(
    orderNum,
    toolOrderId,
    ownedAssetCount,
    oahId,
    oahConditionId,
    fe1OrderNumber,
    returnTypeId
  ) {
    var url = this.globals.getUrl() + "check_return_order_number_availability";
    var jsonData = JSON.stringify({
      order_number: orderNum,
      tool_order_id: toolOrderId,
      owned_assets_count: ownedAssetCount,
      oah_id: oahId,
      oah_condition_id: oahConditionId,
      fe1_order_number: fe1OrderNumber,
      return_type_id: returnTypeId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getCustomerAddress(custId, transactionCountry) {
    var url = this.globals.getUrl() + "getAddressByID";
    var jsonData = JSON.stringify({
      system_id: custId,
      transactionCountry: transactionCountry
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getWarehouseAddress(wareHouseId) {
    var url = this.globals.getUrl() + "getWarehouseAddress";
    var jsonData = JSON.stringify({ fe_to_wh_id: wareHouseId });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getAcknowledgmentRequest(ssoId, alertType, country_id) {
    console.log("alertType ==> " + alertType);
    var url = this.globals.getUrl() + "receive_orderM";
    var jsonData = JSON.stringify({
      sso_id: ssoId,
      alert_type: alertType,
      country_id: country_id
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //  const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  acknowledgeRequestAddressForWHAlert(ssoId, alertType, toolOrderId) {
    var url = this.globals.getUrl() + "ack_address";
    var jsonData = JSON.stringify({
      sso_id: ssoId,
      alert_type: alertType,
      tool_order_id: toolOrderId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  acknowledgeRequestAddressForFEAlert(alertType, rtoID, ssoId) {
    var url = this.globals.getUrl() + "ack_address";
    var jsonData = JSON.stringify({
      alert_type: alertType,
      rto_id: rtoID,
      sso_id: ssoId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getAckRequest(rtoID, jsonObj) {
    console.log("inside getAck request");
    var url = this.globals.getUrl() + "insert_fe2_fe_receiveM";
    if (rtoID == null) {
      url = this.globals.getUrl() + "insert_fe_received_orderM";
    }
    var jsonData: string = JSON.stringify(jsonObj);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(rtoID);
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getExtendDateData(ssoId, country_id) {
    var url = this.globals.getUrl() + "exceededOrderDurationM";
    var jsonData = JSON.stringify({ sso_id: ssoId, country_id: country_id });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  extendDateApproval(jsonData) {
    var url = this.globals.getUrl() + "submitexceededOrderDurationM";

    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  addressChange(ssoId, country_id) {
    var url = this.globals.getUrl() + "address_changeM";
    var jsonData = JSON.stringify({ sso_id: ssoId, country_id: country_id });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getReturnTools(ssoId, alertType, country_id) {
    var url = this.globals.getUrl() + "raise_pickupM";
    var jsonData = JSON.stringify({
      sso_id: ssoId,
      alert_type: alertType,
      country_id: country_id
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getReturnToolInitialize(ssoId, toolOrderId) {
    var url = this.globals.getUrl() + "owned_order_detailsM";
    var jsonData = JSON.stringify({
      tool_order_id: toolOrderId,
      sso_id: ssoId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getAcknowlegeToolInitialize(ssoId, toolOrderId, rtoId) {
    console.log("Inside get Acknowledge Tool Intitialize rtoId=>" + rtoId);
    var url = this.globals.getUrl() + "receive_order_detailsM";

    var jsonData = JSON.stringify({
      tool_order_id: toolOrderId,
      sso_id: ssoId
    });
    //var jsonData = JSON.stringify({ sso_id: ssoId });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    if (rtoId != null) {
      //alert('inside if')
      url = this.globals.getUrl() + "fe2_fe_receive_detailsM";
      jsonData = JSON.stringify({ rto_id: rtoId, sso_id: ssoId });
      // var jsonData = JSON.stringify({ sso_id: ssoId });
      var body = JSON.parse(jsonData);
      let bearer = "Bearer " + this.globals.getAccess();
      let headers = { Accept: "application/json", Authorization: bearer };
      this.httpNative.setDataSerializer("json");
    }
    //alert('Url '+url);
    //alert('Body '+JSON.stringify(body));
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //alert(JSON.stringify(response.data));
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  addressUpdate(jsonData, addressUrl) {
    var url = this.globals.getUrl() + addressUrl;
    console.log(url);

    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  stockTransferList(ssoId, countryId) {
    var url = this.globals.getUrl() + "raiseSTforOrderM";
    var jsonData = JSON.stringify({ sso_id: ssoId, country_id: countryId });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //  alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  refreshForStockTransferOrder(sso_id, tool_id) {
    var url = this.globals.getUrl() + "checkToolsAvailabilityAgainM";
    console.log("sso_id=>" + sso_id + " and tool_id=>" + tool_id);
    var jsonData = JSON.stringify({
      tool_order_id: tool_id,
      sso_id: sso_id,
      device_type: this.globals.deviceName
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log(jsonData);
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        //  alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  stockOrderDetForOrders(ssoId: string, toolOrderId: string) {
    var url = this.globals.getUrl() + "assignSTtoolsForOrderM";
    var jsonData = JSON.stringify({
      tool_order_id: toolOrderId,
      sso_id: ssoId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    console.log(jsonData);
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  stockTransfersForOrdersPost(data) {
    var url = this.globals.getUrl() + "insertAssignedSTtoolsM";
    var jsonData = JSON.stringify(data);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log("************");
    console.log(jsonData);
    console.log("************");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  partialStockOrderDetForOrders(ssoId: string, toolOrderId: string) {
    var url = this.globals.getUrl() + "initiatePartialShipmentM";
    var jsonData = JSON.stringify({
      tool_order_id: toolOrderId,
      sso_id: ssoId
    });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    console.log(jsonData);
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  partialStransfersForOrdersPost(data) {
    var url = this.globals.getUrl() + "insertPartiallyAssignedSTtoolsM";
    var jsonData = JSON.stringify(data);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log("************");
    console.log(jsonData);
    console.log("************");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  stockWHToWH(ssoId: string, toolOrderId: string) {
    var url = this.globals.getUrl() + "approveOrRejectSTM";
    var jsonData = JSON.stringify({
      tool_order_id: toolOrderId,
      sso_id: ssoId
    });
    console.log(jsonData);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  stockTransferWHToWHPost(data) {
    console.log("stock transfer wh ");
    console.log(data);
    var url = this.globals.getUrl() + "submitApproveOrRejectSTM";

    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  returnToolRequest(data) {
    var url = this.globals.getUrl() + "insert_fe_owned_orderM";
    var jsonData = JSON.stringify(data);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log("************");
    console.log(jsonData);
    console.log("************");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        //  alert(JSON.stringify(error));
        return error;
      });
  }

  acknowledgeRequestValue(data, rtoId, order_type) {
    console.log("***rto**************");

    console.log(rtoId);
    console.log("***rto**************");
    if (order_type == 1)
      var url = this.globals.getUrl() + "insert_st_fe_received_orderM";
    else var url = this.globals.getUrl() + "insert_fe_received_orderM";
    if (rtoId != null) {
      url = this.globals.getUrl() + "insert_fe2_fe_receiveM";
    }
    var jsonData = JSON.stringify(data);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    console.log("************");
    console.log(jsonData);
    console.log("************");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  feToFeTransfer(ssoId: string, country_id: string) {
    var url = this.globals.getUrl() + "fe2_fe_approvalM";
    var jsonData = JSON.stringify({ sso_id: ssoId, country_id: country_id });
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    console.log(jsonData);
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  feToFeTransferPost(data) {
    console.log("Before parsing");
    console.log(data);
    var url = this.globals.getUrl() + "insert_fe2_fe_approvalM";

    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getPendingLogisticsAlertRequest(jsonData, alertType) {
    var url = this.globals.getUrl() + "shipment_req_list";
    console.log(alertType);
    console.log(jsonData + " " + url);

    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  extendDate(jsonData) {
    var url = this.globals.getUrl() + "submitDaysExtensionRequestM";
    console.log(jsonData + " " + url);

    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        // var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getPendingFeRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_fe_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
  getPendingStRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_st_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
  getPendingPickupRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_pickup_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
  getPendingCalibrationRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_calibration_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
  getPendingRepairRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_repair_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getPendingAckFeToolRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_ack_fe_tool_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
  getPendingAckWhtoolRequest(jsonData) {
    var url = this.globals.getUrl() + "pending_ack_wh_tool_requestsM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  calRequiredTools(sso_id: string, country_id) {
    var url = this.globals.getUrl() + "calibration_required_toolsM";
    var data = JSON.stringify({ sso_id: sso_id, country_id: country_id });

    var body = JSON.parse(data);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getScannedData(ssoId, assetNumber, countryId) {
    //var url = this.globals.getUrl() + "owned_order_detailsM";
    var url = this.globals.getUrl() + "scan_asset";
    var jsonData = JSON.stringify({
      sso_id: ssoId,
      asset_number: assetNumber,
      country_id: countryId
    });
    var body = JSON.parse(jsonData);
    console.log("the body of the message service");
    console.log(body);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        var i = response.data;
        console.log("the respone of the message service");
        console.log(i);
        return i;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  get_inventory_tools_availabilityM(tool_id, transactionUser, country_id) {
    //var url = this.globals.getUrl() + "owned_order_detailsM";
    var url = this.globals.getUrl() + "get_inventory_tools_availabilityM";
    var jsonData = JSON.stringify({
      tool_id: tool_id,
      transactionUser: transactionUser,
      transactionCountry: country_id
    });
    var body = JSON.parse(jsonData);
    console.log("the body of the message service");
    console.log(body);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        var i = response.data;
        console.log("the respone of the message service");
        console.log(i);
        return i;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  get_countries_by_sso_idM(jsonData) {
    var url = this.globals.getUrl() + "get_countries_by_sso_idM";
    console.log(jsonData + " " + url);
    var body = JSON.parse(jsonData);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        return response.data;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getFeOrderList(ssoId, countryId, itemId) {
    var url = this.globals.getUrl() + "ajax_toolsAvailabilityWithFEM";
    var jsonData = JSON.stringify({
      itemIdWithQty: itemId,
      transactionUser: ssoId,
      transactionCountry: countryId
    });
    var body = JSON.parse(jsonData);
    console.log("the body of the message service");
    console.log(body);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        var i = response.data;
        console.log("the respone of the message service");
        console.log(i);
        return i;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }

  getFeOrderListForReturn(tool_order_id, oah_id) {
    var url = this.globals.getUrl() + "ajax_toolsNeededByFEM";
    var jsonData = JSON.stringify({
      tool_order_id: tool_order_id,
      oah_id: oah_id,
    });
    var body = JSON.parse(jsonData);
    console.log("the body of the message service");
    console.log(body);
    let bearer = "Bearer " + this.globals.getAccess();
    let headers = { Accept: "application/json", Authorization: bearer };
    this.httpNative.setDataSerializer("json");
    // const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.globals.getAccess());
    return this.httpNative
      .post(url, body, headers)
      .then((response: HTTPResponse) => {
        // alert(response.data)
        //var body = JSON.parse(response.data);
        var i = response.data;
        console.log("the respone of the message service");
        console.log(i);
        return i;
      })
      .catch(error => {
        // alert(JSON.stringify(error));
        return error;
      });
  }
}
