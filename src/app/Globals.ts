import { NavController } from "ionic-angular";
export class Globals {
  static instance: Globals;
  static isCreating: Boolean = false;
  //private url: string = "http://125.99.157.182/stats_mobile_new/index.php/";

  // private url: string = "http://13.126.121.68/statsv2_mobile/index.php/";
  // private url: string = "http://localhost/statsv2_enhancements/index.php/";

  // private url: string = "https://stage.api.ge.com/health/stats/";
  // private url: string = "https://api.ge.com/health/stats/";
  //private url: string = "http://13.126.121.68/statsv2_mobile/index.php/";
  //private url: string = "http://localhost/stats-mobile-services/index.php/";
  private url: string = "http://13.126.121.68/statsv2_mobile/index.php/";

  private roles: string[];
  public fe_id: string[];
  public logistics_id: string[];
  public admin_id: string[];
  private field_engineer;
  private zonal;
  private logistics;
  private admin;

  private admin_sub_menu;
  private fe_image_path;
  private zonal_image_path;
  private logistic_image_path;
  private admin_path;
  private super_admin_path;

  private admin_headers = [];
  private notifCount: string;
  private access_global_set: string;
  private refresh_global_set: string;
  public selectedTools: any = [];

  public role_id = "role_id";
  public sso_id = "sso_id";
  public country_id = "country_id";
  public sso_name = "sso_name";
  public authorization_token = "authorization_token";
  public refresh_token = "refresh_token";
  //public authorization_header = 'anthorzation_header';
  public navCtrl: NavController;
  public transaction_country: any;
  public deviceName = "";


  constructor() {
    if (!Globals.isCreating) {
      throw new Error("Error in Globals");
    }
    this.roles = ["1", "2", "3", "4", "5", "6"];
    this.fe_id = ["2", "5", "6"];
    this.logistics_id = ["3"];
    this.admin_id = ["1", "4"];

    this.zonal_image_path = "assets/img/zonal.png";
    this.fe_image_path = "assets/img/fe.png";
    this.logistic_image_path = "assets/img/lo.jpg";
    this.admin_path = "assets/img/admin.jpg";
    this.super_admin_path = "assets/img/super_admin.jpg";


    //this.field_engineer = [{id:"1", img:"assets/img/log-50.svg", title:"Tool Order"}, {id:"2", img:"assets/img/log-52.svg", title:"Acknowledgements"}, {id:"3", img:"assets/img/log-51.svg", title:"Tools Return"}, {id:"4", img:"assets/img/log-53.svg", title:"My order"}];
    this.field_engineer = [
      { id: "1", img: "assets/img/log-50.svg", title: "Tool Order" },
      { id: "2", img: "assets/img/log-52.svg", title: "Acknowledgements" },
      { id: "3", img: "assets/img/log-51.svg", title: "Tools Return" },
      { id: "4", img: "assets/img/log-53.svg", title: "My Order" },
      { id: "6", img: "assets/img/log-55.svg", title: "SCAN" }
    ];

    this.zonal = [
      { id: "1", img: "assets/img/log-50.svg", title: "Tool Order" },
      { id: "2", img: "assets/img/log-52.svg", title: "Acknowledgements" },
      { id: "3", img: "assets/img/log-51.svg", title: "Tools Return" },
      { id: "4", img: "assets/img/log-53.svg", title: "My Order" },
      { id: "6", img: "assets/img/log-55.svg", title: "SCAN" }
    ];
    this.logistics = [
      { id: "5", img: "assets/img/log-46.svg", title: "Work List" }
    ];
    this.admin = [
      { id: "7", img: "assets/img/log-54.svg", title: "Approvals" }
    ];
    this.admin_sub_menu = [
      {
        id: "admin_1",
        img: "assets/img/icon-63.svg",
        title: "Stock Transfer Request"
      },
      {
        id: "admin_2",
        img: "assets/img/icon-62.svg",
        title: "Address Change Request"
      },
      {
        id: "admin_3",
        img: "assets/img/icon-61.svg",
        title: "Extended date Approvals"
      },
      {
        id: "admin_4",
        img: "assets/img/icon-60.svg",
        title: "FE to FE Transfer Request"
      }
    ];
  }

  static getInstance(): Globals {
    if (Globals.instance == null) {
      Globals.isCreating = true;
      Globals.instance = new Globals();
      Globals.isCreating = false;
    }
    return Globals.instance;
  }

  setNotifCount(count: string) {
    this.notifCount = count;
  }

  getNotifCount(): string {
    return this.notifCount;
  }

  getUrl(): string {
    return this.url;
  }

  getRoleIds(): string[] {
    return this.roles;
  }

  getAdminSubMenu() {
    return this.admin_sub_menu;
  }

  getImagePath(role: string) {
    if (this.fe_id.indexOf(role) > -1) {
      //if(role)
      // console.log("feild engineer");
      if (role == "5") return this.zonal_image_path;
      else return this.fe_image_path;
      // return this.fe_image_path;
    } else if (this.admin_id.indexOf(role) > -1) {
      if (role == "1")
        return this.admin_path
      else
        return this.super_admin_path;

    } else {
      console.log("logistics");
      return this.logistic_image_path;
    }
  }

  getRoleByRoleId(role: string) {
    if (this.fe_id.indexOf(role) > -1) {
      console.log("feild engineer");
      return "fe";
    } else if (this.admin_id.indexOf(role) > -1) {
      console.log("admin");
      return "admin";
    } else {
      console.log("logistics");
      return "log";
    }
  }

  getMenuItems(role: string): string[] {
    if (this.fe_id.indexOf(role) > -1) {
      if (role == "5") return this.zonal;
      else return this.field_engineer;

      //return this.field_engineer;
    } else if (this.admin_id.indexOf(role) > -1) {
      return this.admin;
    } else {
      return this.logistics;
    }
  }

  getAdminHeaderValue(alertType: number) {
    switch (alertType) {
      case 5:
        this.admin_headers = [
          "Order No",
          "Tool Desc",
          "Asset No",
          "Warehouse",
          "Type"
        ];
        break;
      case 6:
        this.admin_headers = ["Order No", "FE Name", "Exceeded Date", "Type"];
        break;
    }
    return this.admin_headers;
  }

  setAccess(access_global_set_par: string) {
    this.access_global_set = access_global_set_par;
  }
  getAccess(): string {
    return this.access_global_set;
  }
  setRefresh(access_global_set_par: string) {
    this.refresh_global_set = access_global_set_par;
  }
  getRefresh(): string {
    return this.refresh_global_set;
  }
  getAccessExpiryCode() {
    return "1012116 - Invalid token.";
  }
}
