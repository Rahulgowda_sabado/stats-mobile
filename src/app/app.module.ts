import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from "@ionic/storage";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { NgxQRCodeModule } from "ngx-qrcode2";
import { ScancodePage } from "../pages/scancode/scancode";
import { Toast } from "@ionic-native/toast";
import { ScanToolPage } from "../pages/scan-tool/scan-tool";
import { ScannedToolReturnPage } from "../pages/scanned-tool-return/scanned-tool-return";
import { HttpClientModule } from "@angular/common/http";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { Storage } from "@ionic/storage";
import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { MenuPage } from "../pages/menu/menu";
import { HeaderPage } from "../pages/header/header";
import { ShippingPage } from "../pages/shipping/shipping";
import { AcknowledgementPage } from "../pages/acknowledgement/acknowledgement";
import { NotificationPage } from "../pages/notification/notification";
import { FeordersPage } from "../pages/feorders/feorders";
import { FemyordersPage } from "../pages/femyorders/femyorders";
import { ShoppinglistPage } from "../pages/shoppinglist/shoppinglist";
import { OrderToolsPage } from "../pages/order-tools/order-tools";
import { AdminApprovalPage } from "../pages/admin-approval/admin-approval";
import { AddressChangePage } from "../pages/address-change/address-change";
import { FereturntoolPage } from "../pages/fereturntool/fereturntool";
import { FetoolreturntoolPage } from "../pages/fetoolreturntool/fetoolreturntool";
import { FedateextendPage } from "../pages/fedateextend/fedateextend";
import { StockTransferPage } from "../pages/stock-transfer/stock-transfer";
import { StockOrderDetailsPage } from "../pages/stock-order-details/stock-order-details";
import { FeToFeTransferPage } from "../pages/fe-to-fe-transfer/fe-to-fe-transfer";
import { MessageServiceProvider } from "../providers/message-service/message-service";
import { ReturnToolPage } from "../pages/return-tool/return-tool";
import { ExtendDatePage } from "../pages/extend-date/extend-date";
import { ExtendedDateApprovalPage } from "../pages/extended-date-approval/extended-date-approval";
import { WtwStockTransferPage } from "../pages/wtw-stock-transfer/wtw-stock-transfer";
import { DueReturnPage } from "../pages/due-return/due-return";
import { calRequiredTools } from "../pages/cal-required-tools/cal-required-tools";
import { DueInDeliveriesPage } from "../pages/due-in-deliveries/due-in-deliveries";
import { AckRequestFinalPage } from "../pages/ack-request-final/ack-request-final";
import { HTTP } from "@ionic-native/http";
import { LogisticsAlertPage } from "../pages/logistics-alert/logistics-alert";
import { LogFeReq } from "../pages/log-fe-req/log-fe-req";
import { LogStReq } from "../pages/log-st-req/log-st-req";
import { LogPickupReq } from "../pages/log-pickup-req/log-pickup-req";
import { LogCalibrationReq } from "../pages/log-calibration-req/log-calibration-req";
import { LogRepairReq } from "../pages/log-repair-req/log-repair-req";
import { LogAckFeToolReq } from "../pages/log-ackfetool-req/log-ackfetool-req";
import { LogAckWhToolReq } from "../pages/log-ackwhtool-req/log-ackwhtool-req";
import { PartialStockOrderDetailsPage } from "../pages/partial-stock-order-details/partial-stock-order-details";
import { ToolAvailabiltiyInWhPage } from "../pages/tool-availabiltiy-in-wh/tool-availabiltiy-in-wh";
import { TransactionCountryPage } from "../pages/transaction-country/transaction-country";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenuPage,
    HeaderPage,
    ShippingPage,
    AcknowledgementPage,
    NotificationPage,
    FeordersPage,
    ShoppinglistPage,
    OrderToolsPage,
    FemyordersPage,
    AdminApprovalPage,
    AddressChangePage,
    FereturntoolPage,
    FetoolreturntoolPage,
    FedateextendPage,
    StockTransferPage,
    StockOrderDetailsPage,
    FeToFeTransferPage,
    ExtendedDateApprovalPage,
    ReturnToolPage,
    ExtendDatePage,
    WtwStockTransferPage,
    DueReturnPage,
    calRequiredTools,
    DueInDeliveriesPage,
    AckRequestFinalPage,
    LogisticsAlertPage,
    LogFeReq,
    LogStReq,
    LogPickupReq,
    LogCalibrationReq,
    LogRepairReq,
    LogAckFeToolReq,
    LogAckWhToolReq,

    ScancodePage,

    ScanToolPage,
    ScannedToolReturnPage,
    PartialStockOrderDetailsPage,
    ToolAvailabiltiyInWhPage,
    TransactionCountryPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MenuPage,
    HeaderPage,
    ShippingPage,
    AcknowledgementPage,
    NotificationPage,
    FeordersPage,
    ShoppinglistPage,
    OrderToolsPage,
    FemyordersPage,
    AdminApprovalPage,
    AddressChangePage,
    FereturntoolPage,
    FetoolreturntoolPage,
    FedateextendPage,
    StockTransferPage,
    StockOrderDetailsPage,
    FeToFeTransferPage,
    ExtendedDateApprovalPage,
    ReturnToolPage,
    ExtendDatePage,
    WtwStockTransferPage,
    DueReturnPage,
    calRequiredTools,
    DueInDeliveriesPage,
    AckRequestFinalPage,
    LogisticsAlertPage,
    LogFeReq,
    LogStReq,
    LogPickupReq,
    LogCalibrationReq,
    LogRepairReq,
    LogAckFeToolReq,
    LogAckWhToolReq,

    ScancodePage,
    ScanToolPage,
    ScannedToolReturnPage,
    PartialStockOrderDetailsPage,
    ToolAvailabiltiyInWhPage,
    TransactionCountryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    MessageServiceProvider,
    IonicStorageModule,
    BarcodeScanner,
    InAppBrowser,
    HTTP
  ]
})
export class AppModule {}
