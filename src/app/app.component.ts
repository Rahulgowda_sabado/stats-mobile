import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController, AlertController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuPage } from '../pages/menu/menu';
import { DueReturnPage } from '../pages/due-return/due-return';
import { StockTransferPage } from "../pages/stock-transfer/stock-transfer";
import { AdminApprovalPage } from "../pages/admin-approval/admin-approval";
import { StockOrderDetailsPage } from "../pages/stock-order-details/stock-order-details";
import { FeordersPage } from "../pages/feorders/feorders";
import { FemyordersPage } from "../pages/femyorders/femyorders";
import { AcknowledgementPage } from "../pages/acknowledgement/acknowledgement";
import { NotificationPage } from '../pages/notification/notification';
import { FereturntoolPage } from "../pages/fereturntool/fereturntool";
import { AddressChangePage } from '../pages/address-change/address-change';
import { FeToFeTransferPage } from '../pages/fe-to-fe-transfer/fe-to-fe-transfer';
import { ExtendedDateApprovalPage } from '../pages/extended-date-approval/extended-date-approval';
import { ReturnToolPage } from '../pages/return-tool/return-tool';
import { Globals } from '../app/Globals';
import { HomePage } from '../pages/home/home';
import { ScancodePage } from '../pages/scancode/scancode';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  globals: Globals;
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Nav) navCon: NavController;
  rootPage: any = HomePage;
  // rootPage:any = ScancodePage;


  pages1: Array<{ title: string, component: any, icon: string }>;
  pages2: Array<{ title: string, component: any, icon: string }>;
  pages3: Array<{ title: string, component: any, icon: string }>;


  constructor(public alertCtrl: AlertController, public menuCtrl: MenuController, private storage: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.globals = Globals.getInstance();
    this.initializeApp();
    this.pages1 = [
      //{ title: 'Quote', component: QuotePage, icon: "attach" },
      { title: 'Home', component: HomePage, icon: "md-home" },
      { title: 'Stock Transfer Request', component: StockTransferPage, icon: "appname-customicon6" },
      { title: 'Address Change Request', component: AddressChangePage, icon: "appname-customicon7" },
      { title: 'Extended date Approvals', component: ExtendedDateApprovalPage, icon: "appname-customicon8" },
      { title: 'FE to FE Transfer Request', component: FeToFeTransferPage, icon: "appname-customicon9" }

    ];

    this.pages2 = [
      //{ title: 'Quote', component: QuotePage, icon: "attach" },
      { title: 'Home', component: HomePage, icon: "md-home" },
      { title: 'Tool Order', component: FeordersPage, icon: "appname-customicon1" },
      { title: 'Acknowledgements', component: AcknowledgementPage, icon: "appname-customicon2" },
      { title: 'Tools Return', component: FereturntoolPage, icon: "appname-customicon3" },
      { title: 'My order', component: FemyordersPage, icon: "appname-customicon4" },
      { title: 'Scan', component: ScancodePage, icon: "appname-customicon10" }

    ];


    this.pages3 = [
      //{ title: 'Quote', component: QuotePage, icon: "attach" },
      { title: 'Home', component: HomePage, icon: "md-home" },
      { title: 'Work List', component: NotificationPage, icon: "appname-customicon5" },
    ]

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.styleDefault();
      this.statusBar.styleBlackOpaque();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logOut() {
    let alert = this.alertCtrl.create({
      title: 'Log Out',
      message: 'Do you really want to log out?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.menuCtrl.close();
            console.log("Cancel");
          }
        },
        {
          text: 'Sure',
          handler: () => {
            console.log("Sure");
            this.menuCtrl.close();
            this.storage.remove(this.globals.role_id);
            this.storage.remove(this.globals.sso_id);
            this.navCon.setRoot(HomePage);
          }
        }
      ]
    });
    alert.present();
  }
}
