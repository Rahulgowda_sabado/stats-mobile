import { Component } from "@angular/core";
import { NavController, Platform, LoadingController } from "ionic-angular";
import { MenuPage } from "../menu/menu";
import { Storage } from "@ionic/storage";
import { Globals } from "../../app/Globals";

import { InAppBrowser } from "@ionic-native/in-app-browser";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse
} from "@angular/common/http";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { HTTP, HTTPResponse } from "@ionic-native/http";

declare var cordova: any;

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  events = [];
  post_globals: any;
  access: string;
  refresh: string;
  pswdText: string = "password";
  globals: Globals;
  access_variable: any;
  authorization_message: any = "";
  error_type: any;
  loading: any;
  //showUserDetails: boolean = false;
  menu_display: boolean = false;

  // hard_coded_role = '2';
  // hard_coded_sso_id = '502654354';
  // hard_coded_sso_name = 'Santosh';
  // hard_coded_access_token ='gk0ijHVD9HGvHsmCLU6vl7dyawri' ;
  // hard_coded_refresh_token = 'FnB7ndoZVhuTAuqFFTzHYcyU8WjF1C1cHZ4MqC4btc';
  userInfo = {};
  //loginUrl : string= "https://fssfed.stage.ge.com/fss/as/authorization.oauth2?response_type=code&client_id=STATS_STAGE_APP&grant_type=authorization_code&scope=api+profile+openid&redirect_uri=http://stage.stats.ge.com/redirect";
  loginUrl: string =
    "https://fssfed.ge.com/fss/as/authorization.oauth2?response_type=code&client_id=STATS&grant_type=authorization_code&scope=api+profile+openid&redirect_uri=http://stats.ge.com/redirect";

  constructor(
    public storage: Storage,
    public iab: InAppBrowser,
    private http: HttpClient,
    public loadingCtrl: LoadingController,
    private httpNative: HTTP,
    private platform: Platform,
    public messageService: MessageServiceProvider,
    public navCtrl: NavController
  ) {
    this.platform = platform;
    this.globals = Globals.getInstance();

    if (this.platform.is("ios")) {
      // This will only print when on iOS
      console.log("I am an iOS device!");
      this.globals.deviceName = "2";
      //alert("ios");
    } else if (this.platform.is("android")) {
      // This will only print when on Android
      console.log("I am an android device!");
      //alert("android");
      this.globals.deviceName = "1";
    } else if (this.platform.is("browser")) {
      console.log("I am an browser device!");
      this.globals.deviceName = "0";
    }
    this.menu_display = false;
    // this.http = http;
    // this.storage.set('role_id',this.hard_coded_role);
    // this.storage.set('sso_id',this.hard_coded_sso_id);
    // this.storage.set('sso_name',this.hard_coded_sso_name);
    // this.storage.set('authorization_token',this.hard_coded_access_token);
    // this.storage.set('refresh_token',this.hard_coded_refresh_token);
    console.log("kiii");
    this.globals = Globals.getInstance();
    //console.log("Globals =>"+this.globals);
    console.log("global role ID=>" + this.globals.role_id);
    this.storage.get(this.globals.role_id).then(role_id => {
      //alert("role id =>"+role_id);

      console.log("role id at landing screen =>" + role_id);
      //  if(role_id == null ){
      //   console.log("login function called");
      //   this.login();

      // }else{
      //   // this.loading = this.loadingCtrl.create({
      //   //   content:'Please wait...'
      //   // });
      //   // this.loading.present();
      //   this.userInfo = "User Logged In";
      //   this.gotoMenu();
      // }

      if (role_id != null) this.gotoMenu();
    });
  }

  public login() {
    this.geLogin().then(
      success => {
        //alert("Inside ge login alert");
        // alert("ge login Success=>"+success.access_token);
        // console.log("Ge login success entire data=>"+success);
        // console.log(JSON.stringify(success));
        // console.log("globals in ge login"+this.globals);
        // console.log("globals json"+JSON.stringify(this.globals));
        if (success === 1) {
          console.log("navigating to error screen");
          this.error_type = 3;
          this.gotoErrorScreen();
        } else {
          console.log("local  at ge login access" + success[0]["access_token"]);
          console.log(
            "local at ge login refresh" + success[0]["refresh_token"]
          );
          if (
            success[0]["role_id"] == 1 ||
            (success[0]["role_id"] == 2 && success[0]['dealer_check'] ==0) ||
            success[0]["role_id"] == 3 ||
            success[0]["role_id"] == 4 ||
            success[0]["role_id"] == 5
          ) {
            this.storage.set("role_id", success[0]["role_id"]);
            this.storage.set("sso_id", success[0]["sso_id"]);
            this.storage.set("sso_name", success[0]["name"]);
            this.storage.set("country_id", success[0]["country_id"]);
            this.storage.set("authorization_token", success[0]["access_token"]);
            this.storage.set("refresh_token", success[0]["refresh_token"]);
            this.globals.setAccess(success[0]["access_token"]);
            this.globals.setRefresh(success[0]["refresh_token"]);

            this.userInfo = success;
            this.gotoMenu();
          } else {
            this.error_type = 4;
            this.gotoErrorScreen();
          }
        }
      },
      error => {
        this.error_type = 1;
        this.gotoErrorScreen();
        //alert("error at ge login after sso"+error);
      }
    );
  }

  public geLogin(): Promise<any> {
    var self = this;
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();

      // let params = new HttpParams()
      // .set('client_id', "STATS_STAGE_APP")
      // .set("grant_type","authorization_code")
      // .set("redirect_uri", "http://stage.stats.ge.com/redirect")
      // .set("response_type",'code')
      // .set("scope", "api profile openid");

      let params = new HttpParams()
        .set("client_id", "STATS")
        .set("grant_type", "authorization_code")
        .set("redirect_uri", "http://stats.ge.com/redirect")
        .set("response_type", "code")
        .set("scope", "api profile openid");

      //var authUrl = "https://fssfed.stage.ge.com/fss/as/authorization.oauth2?" + params;

      var authUrl =
        "https://fssfed.ge.com/fss/as/authorization.oauth2?" + params;

      console.log(authUrl);
      var options =
        "location=no,clearcache=yes,clearsessioncache=yes,toolbar=yes";
      var browserRespAvailable = false;

      //Wait till the cordova platform is ready before triggering the inappbrowser plugin
      this.platform.ready().then(() => {
        if (typeof cordova !== "undefined") {
          let inAppBrowserRef = new InAppBrowser().create(
            authUrl,
            "_blank",
            options
          );
          console.log("*******inAppBrower****");
          console.log(inAppBrowserRef);
          console.log("*******inAppBrower****");

          //Capture code field from browser url on loadstart event
          inAppBrowserRef.on("loadstart").subscribe(event => {
            var url = "" + event.url + "";
            var code = url.match(/\?code=(.+)$/);
            var error = url.match(/\?error=(.+)$/);
            //alert("at before demand for code"); // maruthi final uncomment code
            //alert(code); // maruthi final uncomment code
            //if code is available demand for authorization token
            if (code) {
              //alert(code);// maruthi final uncomment code

              // let Url = "https://fssfed.stage.ge.com/fss/as/token.oauth2?";
              // let urlSearchParams = new URLSearchParams();
              // urlSearchParams.append('code', code[1]);
              // urlSearchParams.append('client_id', "STATS_STAGE_APP");
              // urlSearchParams.append('client_secret', "68d82ebba1c4545bbb42edfba019c3b40394a7c1");
              // urlSearchParams.append('redirect_uri', "http://stage.stats.ge.com/redirect");
              // urlSearchParams.append('grant_type', 'authorization_code');

              let Url = "https://fssfed.ge.com/fss/as/token.oauth2?";
              let urlSearchParams = new URLSearchParams();
              urlSearchParams.append("code", code[1]);
              urlSearchParams.append("client_id", "STATS");
              urlSearchParams.append(
                "client_secret",
                "860b08bbb8ab7249bcf250a6e45a884ff2aab422"
              );
              urlSearchParams.append(
                "redirect_uri",
                "http://stats.ge.com/redirect"
              );
              urlSearchParams.append("grant_type", "authorization_code");

              // var body = JSON.parse(urlSearchParams.toString());
              //let bearer = 'Bearer ' + this.globals.getAccess();
              console.log("code" + code[1]);
              //let headers = { 'Accept': 'application/json' , 'Authorization':  code[1] };

              //let body = urlSearchParams.toString();
              // alert(body);// maruthi final uncomment code
              // alert(Url);// maruthi final uncomment code
              console.log("Url at ligin" + Url);
              //console.log("Url at ligin"+body);
              Url = Url + urlSearchParams.toString();
              this.httpNative.setDataSerializer("json");
              //const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
              console.log("Url at ligin" + JSON.stringify(headers));
              this.httpNative.post(Url, {}, {}).then(
                (response: HTTPResponse) => {
                  //   alert("succees")  ;
                  // let data: any = response;
                  let data: any = response.data;

                  console.log("Json String" + data);
                  // alert("Json String - "+data);
                  console.log("before parse -" + data);
                  var body = JSON.parse(data);
                  console.log("after parse -" + body);
                  console.log("before stringfy -" + body);
                  //  var res=JSON.stringify(body);
                  // console.log("after stringfy -"+res);

                  // console.log(data);
                  // alert("before fetching the sso details");
                  //  alert(body);// maruthi final uncomment code
                  console.log("Globals are=>" + this.globals);

                  fetchUserSSODetails(body);
                },
                error => {
                  this.error_type = 1;
                  //alert("inside error block");
                  reject("error");
                }
              );
            } else if (error) {
              reject("error");
            }

            // Close the inappbrowser window if code is available in url
            if (code != null || error != null) {
              browserRespAvailable = true;
              inAppBrowserRef.close();
            }
          });

          //reopen the inappbrowser instance if user clicks on 'DONE' button
          /*inAppBrowserRef.on('exit').subscribe((event) => {
                if (!browserRespAvailable) {
                    reject("validateLoginCondition");
                }
            });*/
          //end if
        }
      });

      //Fetch user details once we get authorization token
      function fetchUserSSODetails(tokens) {
        let new_token = tokens;

        let headers = {
          Accept: "application/json",
          Authorization: "Bearer " + new_token.access_token
        };

        //let Url = "https://fssfed.stage.ge.com/fss/idp/userinfo.openid";

        let Url = "https://fssfed.ge.com/fss/idp/userinfo.openid";

        self.httpNative.setDataSerializer("json");
        //alert('httpNATIVE');
        self.httpNative
          .post(Url, {}, headers)
          .then((response: HTTPResponse) => {
            //alert("response after ")
            // alert()
            let data1: any = response.data;
            let data = JSON.parse(data1);
            // alert(data.sub);
            //alert('2');

            //const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + tokens.access_token);

            //let UrlLogin = "https://stage.api.ge.com/health/stats/login";

            let UrlLogin = "https://api.ge.com/health/stats/login";

            let bodyData = JSON.stringify({ sso_id: data.sub });
            var body = JSON.parse(bodyData);
            //alert('body' + JSON.stringify(body));

            //let bearer = 'Bearer ' + this.globals.getAccess();
            let headers = {
              Accept: "application/json",
              Authorization: "Bearer " + new_token.access_token
            };
            // alert('header'+JSON.stringify(headers));
            self.httpNative.setDataSerializer("json");
            //alert('self native');
            console.log(JSON.stringify(headers));

            self.httpNative
              .post(UrlLogin, body, headers)
              .then((response: HTTPResponse) => {
                //alert('3');
                console.log("respponse data" + response);
                //  alert("response inside stats url"+response);
                let new_response = JSON.parse(response.data);
                // let new_response = JSON.stringify(new_response1);
                var y = new_response.hasOwnProperty("invalid_token");
                console.log("y value " + y);
                if (y) {
                  // don't have the access to stats
                  console.log("invalid ");
                  var invalid_token = 1;
                  resolve(invalid_token);
                } else {
                  //alert("Inside else block");

                  console.log(
                    "user info before push",
                    +JSON.stringify(new_response)
                  );

                  console.log("access before push Token" + tokens.access_token);
                  console.log(
                    "refresh Token before push" + tokens.refresh_token
                  );
                  new_response[0]["access_token"] = tokens.access_token;
                  new_response[0]["refresh_token"] = tokens.refresh_token;
                  console.log(
                    "After pushing new response Respponse" +
                      JSON.stringify(new_response)
                  );
                  //alert("alert new response" + JSON.stringify(new_response) );
                  resolve(new_response);
                }
              })
              .catch((e: any) => {
                //alert("hii"+JSON.stringify(e));
                //alert('4');

                console.log("Response from STATS URL" + JSON.stringify(e));
              });
          })
          .catch((e: any) => {
            this.error_type = 2;
            //alert("inside stats url error block");
            reject(JSON.stringify(e));
          });
      }
    });
  }

  gotoMenu() {
    //alert("inside goto menu"+JSON.stringify(this.userInfo));
    //this.loading.dismiss();
    this.navCtrl.setRoot(MenuPage, { userInfo: this.userInfo });
  }
  gotoErrorScreen() {
    console.log(
      "Sorry! You are not the STATS User.Please Contact Administrator"
    );
    if (this.error_type == 1)
      this.authorization_message =
        "Sorry! Error at GE Login.Please Contact Administrator";
    else if (this.error_type == 2)
      this.authorization_message =
        "Sorry! Error at STATS Login.Please Contact Administrator";
    else if (this.error_type == 3)
      this.authorization_message =
        "Sorry! You are not the STATS User.Please Contact Administrator";
    else if (this.error_type == 4)
      this.authorization_message =
        "Sorry! Your Role don't have the access to STATS application.Please Contact Administrator or login with SA/Admin/FE/Logistic Roles";
    else this.authorization_message = "please wait....";
    this.menu_display = true;
    //this.loading.dismiss();
  }

  signIn(value: { uname: string; pswd: string }) {
    this.messageService.login(value).then(data => {
      var user: any = data;
      console.log("user" + JSON.stringify(user));
      console.log(
        "user" + JSON.stringify(user.hasOwnProperty("invalid_token"))
      );
      if (user.hasOwnProperty("invalid_token")) {
        alert("Sorry! You don't have access.Please contact administrator");
        this.navCtrl.setRoot(HomePage);
      } else {
        var res = JSON.parse(data);
        if (res.length == undefined || res.length == "undefined") {
          alert(
            "Sorry! Your Role don't have access.Please contact administrator"
          );
          this.navCtrl.setRoot(HomePage);
        } else {
          var user = res[0];
          if (
            user.role_id == 1 ||
            (user.role_id == 2 && user.dealer_check ==0) ||
            user.role_id == 3 ||
            user.role_id == 4 ||
            user.role_id == 5
          ) {
            console.log(user.sso_id + " " + user.name + " " + user.role_id);
            this.userInfo = user;
            console.log("user info");
            console.log(this.userInfo);
            if (this.globals.getRoleIds().indexOf(user.role_id) > -1) {
              console.log("Login successful");
              this.storage.set(this.globals.role_id, user.role_id);
              this.storage.set(this.globals.sso_id, user.sso_id);
              this.storage.set(this.globals.country_id, user.country_id);
              this.storage.set("sso_name", user.name);
              this.storage.set("authorization_token", "1");
              this.storage.set("refresh_token", "1");
              this.globals.setAccess("1");
              this.globals.setRefresh("1");
              this.gotoMenu();
            }
          } else {
            alert(
              "Sorry! Your Role don't have access.Please contact administrator"
            );
            this.navCtrl.setRoot(HomePage);
          }
        }
      }
    });
  }

  LoginCicked() {
    this.login();
  }
  onIconClick() {
    if (this.pswdText == "password") {
      this.pswdText = "text";
    } else {
      this.pswdText = "password";
    }
  }
}
