import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { calRequiredTools } from './cal-required-tools';

@NgModule({
  declarations: [
    calRequiredTools,
  ],
  imports: [
    IonicPageModule.forChild(calRequiredTools),
  ],
})
export class DueReturnPageModule {}
