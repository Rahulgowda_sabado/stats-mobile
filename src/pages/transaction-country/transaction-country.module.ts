import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionCountryPage } from './transaction-country';

@NgModule({
  declarations: [
    TransactionCountryPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionCountryPage),
  ],
})
export class TransactionCountryPageModule {}
