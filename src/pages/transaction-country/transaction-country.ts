import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Globals } from "../../app/Globals";
import { OrderToolsPage } from "../order-tools/order-tools";
import { FeordersPage } from "../feorders/feorders";

@Component({
  selector: "page-transaction-country",
  templateUrl: "transaction-country.html"
})
export class TransactionCountryPage {
  countryList: any = [];
  check: any;
  error_msg: any = "";
  country: any;
  enable: any = false;
  country1: any;
  globals: Globals;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.globals = Globals.getInstance();

    this.countryList = this.navParams.data;

    // this.country = this.countryList[0].name;
    // this.country1 = this.countryList[0];
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad TransactionCountryPage");
  }

  getCountry(eve) {
    console.log(eve);
    this.enable = true;

    this.error_msg = "";
    this.country = eve;
  }

  submit() {
    if (this.country == "" || this.country == undefined) {
      this.error_msg = "Please select Country";
    } else {
      for (let a of this.countryList) {
        if (a.name == this.country) {
          this.globals.transaction_country = a;
        }
      }

      this.error_msg = "";

      this.navCtrl.push(FeordersPage);
    }
  }
}
