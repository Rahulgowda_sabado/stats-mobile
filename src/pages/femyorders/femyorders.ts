import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { Globals } from "../../app/Globals";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-femyorders",
  templateUrl: "femyorders.html"
})
export class FemyordersPage {
  shownGroup = null;
  globals: Globals;
  orders = [];
  ssoId = "";
  pageType: string;
  responseData: any;
  country_id = "";
  countries_list = "";
  countries_list_data = "";
  itemExist = true;
  loading: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    /* this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id = country_id;
    }); */
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.orderData();
    });
  }

  orderData() {
    this.messageService
      .feMyOrderList(this.ssoId, this.country_id)
      .then(data => {
        // var res =  JSON.stringify(data);
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.responseData = JSON.parse(data);
        this.countries_list = this.responseData.countries_list;
        this.countries_list_data = this.responseData.countries_list_data;
        console.log(" data fro my orders");
        console.log(data);
        console.log(" data fro my orders");

        //Converting the yyyy-mm-dd format to dd-mm-yyyy
        /*  var datePipe = new DatePipe("en-US");
        for (var i = 0; i < this.responseData.final_data.length; i++) {
          this.responseData.final_data[
            i
          ].order_info.return_date = datePipe.transform(
            this.responseData.final_data[i].order_info.return_date,
            "dd-MM-yyyy"
          );
          this.responseData.final_data[
            i
          ].order_info.deploy_date = datePipe.transform(
            this.responseData.final_data[i].order_info.deploy_date,
            "dd-MM-yyyy"
          );
        } */

        this.orders = this.responseData.final_data;
        if (this.orders.length == 0) {
          this.itemExist = false;
        }
        this.loading.dismiss();
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);

        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .feMyOrderList(this.ssoId, this.country_id)
                .then(data => {
                  this.responseData = JSON.parse(data);
                  this.countries_list = this.responseData.countries_list;
                  console.log("list" + this.countries_list);
                  this.countries_list_data = this.responseData.countries_list_data;
                  console.log(" data fro my orders");
                  console.log(data);
                  console.log(" data fro my orders");
                  /*  var datePipe = new DatePipe("en-US");
                  for (
                    var i = 0;
                    i < this.responseData.final_data.length;
                    i++
                  ) {
                    this.responseData.final_data[
                      i
                    ].order_info.return_date = datePipe.transform(
                      this.responseData.final_data[i].order_info.return_date,
                      "dd-MM-yyyy"
                    );
                    this.responseData.final_data[
                      i
                    ].order_info.deploy_date = datePipe.transform(
                      this.responseData.final_data[i].order_info.deploy_date,
                      "dd-MM-yyyy"
                    );
                  } */
                  this.orders = this.responseData.final_data;
                  if (this.orders.length == 0) {
                    this.itemExist = false;
                  }
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.loading.dismiss();
              this.navCtrl.push(HomePage);
            }
          );
        }
      });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FemyordersPage");
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(FemyordersPage, { country_id: country_id });
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }
}
