import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
//import { QrScanPage } from '../../pages/qr-scan/qr-scan';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-extended-date-approval",
  templateUrl: "extended-date-approval.html"
})
export class ExtendedDateApprovalPage {
  orders = [];
  globals: Globals;
  role: string;
  menu_items: string[];
  shownGroup = null;
  requestedDate: any;
  ssoId = "";
  itemExist = true;
  responseData: any;
  errMsg: any;

  country_id = "";
  countries_list = "";
  countries_list_data = "";
  loading: any;
  normalData: any;

  toggleGroup(group, order) {
    console.log("detais:" + JSON.stringify(order));
    console.log(order.tools.length);
    if (order.tools.length == 0) {
      this.errMsg = true;
    } else {
      this.errMsg = false;
    }

    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    this.requestedDate = new Date().toISOString();
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.extendDateApprovalData();
    });
  }

  extendDateApprovalData() {
    this.messageService
      .getExtendDateData(this.ssoId, this.country_id)
      .then(data => {
        console.log("*****extend date request data********");
        //var res =  JSON.stringify(data);
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.normalData = JSON.parse(data);
        this.responseData = this.normalData.response_data;
        this.countries_list = this.normalData.countries_list;
        this.countries_list_data = this.normalData.countries_list_data;

        this.orders = this.responseData;
        console.log(this.orders);
        if (this.orders.length == 0) {
          this.itemExist = false;
        }
        console.log("*****extend date request data********");
        this.loading.dismiss();
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .getExtendDateData(this.ssoId, this.country_id)
                .then(data => {
                  console.log("*****extend date request data********");
                  this.normalData = JSON.parse(data);
                  this.responseData = this.normalData.response_data;
                  this.countries_list = this.normalData.countries_list;
                  this.countries_list_data = this.normalData.countries_list_data;
                  console.log(this.orders);
                  if (this.orders.length == 0) {
                    this.itemExist = false;
                  }
                  console.log("*****extend date request data********");
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ExtendedDateApprovalPage");
  }
  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(ExtendedDateApprovalPage, { country_id: country_id });
  }
  extendDate(order, status) {
    var jsonData = JSON.stringify({
      sso_id: this.ssoId,
      tool_order_id: order.order_info.tool_order_id,
      order_number: order.order_info.order_number,
      request_date: order.old_return_date,
      new_return_date: order.new_return_date,
      reason: "NA",
      approved_remarks: "NA",
      ac_submit: status,
      device_type: this.globals.deviceName
    });
    let alert = this.alertCtrl.create({
      title: "Submission of Tool Order Extend Date Request",
      message: "Are you sure you want to Submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the order gets displayed ....."
              });
              loading.present();
              this.messageService
                .extendDateApproval(jsonData)
                .then(res => {
                  if (res.status === 401) {
                    throw new Error("Unauthorized");
                  }
                  var data = JSON.parse(res);
                  loading.dismiss();
                  if (data["transaction_status"] == 1) {
                    console.log(data["transaction_des"]);
                    this.presentAlert(
                      "Extend Date Approved",
                      data["transaction_des"]
                    );
                  } else {
                    if (data["transaction_status"] == 2)
                      this.presentAlert(
                        "Extend Date Rejected",
                        data["transaction_des"]
                      );
                    else
                      this.presentAlert(
                        "Extend Date Status",
                        data["transaction_des"]
                      );
                  }
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error == "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .extendDateApproval(jsonData)
                          .then(res => {
                            var data = JSON.parse(res);
                            loading.dismiss();
                            if (data["transaction_status"] == 1) {
                              console.log(data["transaction_des"]);
                              this.presentAlert(
                                "Extend Date Approved",
                                data["transaction_des"]
                              );
                            } else {
                              if (data["transaction_status"] == 2)
                                this.presentAlert(
                                  "Extend Date Rejected",
                                  data["transaction_des"]
                                );
                              else
                                this.presentAlert(
                                  "Extend Date Status",
                                  data["transaction_des"]
                                );
                            }
                          });
                      },
                      error => {
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert.present();
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
  }
}
