import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogAckWhToolReq } from './log-ackwhtool-req';

@NgModule({
  declarations: [
    LogAckWhToolReq,
  ],
  imports: [
    IonicPageModule.forChild(LogAckWhToolReq),
  ],
})
export class LogAckWhToolReqModule {}
