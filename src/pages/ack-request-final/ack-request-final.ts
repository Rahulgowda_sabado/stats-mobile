import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { FetoolreturntoolPage } from "../../pages/fetoolreturntool/fetoolreturntool";
import { ExtendDatePage } from "../../pages/extend-date/extend-date";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { MenuPage } from "../menu/menu";

@IonicPage()
@Component({
  selector: "page-ack-request-final",
  templateUrl: "ack-request-final.html"
})
export class AckRequestFinalPage {
  globals: Globals;
  tools: any = {};
  allSelect = "Select All";
  toolDetails = [];
  selected = [];
  ssoId = "";
  toolOrderId = "";
  initiateRTResponse: any;
  acknowledgeToolMasterResponse: any;
  shownGroup = null;
  detailText = null;
  rtoId = null;
  responseData: any;
  error_msg = "";
  success_msg = "";
  order_type = "";
  toolHealth = "";
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider
  ) {
    let loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loading.present();
    this.globals = Globals.getInstance();
    this.toolOrderId = navParams.get("toolOrderId");
    this.rtoId = navParams.get("rtoId");
    console.log("fe tool return tool page");
    console.log(this.rtoId);
    console.log("fe tool return tool page");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      //this.toolOrderId = this.tools.order_info.tool_order_id;
      console.log("*******tool order id*******");
      console.log(this.toolOrderId);
      console.log("*******tool order id*******");

      this.messageService
        .getAcknowlegeToolInitialize(this.ssoId, this.toolOrderId, this.rtoId)
        .then(data => {
          console.log("initialite return tool response");
          console.log(data);
          console.log("initialite return tool response");
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          loading.dismiss();
          this.initiateRTResponse = JSON.parse(data);
          console.log("testing:" + JSON.stringify(this.initiateRTResponse));
          this.toolDetails = this.initiateRTResponse.return_parts;
          this.order_type = this.initiateRTResponse.order_type;
          console.log("tool details");
          console.log(this.toolDetails);
          this.toolDetails.forEach(tool => {
            tool.main_data.btn = "Select";
            tool.main_data.return_status = 1;
            tool["health_data"].forEach(healthData => {
              healthData.health = 1;
            });
          });
          console.log(this.initiateRTResponse);
          console.log("initialite return tool response");
        })
        .catch((e: any) => {
          let error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error === "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getAcknowlegeToolInitialize(
                    this.ssoId,
                    this.toolOrderId,
                    this.rtoId
                  )
                  .then(data => {
                    console.log("initialite return tool response");
                    console.log(data);
                    console.log("initialite return tool response");
                    this.initiateRTResponse = JSON.parse(data);
                    this.toolDetails = this.initiateRTResponse.return_parts;
                    console.log("tool details");
                    console.log(this.toolDetails);
                    this.toolDetails.forEach(tool => {
                      tool.main_data.btn = "Select";
                      tool.main_data.return_status = 1;
                      tool["health_data"].forEach(healthData => {
                        healthData.health = 1;
                      });
                    });
                    console.log(this.initiateRTResponse);
                    console.log("initialite return tool response");
                    loading.dismiss();
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });
    });
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  /* selectAll() {
    this.toolDetails.forEach(tool => {
      if (this.allSelect == "Select All") {
        tool.main_data.isAdded = true;
        this.selected.push(tool);
        tool.main_data.btn = "Unselect";
      } else {
        tool.main_data.isAdded = false;
        this.selected = this.selected.filter(obj => obj !== tool);
        tool.main_data.btn = "Select";
      }
    });
    if (this.allSelect == "Select All") {
      this.allSelect = "Unselect All";
    } else {
      this.allSelect = "Select All";
    }
  } */
  /*  public addElement(tool) {
    var main_data = tool.main_data;
    var contains = this.containsElement(main_data);
    if (contains) {
      main_data.isAdded = false;
      this.selected = this.selected.filter(obj => obj !== main_data);
      this.allSelect = "Select All";
      main_data.btn = "Select";
    } else {
      main_data.isAdded = true;
      this.selected.push(tool);
      main_data.btn = "Unselect";
      if (this.selected.length == this.toolDetails.length)
        this.allSelect = "Unselect All";
    }
  } */

  /*  public containsElement(mainData) {
    console.log("Selected Items ");
    console.log(this.selected);
    console.log("Selected Items ");
    console.log("Passed tool");
    console.log(mainData);
    console.log("Passed tool");
    var contains: boolean = false;
    this.selected.forEach(s1 => {
      if (s1.main_data.part_number == mainData.part_number) {
        contains = true;
      }
    });
    return contains;
  } */

  ionViewDidLoad() {
    console.log("ionViewDidLoad FetoolreturntoolPage");
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      //this.detailText = "Show Tool Detail";
      this.shownGroup = null;
      //alert(this.detailText);
    } else {
      //this.detailText = "Hide Tool Details";
      this.shownGroup = group;
      //alert(this.detailText);
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  changedStatus(tool) {
    this.toolHealth = tool;
  }

  continueReturn() {
    /* console.log("*************selected values*************");
    console.log(this.selected);
    console.log("************selected values******************");
    console.log("return response"); */
    console.log(this.initiateRTResponse);
    this.acknowledgeToolMasterResponse = this.initiateRTResponse;
    console.log("*********return response**********");
    this.convertLogic();
  }

  convertLogic() {
    /*  let loading = this.loadingCtrl.create({
      content:'Please wait...'
    });
    loading.present(); */
    let toolHealth1: any = [];

    this.acknowledgeToolMasterResponse["return_parts"].forEach(element => {
      console.log(element);

      if (
        ((element.health_data[0].health == "3" && element.health_data[0].remark == "") || (element.health_data[0].health == "3" && element.health_data[0].remark == null))
        || ((element.health_data[0].health == "2" && element.health_data[0].remark == "") || (element.health_data[0].health == "2" && element.health_data[0].remark == null))) {
        console.log("found defect without remarks");

        toolHealth1.push(element.health_data[0].part_number);
      }
    });

    if (toolHealth1.length != 0) {
      alert(
        "Please write the remarks for the following tools - " +
        JSON.stringify(toolHealth1)
      );
    } else {
      console.log("acknowledgeToolMasterResponse");
      console.log(this.acknowledgeToolMasterResponse);
      console.log("acknowledgeToolMasterResponse");
      console.log(this.rtoId);
      var tool_order_id = this.toolOrderId;
      var order_number = this.acknowledgeToolMasterResponse.order_number;
      var order_status_id = this.acknowledgeToolMasterResponse.order_status_id;
      var values = "";
      if (this.rtoId == null) {
        values =
          '{"order_status_id":"' +
          this.acknowledgeToolMasterResponse.order_status_id +
          '","tool_order_id":"' +
          tool_order_id +
          '","order_number":"' +
          order_number +
          '","device_type":"' +
          this.globals.deviceName +
          '","approve":"' +
          "1" +
          '","oah_id":{},"assetAndOrderedAsset":{},"oah_condition_id":{},"oah_oa_health_id_part_id":{},"oa_health_id":{},"remarks":{},' +
          '"sso_id":"' +
          this.ssoId +
          '"' +
          "}";
      } else if (this.rtoId != null) {
        values =
          '{"order_status_id":"' +
          this.acknowledgeToolMasterResponse.order_status_id +
          '","rto_id":"' +
          this.rtoId +
          '","device_type":"' +
          this.globals.deviceName +
          '","approve":"' +
          "1" +
          '","oah_id":{},"assetAndOrderedAsset":{},"orderedAssetAndAsset":{},"oah_condition_id":{},"oah_oa_health_id_part_id":{},"oa_health_id":{},"remarks":{},' +
          '"sso_id":"' +
          this.ssoId +
          '"' +
          "}";
      }

      console.log("************** values**********");
      console.log(values);
      var jsObj = JSON.parse(values);
      console.log("******************the jsObj*************");
      console.log(jsObj);

      if (this.rtoId == null) {
        this.toolDetails.forEach(element => {
          var healthObj = "";
          var mainData = element.main_data;
          var oahId = mainData.oah_id;
          //var orderStatusId = mainData.ordered_asset_id;
          var assetId = mainData.asset_id;
          var orderedAssetId = mainData.ordered_asset_id;
          var returnStatus = mainData.return_status;
          jsObj.oah_id[oahId] = orderedAssetId;
          jsObj.assetAndOrderedAsset[assetId] = orderedAssetId;
          jsObj.oah_condition_id[orderedAssetId] = returnStatus;
          jsObj.oah_oa_health_id_part_id[oahId] = {};
          jsObj.oa_health_id[oahId] = {};
          jsObj.remarks[oahId] = {};
          var healthData = element.health_data;
          healthData.forEach(healthElement => {
            var oaHealthId = healthElement.oa_health_id;
            var partIdVal = healthElement.part_id;
            var remarkwa = healthElement.remark;
            var healthStatus = healthElement.health;
            jsObj.oah_oa_health_id_part_id[oahId][oaHealthId] = [];
            jsObj.oa_health_id[oahId][oaHealthId] = {};
            jsObj.remarks[oahId][oaHealthId] = {};
            jsObj.oa_health_id[oahId][oaHealthId][partIdVal] = {};
            jsObj.oa_health_id[oahId][oaHealthId][partIdVal] = healthStatus;
            jsObj.remarks[oahId][oaHealthId][partIdVal] = {};
            jsObj.remarks[oahId][oaHealthId][partIdVal] =
              remarkwa == null ? "" : remarkwa;
            jsObj.oah_oa_health_id_part_id[oahId][oaHealthId].push(partIdVal);
          });
        });
      } else if (this.rtoId != null) {
        this.toolDetails.forEach(element => {
          var healthObj = "";
          var mainData = element.main_data;
          var oahId = mainData.oah_id;
          //var orderStatusId = mainData.ordered_asset_id;
          var assetId = mainData.asset_id;
          var orderedAssetId = mainData.ordered_asset_id;
          var returnStatus = mainData.return_status;
          jsObj.oah_id[oahId] = orderedAssetId;
          jsObj.assetAndOrderedAsset[assetId] = orderedAssetId;
          jsObj.orderedAssetAndAsset[orderedAssetId] = assetId;
          jsObj.oah_condition_id[orderedAssetId] = returnStatus;
          jsObj.oah_oa_health_id_part_id[oahId] = {};
          jsObj.oa_health_id[oahId] = {};
          jsObj.remarks[oahId] = {};
          var healthData = element.health_data;
          healthData.forEach(healthElement => {
            var oaHealthId = healthElement.oa_health_id;
            var partIdVal = healthElement.part_id;
            var remarkwa = healthElement.remark;
            var healthStatus = healthElement.health;
            jsObj.oah_oa_health_id_part_id[oahId][oaHealthId] = [];
            jsObj.oa_health_id[oahId][oaHealthId] = {};
            jsObj.remarks[oahId][oaHealthId] = {};
            jsObj.oa_health_id[oahId][oaHealthId][partIdVal] = {};
            jsObj.oa_health_id[oahId][oaHealthId][partIdVal] = healthStatus;
            jsObj.remarks[oahId][oaHealthId][partIdVal] = {};
            jsObj.remarks[oahId][oaHealthId][partIdVal] =
              remarkwa == null ? "" : remarkwa;
            jsObj.oah_oa_health_id_part_id[oahId][oaHealthId].push(partIdVal);
          });
        });
      }

      let alert1 = this.alertCtrl.create({
        title: "Submission of Acknowledgement",
        message: "Are you sure you want to submit",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Submit",
            handler: () => {
              console.log("Submit clicked");
              alert1.dismiss().then(() => {
                let loading = this.loadingCtrl.create({
                  content:
                    "Please wait until the Acknowledgement gets displayed ....."
                });
                loading.present();
                console.log(jsObj);
                this.messageService
                  .acknowledgeRequestValue(jsObj, this.rtoId, this.order_type)
                  .then(
                    data => {
                      if (data.status === 401) {
                        throw new Error("Unauthorized");
                      }
                      loading.dismiss();
                      this.responseData = JSON.parse(data);

                      if (this.responseData.transaction_status == 1) {
                        const alert2 = this.alertCtrl.create({
                          title: "Acknowledge Request",
                          subTitle: this.responseData.transaction_des,
                          buttons: ["OK"]
                        });
                        alert2.present();
                        this.navCtrl.setRoot(HomePage);
                      } else {
                        this.error_msg = this.responseData.transaction_des;
                        this.success_msg = "";
                      }
                    }

                    // ,(error: any) => {
                    //     console.log("Error from http header"+error);
                    //     console.log("Error from http header status text"+error.statusText);
                    //     if(error.statusText == "Unauthorized")
                    //     {
                    //       console.log("code matched and call the api");
                    //       this.refreshPromise().then(success => {
                    //           this.messageService.acknowledgeRequestValue(jsObj, this.rtoId).then(data =>{
                    //             loading.dismiss();
                    //             this.responseData=JSON.parse(data);;
                    //                if (this.responseData.transaction_status == 1) {
                    //                 const alert = this.alertCtrl.create({
                    //                   title: 'Acknowledge Request',
                    //                   subTitle: this.responseData.transaction_des,
                    //                   buttons: ['OK']
                    //                 });
                    //                 alert.present();

                    //                 this.navCtrl.setRoot(HomePage);
                    //               } else {
                    //                 this.error_msg = this.responseData.transaction_des;
                    //                 this.success_msg = "";
                    //               }

                    //           });
                    //         },(error) => {
                    //             console.log("error at Calling the Refresh Token"+JSON.stringify(error));
                    //             this.storage.set(this.globals.role_id, null);
                    //             this.navCtrl.push(HomePage);
                    //       });
                    //     }
                    //   }
                  )
                  .catch((e: any) => {
                    let error = e.message;
                    console.log("Error from http header" + error);
                    console.log("Error from http header status text" + error);
                    if (error == "Unauthorized") {
                      console.log("code matched and call the api");
                      this.refreshPromise().then(
                        success => {
                          this.messageService
                            .acknowledgeRequestValue(
                              jsObj,
                              this.rtoId,
                              this.order_type
                            )
                            .then(data => {
                              loading.dismiss();
                              this.responseData = JSON.parse(data);
                              if (this.responseData.transaction_status == 1) {
                                const alert2 = this.alertCtrl.create({
                                  title: "Acknowledge Request",
                                  subTitle: this.responseData.transaction_des,
                                  buttons: ["OK"]
                                });
                                alert2.present();

                                this.navCtrl.setRoot(HomePage);
                              } else {
                                this.error_msg = this.responseData.transaction_des;
                                this.success_msg = "";
                              }
                            });
                        },
                        error => {
                          console.log(
                            "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                          );
                          this.storage.set(this.globals.role_id, null);
                          this.navCtrl.push(HomePage);
                        }
                      );
                    }
                  });
              });
            }
          }
        ]
      });
      alert1.present();
      return false;
    }
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert3 = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert3.present();
    this.navCtrl.pop();
  }
}
