import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogAckFeToolReq } from './log-ackfetool-req';

@NgModule({
  declarations: [
    LogAckFeToolReq,
  ],
  imports: [
    IonicPageModule.forChild(LogAckFeToolReq),
  ],
})
export class LogAckFEToolReqModule {}
