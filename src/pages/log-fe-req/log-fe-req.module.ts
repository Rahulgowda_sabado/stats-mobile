import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogFeReq } from './log-fe-req';

@NgModule({
  declarations: [
    LogFeReq,
  ],
  imports: [
    IonicPageModule.forChild(LogFeReq),
  ],
})
export class LogFeReqModule {}
