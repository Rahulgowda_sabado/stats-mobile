import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";

@IonicPage()
@Component({
  selector: "page-wtw-stock-transfer",
  templateUrl: "wtw-stock-transfer.html"
})
export class WtwStockTransferPage {
  globals: Globals;
  ssoId = "";
  stockInformations: any = [];
  //stockInformations.or  der_in  f o:   = {};
  stnNo = "";
  assets: any = [];
  comment: string = "";
  toolOrderId: string = "";
  responseData: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    let loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loading.present();
    this.globals = Globals.getInstance();
    this.toolOrderId = navParams.get("tool_order_id");
    console.log("tool Order Id");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;

      this.messageService
        .stockWHToWH(sso_id, this.toolOrderId)
        .then(data => {
          //alert("count:"+JSON.stringify(data))
          //var res =  JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.stockInformations = JSON.parse(data);

          this.stnNo = this.stockInformations.order_info.stn_number;
          this.assets = this.stockInformations.assets;
          console.log("stock informations");
          console.log(this.stockInformations);
          console.log(this.stnNo);
          console.log(this.assets);
          loading.dismiss();
        })
        .catch((e: any) => {
          var error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .stockWHToWH(sso_id, this.toolOrderId)
                  .then(data => {
                    this.stockInformations = JSON.parse(data);
                    this.stnNo = this.stockInformations.order_info.stn_number;
                    this.assets = this.stockInformations.assets;
                    console.log("stock informations");
                    console.log(this.stockInformations);
                    console.log(this.stnNo);
                    console.log(this.assets);
                    loading.dismiss();
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });
    });
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad WtwStockTransferPage");
  }

  accept() {
    console.log(this.ssoId);
    var jsonData = JSON.stringify({
      stn_number: this.stnNo,
      tool_order_id: this.toolOrderId,
      admin_remarks: this.comment,
      sso_id: this.ssoId,
      submit_action: "1",
      device_type: this.globals.deviceName
    });

    let alert = this.alertCtrl.create({
      title: "Submission of Stock Transfer",
      message: "Are you sure you want to submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the order gets displayed ....."
              });
              loading.present();
              this.messageService
                .stockTransferWHToWHPost(jsonData)
                .then(data => {
                  // var res =  JSON.stringify(data);
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }
                  this.responseData = JSON.parse(data);
                  loading.dismiss();

                  this.presentAlert(
                    "Warehouse To Warehouse Stock Transfer",
                    this.responseData.transaction_des
                  );
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error == "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .stockTransferWHToWHPost(jsonData)
                          .then(data => {
                            loading.dismiss();
                            this.presentAlert(
                              "Warehouse To Warehouse Stock Transfer",
                              this.responseData.transaction_des
                            );
                          });
                      },
                      error => {
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert.present();
    return false;
  }

  decline() {
    console.log(this.ssoId);
    var jsonData = JSON.stringify({
      stn_number: this.stnNo,
      tool_order_id: this.toolOrderId,
      admin_remarks: this.comment,
      sso_id: this.ssoId,
      submit_action: "2",
      device_type: this.globals.deviceName
    });

    let alert = this.alertCtrl.create({
      title: "Submission of Stock Transfer",
      message: "Are you sure you want to submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the order gets displayed ....."
              });
              loading.present();
              this.messageService
                .stockTransferWHToWHPost(jsonData)
                .then(data => {
                  //var res =  JSON.stringify(data);
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }
                  this.responseData = JSON.parse(data);
                  loading.dismiss();
                  console.log(data);
                  this.presentAlert(
                    "Warehouse To Warehouse Stock Transfer",
                    this.responseData.transaction_des
                  );
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error == "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .stockTransferWHToWHPost(jsonData)
                          .then(data => {
                            console.log(data);
                            loading.dismiss();
                            this.presentAlert(
                              "Warehouse To Warehouse Stock Transfer",
                              this.responseData.transaction_des
                            );
                          });
                      },
                      error => {
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert.present();
    return false;
  }

  // end
  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
  }
}
