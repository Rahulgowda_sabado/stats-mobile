import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";

import { AckRequestFinalPage } from "../../pages/ack-request-final/ack-request-final";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { MenuPage } from "../menu/menu";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-acknowledgement",
  templateUrl: "acknowledgement.html"
})
export class AcknowledgementPage {
  error_msg = "";
  success_msg = "";
  shownGroup = null;

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  orders = [];
  globals: Globals;
  role: string;
  menu_items: string[];
  ssoId: string;
  showMessage: boolean = false;
  responseData: any;
  loading: any;
  alertType: any;
  country_id = "";
  countries_list = "";
  countries_list_data = "";
  itemExist = true;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.alertType = navParams.get("alertType");
    this.country_id = this.navParams.get("country_id");
    //this.country_id = "";
    if (this.alertType == undefined) this.alertType = 0;
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.getAcknowldgement();
    });
  }

  getAcknowldgement() {
    this.messageService
      .getAcknowledgmentRequest(this.ssoId, this.alertType, this.country_id)
      .then(
        (data: any) => {
          console.log("*****acknoledge request data********");
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);
          this.countries_list = this.responseData.countries_list;
          this.countries_list_data = this.responseData.countries_list_data;
          /*   var datePipe = new DatePipe("en-US");
          for (var i = 0; i < this.responseData.length; i++) {
            this.responseData[i].order_info.return_date = datePipe.transform(
              this.responseData[i].order_info.return_date,
              "dd-MM-yyyy"
            );
          } */

          this.orders = this.responseData.final_data;
          console.log(this.orders);

          console.log(this.orders.length);
          if (this.orders.length == 0) {
            this.itemExist = false;
            this.showMessage = true;
          }
          console.log(this.showMessage);
          console.log("*****acknoledge request data********");
          this.loading.dismiss();
        },
        (error: any) => {
          console.log("Error from http header" + error);
          //console.log("Error from http header status text"+error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getAcknowledgmentRequest(
                    this.ssoId,
                    this.alertType,
                    this.country_id
                  )
                  .then(data => {
                    console.log("*****acknoledge request data********");
                    this.responseData = JSON.parse(data);
                    this.countries_list = this.responseData.countries_list;
                    this.countries_list_data = this.responseData.countries_list_data;
                    this.orders = this.responseData.final_data;
                    console.log(this.orders);

                    console.log(this.orders.length);
                    if (this.orders.length == 0) {
                      this.itemExist = false;
                      this.showMessage = true;
                    }
                    console.log(this.showMessage);
                    console.log("*****acknoledge request data********");
                    this.loading.dismiss();
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  // scanAsset(order) {
  //   this.navCtrl.push(QrScanPage, { orders: order });
  // }

  ionViewDidLoad() { }

  presentAwkAlert(order) {
    var orderInfo = order.order_info;
    var rtoId = orderInfo.rto_id;
    var ssoId = this.ssoId;
    var toolOrderId = orderInfo.tool_order_id;
    var alertType = 1;
    if (rtoId != null) {
      alertType = 2;
      this.messageService
        .acknowledgeRequestAddressForFEAlert(alertType, rtoId, ssoId)
        .then(data => {
          console.log("*****acknoledge request data********");
          console.log(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);

          var from = this.responseData.from;
          var expectedDate =
            this.responseData.expected_delivery_date == ""
              ? "NA"
              : this.responseData.expected_delivery_date;
          var awb =
            this.responseData.awb_or_docket_number == ""
              ? "NA"
              : this.responseData.awb_or_docket_number;
          var subText =
            "<strong>From</strong>: " +
            from +
            "<br/><strong>Exp. dekivery date</strong>:" +
            expectedDate +
            "<br/><strong>AWB/Docket No.</strong>:" +
            awb +
            "<br/>";

          const alert = this.alertCtrl.create({
            title: "Acknowledge request data",
            subTitle: subText,

            buttons: ["OK"]
          });
          alert.present();
        })
        .catch((e: any) => {
          let error = e.message;
          console.log("Error from http header" + error);
          //console.log("Error from http header status text"+error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .acknowledgeRequestAddressForFEAlert(alertType, rtoId, ssoId)
                  .then(data => {
                    console.log("*****acknoledge request data********");
                    console.log(data);
                    this.responseData = JSON.parse(data);

                    var from = this.responseData.from;
                    var expectedDate =
                      this.responseData.expected_delivery_date == ""
                        ? "NA"
                        : this.responseData.expected_delivery_date;
                    var awb =
                      this.responseData.awb_or_docket_number == ""
                        ? "NA"
                        : this.responseData.awb_or_docket_number;
                    var subText =
                      "<strong>From</strong>: " +
                      from +
                      "<br/><strong>Exp. delivery date</strong>:" +
                      expectedDate +
                      "<br/><strong>AWB/Docket No.</strong>:" +
                      awb +
                      "<br/>";
                    // this.presentAlertAwkPage("Address Information",subText);
                    const alert = this.alertCtrl.create({
                      title: "More Info.",
                      subTitle: subText,
                      buttons: ["OK"]
                    });
                    alert.present();
                    // this.navCtrl.pop();
                    this.navCtrl.setRoot(HomePage);
                    // console.log("*****acknoledge request data********");
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });
    } else {
      this.messageService
        .acknowledgeRequestAddressForWHAlert(ssoId, alertType, toolOrderId)
        .then(data => {
          console.log("*****acknoledge request data********");
          console.log(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);

          var from = this.responseData.from;
          console.log(this.responseData.expected_delivery_date);
          var expectedDate =
            this.responseData.expected_delivery_date == ""
              ? "NA"
              : this.responseData.expected_delivery_date;
          var awb =
            this.responseData.awb_or_docket_number == ""
              ? "NA"
              : this.responseData.awb_or_docket_number;
          var subText =
            "<strong>From</strong>: " +
            from +
            "<br/><strong>Exp. delivery date</strong>:" +
            expectedDate +
            "<br/><strong>AWB/Docket No.</strong>:" +
            awb +
            "<br/>";
          //this.presentAlertAwkPage("Address Information",subText);
          const alert = this.alertCtrl.create({
            title: "More Info.",
            subTitle: subText,
            buttons: ["OK"]
          });
          alert.present();
          // this.navCtrl.pop();
          //this.navCtrl.setRoot(MenuPage);

          //console.log("*****acknoledge request data********");
        })
        .catch((e: any) => {
          let error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .acknowledgeRequestAddressForWHAlert(
                    ssoId,
                    alertType,
                    toolOrderId
                  )
                  .then(data => {
                    console.log("*****acknoledge request data********");
                    console.log(data);
                    this.responseData = JSON.parse(data);

                    var from = this.responseData.from;
                    //console.log(this.responseData.expected_delivery_date);
                    var expectedDate =
                      this.responseData.expected_delivery_date == ""
                        ? "NA"
                        : this.responseData.expected_delivery_date;
                    var awb =
                      this.responseData.awb_or_docket_number == ""
                        ? "NA"
                        : this.responseData.awb_or_docket_number;
                    var subText =
                      "<strong>From</strong>: " +
                      from +
                      "<br/><strong>Expected Date</strong>:" +
                      expectedDate +
                      "<br/><strong>AWB/Docket No.</strong>:" +
                      awb +
                      "<br/>";
                    // this.presentAlertAwkPage("Address Information",subText);
                    const alert = this.alertCtrl.create({
                      title: "Acknowledge request data",
                      subTitle: subText,
                      buttons: ["OK"]
                    });
                    alert.present();
                    // this.navCtrl.pop();
                    this.navCtrl.setRoot(MenuPage);
                    //console.log("*****acknoledge request data********");
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });

      // this.messageService.acknowledgeRequestAddressForWHAlert(ssoId,alertType, toolOrderId).subscribe(data => {
      //   console.log("*****acknoledge request data********");
      //   console.log(data);
      //   this.responseData = data;
      //   var from = this.responseData.from;
      //   console.log(this.responseData.expected_delivery_date);
      //   var expectedDate = (this.responseData.expected_delivery_date == "")?"NA":this.responseData.expected_delivery_date;
      //   var awb = (this.responseData.awb_or_docket_number == "")?"NA":this.responseData.awb_or_docket_number;
      //   var subText = "<strong>From</strong>: "+from+"<br/><strong>Expected Date</strong>:"+expectedDate+"<br/><strong>AWB/Docket No.</strong>:"+awb+"<br/>";
      //   this.presentAlertAwkPage("Address Information",subText);
      //   console.log("*****acknoledge request data********");
      // });
    }
  }

  viewCountryData(country_id, alert_type: any) {
    this.country_id = country_id;
    this.navCtrl.push(AcknowledgementPage, { country_id: country_id, alertType: alert_type });
  }

  presentAlertForm(tiTLE, subTiTLE) {
    const alert = this.alertCtrl.create({
      title: tiTLE,
      subTitle: subTiTLE,
      buttons: ["Dismiss"]
    });
    alert.present();
  }

  presentAlert(tiTLE, subTiTLE) {
    const alert = this.alertCtrl.create({
      title: tiTLE,
      subTitle: subTiTLE,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.pop();
  }

  initiate(order) {
    console.log("initate tool called");
    console.log(order);
    var toolOrderId = order.order_info.tool_order_id;
    var rtoId = order.order_info.rto_id;
    console.log("*********rto id********");
    console.log(rtoId);
    console.log("*********rto id********");
    console.log("*********toolOrderId********");
    console.log(toolOrderId);
    console.log("*********toolOrderId********");
    this.navCtrl.push(AckRequestFinalPage, {
      toolOrderId: toolOrderId,
      rtoId: rtoId
    });
    console.log("initate tool called");
  }

  awkSubmit(order) {
    console.log("awk submit");
    console.log(order);
    var toolOrderId = order.fe1_tool_order_id;
    var orderNumber = order["order_info"]["order_number"];
    var orderStatusId = order.assets[0]["order_status_id"];
    var rtoID = order.rto_id;
    var inValidForm: string = "false";
    var val = "";
    if (rtoID == null) {
      //condition for WH
      val =
        '{"tool_order_id":"' +
        toolOrderId +
        '","order_number":"' +
        orderNumber +
        '","order_status_id":"' +
        orderStatusId +
        '","sso_id":"' +
        this.ssoId +
        '","device_type:"' +
        this.globals.deviceName +
        '","oah_condition_id":{}}';
    } else {
      //condition for FE
      val =
        '{"fe1_tool_order_id":"' +
        toolOrderId +
        '","sso_id":"' +
        this.ssoId +
        '","order_status_id":"' +
        orderStatusId +
        '","rto_id":"' +
        rtoID +
        '","device_type:"' +
        this.globals.deviceName +
        '","oah_condition_id":{}}';
    }

    var jsonObj = JSON.parse(val);
    console.log(jsonObj);

    order.assets.forEach(asset => {
      console.log(asset);
      var assetId = asset.ordered_asset_id;
      var status = asset.status;

      if (status == 1) {
        inValidForm = "true";
      }

      if (status == "true") {
        status = "1";
      } else if (status == "false") {
        status = "2";
      }

      jsonObj.oah_condition_id[+assetId] = status;
    });

    console.log("***********");
    console.log(toolOrderId);
    console.log(orderNumber);
    console.log(orderStatusId);
    console.log("***********");

    console.log(jsonObj);
    console.log("****************");

    if (inValidForm == "true") {
      this.presentAlertForm(
        "Acknowledge request",
        "Please acknowledge all the tools for order"
      );
    } else {
      this.postRequest(rtoID, jsonObj);
    }
  }

  postRequest(rtoID, jsonObj) {
    this.messageService
      .getAckRequest(rtoID, jsonObj)
      .then(
        res => {
          console.log("*****acknoledge request data********");
          console.log(data);
          if (res.status === 401) {
            throw new Error("Unauthorized");
          }
          var data = JSON.parse(res);

          var statusId = data["transaction_status"];
          var statusDesc = data["transaction_des"];

          if (statusId == 1) {
            this.presentAlert("Acknowledge Request Request", statusDesc);
          } else if (statusId == 2) {
            this.presentAlert("Tool Acknowledge Failed", statusDesc);
          }
          console.log("*****acknoledge request data********");
        }
        // ,(error: any) => {
        //     console.log("Error from http header"+error);
        //     console.log("Error from http header status text"+error.statusText);
        //     if(error.statusText == "Unauthorized")
        //     {
        //       console.log("code matched and call the api");
        //       this.refreshPromise().then(success => {
        //           this.messageService.getAckRequest(rtoID, jsonObj).then(res =>{
        //             console.log("*****acknoledge request data********");
        //             console.log(data);
        //             var data = JSON.parse(res);
        //             var statusId = data['transaction_status'];
        //             var statusDesc = data['transaction_des'];

        //             if (statusId == 1) {
        //               this.presentAlert("Acknowledge Request Request", statusDesc);
        //             } else if (statusId == 2) {
        //               this.presentAlert("Tool Acknowledge Failed", statusDesc);
        //             }
        //             console.log("*****acknoledge request data********");
        //           });
        //         },(error) => {
        //             console.log("error at Calling the Refresh Token"+JSON.stringify(error));
        //             this.storage.set(this.globals.role_id, null);
        //             this.navCtrl.push(HomePage);
        //       });
        //     }
        //   }
      )
      .catch((e: any) => {
        let error = e.message;
        console.log("Error from http header" + error);
        //console.log("Error from http header status text"+error.statusText);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService.getAckRequest(rtoID, jsonObj).then(res => {
                console.log("*****acknoledge request data********");
                console.log(data);
                var data = JSON.parse(res);
                var statusId = data["transaction_status"];
                var statusDesc = data["transaction_des"];

                if (statusId == 1) {
                  this.presentAlert("Acknowledge Request Request", statusDesc);
                } else if (statusId == 2) {
                  this.presentAlert("Tool Acknowledge Failed", statusDesc);
                }
                console.log("*****acknoledge request data********");
              });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      });

    console.log("inside getAck request");
  }

  presentAlertAwkPage(tiTLE, subTiTLE) {
    console.log("present alert calld");
    const alert = this.alertCtrl.create({
      title: tiTLE,
      subTitle: subTiTLE,
      buttons: ["OK"]
    });
    alert.present();
  }
}
