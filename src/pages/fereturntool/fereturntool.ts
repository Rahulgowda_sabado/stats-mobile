import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { FetoolreturntoolPage } from "../../pages/fetoolreturntool/fetoolreturntool";
import { ExtendDatePage } from "../../pages/extend-date/extend-date";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-fereturntool",
  templateUrl: "fereturntool.html"
})
export class FereturntoolPage {
  globals: Globals;
  returnTools = [];
  shownGroup = null;
  ssoId = "";
  showMessage: boolean = false;
  responseData: any;
  errMsg: any;
  country_id = "";
  countries_list = "";
  countries_list_data = "";

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider
  ) {
    const loading = this.loadingCtrl.create({
      content: "Loading..."
    });
    loading.present();
    this.globals = Globals.getInstance();
    var alertType = navParams.get("alertType");
    this.country_id = this.navParams.get("country_id");
    /*  this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id = country_id;
    }); */
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;

      this.messageService
        .getReturnTools(sso_id, alertType, this.country_id)
        .then(data => {
          // alert(JSON.stringify(data));
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          console.log(JSON.stringify(data));
          this.responseData = JSON.parse(data);
          this.countries_list = this.responseData.countries_list;
          this.countries_list_data = this.responseData.countries_list_data;

          //Converting the yyyy-mm-dd format to dd-mm-yyyy
          /*  var datePipe = new DatePipe('en-US')
        for(var i=0;i<this.responseData.length;i++){
        this.responseData[i].order_info.return_date = datePipe.transform(this.responseData[i].order_info.return_date, 'dd-MM-yyyy');
        this.responseData[i].order_info.request_date = datePipe.transform(this.responseData[i].order_info.request_date, 'dd-MM-yyyy');  
          } */

          this.returnTools = this.responseData.final_data;
          console.log("return tool informations");
          console.log(this.returnTools);
          if (this.returnTools.length == 0) {
            this.showMessage = true;
          }
          console.log("return tool informations");
          loading.dismiss();
        })
        .catch((e: any) => {
          var error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getReturnTools(sso_id, alertType, this.country_id)
                  .then(data => {
                    this.responseData = JSON.parse(data);
                    this.countries_list = this.responseData.countries_list;
                    this.countries_list_data = this.responseData.countries_list_data;
                    this.returnTools = this.responseData.final_data;
                    console.log("return tool informations");
                    console.log(this.returnTools);
                    if (this.returnTools.length == 0) {
                      this.showMessage = true;
                    }
                    console.log("return tool informations");
                    loading.dismiss();
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });

      // end
      // this.messageService.getReturnTools(sso_id,alertType).subscribe(data => {
      //   this.responseData = data;
      //    this.returnTools = this.responseData;
      //   console.log("return tool informations");
      //   console.log(this.returnTools);
      //   if(this.returnTools.length == 0){
      //     this.showMessage = true;
      //   }
      //   console.log("return tool informations");
      //   loading.dismiss();
      // });
    });
  }

  toggleGroup(group, tool) {
    console.log(tool);
    if (tool.assets.length == 0) {
      this.errMsg = true;
    } else {
      this.errMsg = false;
    }

    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FereturntoolPage");
  }

  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(FereturntoolPage, { country_id: country_id });
  }

  navigateTo(tool) {
    this.navCtrl.push(FetoolreturntoolPage, { tool: tool });
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  extendDate(tool) {
    console.log("extend date");
    console.log(tool);
    console.log("extend date");
    //alert(JSON.stringify(tool));
    this.navCtrl.push(ExtendDatePage, { toolInfo: tool });
  }
}
