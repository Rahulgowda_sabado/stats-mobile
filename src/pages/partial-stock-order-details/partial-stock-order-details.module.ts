import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PartialStockOrderDetailsPage } from './partial-stock-order-details';

@NgModule({
  declarations: [
    PartialStockOrderDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PartialStockOrderDetailsPage),
  ],
})
export class PartialStockOrderDetailsPageModule {}
