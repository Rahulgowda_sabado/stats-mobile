import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
//import { StockOrderDetailsPage } from "../stock-order-details/stock-order-details";
// import { WtwStockTransferPage } from "../wtw-stock-transfer/wtw-stock-transfer";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
//import { StockTransferPage } from "../stock-transfer/stock-transfer";

@Component({
  selector: "page-partial-stock-order-details",
  templateUrl: "partial-stock-order-details.html"
})
export class PartialStockOrderDetailsPage {
  disabled = false;

  error_msg = "";
  errorMsg = false;
  shownGroup = null;
  stockOrderDet = [];
  globals: Globals;
  normalData: any;
  ssoId = "";
  selectedWarehouseId = {};
  sumbitButtonShown = true;
  responseData: any;
  assignedQty: any;
  stockOrderLength: any;
  allowSubmit: any;

  option_value1 = "";
  option_value2 = "";
  fe_check = "";
  loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    var toolOrderId = navParams.get("tool_order_id");
    console.log("tool Order Id");
    //console.log("!tool['wh_data'].length:"+!tool['wh_data'].length)
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;

      this.messageService
        .partialStockOrderDetForOrders(sso_id, toolOrderId)
        .then(data => {
          //var res =  JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.normalData = JSON.parse(data);

          this.stockOrderDet = this.normalData.final_data;
          this.option_value1 = this.normalData["option_value1"];
          this.option_value2 = this.normalData["option_value2"];
          this.fe_check = this.normalData["fe_check"];

          console.log(this.stockOrderDet);

          let len: any;
          len = 0;
          for (var i = 0; i < this.stockOrderDet.length; i++) {
            if (
              this.stockOrderDet[i].quantity != null &&
              this.stockOrderDet[i].quantity != undefined
            ) {
              len = len + +this.stockOrderDet[i].quantity;
            }
          }
          this.stockOrderLength = len;
          //alert(this.stockOrderLength)
          this.loading.dismiss();
          this.submitButtonCheck();
        })
        .catch((e: any) => {
          var error = e.message;

          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .partialStockOrderDetForOrders(sso_id, toolOrderId)
                  .then(data => {
                    this.normalData = JSON.parse(data);
                    this.stockOrderDet = this.normalData.final_data;
                    this.option_value1 = this.normalData["option_value1"];
                    this.option_value2 = this.normalData["option_value2"];
                    this.fe_check = this.normalData["fe_check"];
                    console.log(this.stockOrderDet);
                    this.submitButtonCheck();
                    this.loading.dismiss();
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });

      // end

      // this.messageService.stockOrderDetForOrders(sso_id, toolOrderId).subscribe(data => {
      //   this.normalData = data;
      //   this.stockOrderDet = this.normalData;
      //   console.log(this.stockOrderDet);
      //   this.submitButtonCheck();
      // });
    });
  }

  submitButtonCheck() {
    var data = this.stockOrderDet;
    data.forEach(element => {
      var warehouses = element.wh_data;
      console.log("******warehouses****");
      console.log(warehouses.length);

      if (warehouses.length == 0) {
        // this.error_msg = "There are no tools found in the warehouse";
        /* alert("There are tools found in the warehouse"); */
        this.sumbitButtonShown = false;
      } else {
        console.log("submit button status " + this.sumbitButtonShown);
        console.log("******warehouses****");
      }
    });
  }

  toggleGroup(group, i) {
    if (this.sumbitButtonShown) {
      let requiredQty;
      let whAssignedQty = 0;
      requiredQty = i.quantity;

      for (var k = 0; k < i.wh_data.length; k++) {
        whAssignedQty = whAssignedQty + +i.wh_data[k].qty;
      }

      if (requiredQty == whAssignedQty) {
        this.errorMsg = false;
      } else {
        this.errorMsg = true;
      }
    }
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  getToolTotalOrdered(warehouses) {
    console.log(warehouses);
    var wareOrderedCount: number = 0;
    for (let warehouse of warehouses) {
      wareOrderedCount += warehouse.qty;
    }
    return wareOrderedCount;
  }

  updtQty(tool, warehouse, inc) {
    console.log("*************");
    console.log(tool);
    console.log(warehouse);

    if (warehouse.qty === undefined) warehouse.qty = 1;

    var wareOrderedCount = this.getToolTotalOrdered(tool.wh_data);
    console.log(wareOrderedCount);
    console.log("***************");
    if (inc) {
      console.log(warehouse.qty + ">" + warehouse.wh_qty);
      console.log(
        tool.quantity - tool.available_quantity + "<=" + warehouse.wh_qty
      );
      if (
        warehouse.wh_qty > warehouse.qty &&
        wareOrderedCount < tool.quantity - tool.available_quantity
      ) {
        warehouse.order_qty += 1;
      }
    } else {
      if (warehouse.order_qty > 0) {
        // order.req_qty -= 1;
        warehouse.order_qty -= 1;
      }
    }
  }

  get(warehouse, tool) {
    this.assignedQty = warehouse.qty;
    if (warehouse.qty != tool.quantity) {
      this.errorMsg = true;
    } else {
      this.errorMsg = false;
    }
  }

  onClick(tool) {
    console.log(this.stockOrderDet);
    console.log(this.ssoId);
    var jsObj = this.convertLogic(this.stockOrderDet, this.ssoId);
    if (this.allowSubmit == true) {
      let alert = this.alertCtrl.create({
        title: "Submission of Order",
        message: "Are you sure you want to submit",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Submit",
            handler: () => {
              console.log("Submit clicked");
              alert.dismiss().then(() => {
                let loading = this.loadingCtrl.create({
                  content: "Please wait until the order gets displayed ....."
                });
                loading.present();
                this.messageService
                  .partialStransfersForOrdersPost(jsObj)
                  .then(data => {
                    //alert("count:"+JSON.stringify(data))
                    //var res =  JSON.stringify(data);
                    if (data.status === 401) {
                      throw new Error("Unauthorized");
                    }
                    this.responseData = JSON.parse(data);

                    // alert('responce data'+JSON.stringify(this.responseData));
                    loading.dismiss();
                    console.log("****data*******");
                    console.log(data);
                    this.presentAlert(
                      "Stock transfer",
                      this.responseData.transaction_des
                    );

                    console.log("***********");
                  })
                  .catch((e: any) => {
                    var error = e.message;
                    console.log("Error from http header" + error);
                    console.log("Error from http header status text" + error);
                    if (error == "Unauthorized") {
                      console.log("code matched and call the api");
                      this.refreshPromise().then(
                        success => {
                          this.messageService
                            .partialStransfersForOrdersPost(jsObj)
                            .then(data => {
                              console.log("****data*******");
                              console.log(data);
                              loading.dismiss();
                              this.presentAlert(
                                "Stock transfer",
                                "Initialized Succesfully"
                              );
                              console.log("***********");
                            });
                        },
                        error => {
                          console.log(
                            "error at Calling the Refresh Token" +
                              JSON.stringify(error)
                          );
                          this.storage.set(this.globals.role_id, null);
                          this.navCtrl.push(HomePage);
                        }
                      );
                    }
                  });
              });
            }
          }
        ]
      });
      alert.present();
      return false;
    }
  }

  convertLogic(data, ssoid) {
    console.log("******convert logic**************");
    console.log(data);

    let leng: any;
    let leng1: any;
    let leng2: any;
    let totalSum: any;
    let toolArray = [];
    let whArray = [];
    let toolName: any;
    leng = 0;
    leng1 = 0;
    leng2 = 0;
    totalSum = 0;
    for (var i = 0; i < data.length; i++) {
      let sum = 0;
      for (var j = 0; j < data[i].wh_data.length; j++) {
        if (
          data[i].wh_data[j].qty != undefined &&
          data[i].wh_data[j].qty != "" &&
          data[i].wh_data[j].qty != 0
        ) {
          if (data[i].wh_data[j].qty > data[i].wh_data[j].wh_qty) {
            sum = 0;
            leng2++;
            whArray.push(data[i].wh_data[j].wh_name);
            toolName = data[i].part_number;
            sum = sum + data[i].wh_data[j].qty;
          }
          let a: number = data[i].wh_data[j].qty;
          sum = sum + +a;
          console.log(sum + "+++++++++++++++++++++++++++");
        }
      }

      console.log(sum + "--------------------");

      totalSum = totalSum + sum;

      console.log("sum " + totalSum);

      if (sum > data[i].quantity) {
        leng++;
        toolArray.push(data[i].part_number);
        break;
      }
    }

    if (totalSum == "" || totalSum == 0) {
      leng1++;
    }

    if (leng2 != 0) {
      alert(
        "Assigned quantity is more than the available quantity in the following Warehouses: " +
          JSON.stringify(whArray) +
          "  for the Tool " +
          toolName
      );
      this.allowSubmit = false;
    } else if (leng != 0) {
      alert(
        "Assigned Quantity is more than the required quantity for the Following Tool List:" +
          JSON.stringify(toolArray)
      );
      this.allowSubmit = false;
    } else if (leng1 != 0) {
      alert("Please assign atleast one quantity for the request");
      this.allowSubmit = false;
    } else {
      this.allowSubmit = true;
    }

    //console.log(data[0].wh_data[0].qty)
    var toolOrderId = data[0].tool_order_id;
    console.log(toolOrderId);
    var values =
      '{"sso_id":"' +
      ssoid +
      '","fe_check":"' +
      this.fe_check +
      '","tool_order_id":"' +
      toolOrderId +
      '","device_type":"' +
      this.globals.deviceName +
      '","od_req_qty":{},"post_od_tool":{},"post_qty":{},"submit_fe":"1"}';
    var jsObj = JSON.parse(values);
    console.log(jsObj);
    //var warehouseIds = [];

    data.forEach(element => {
      // console.log("ui_logic Qty"+ui_logic_qty);
      // console.log("tool ID "+element.tool_id);

      // console.log("ui_logic Qty"+ui_logic_qty);
      // console.log("tool ID "+element.tool_id);
      var orderToolID = element.ordered_tool_id;
      var quantity = element.quantity;
      var toolId = element.tool_id;
      //console.log("Before Empty Pipe"+JSON.stringify(jsObj));
      jsObj.od_req_qty = jsObj.od_req_qty || {};
      //console.log("After Empty Pipe"+JSON.stringify(jsObj));
      jsObj.od_req_qty[orderToolID] = quantity;
      //console.log("After Empty Pipe with Pushed Value"+JSON.stringify(jsObj));
      jsObj.post_od_tool = jsObj.post_od_tool || {};
      jsObj.post_od_tool[orderToolID] = toolId;
      var warehouses = element.wh_data;
      console.log("******warehouses****");
      console.log(warehouses);
      console.log("******warehouses****");
      warehouses.forEach(warehouse => {
        if (
          warehouse.qty != undefined &&
          warehouse.qty != "" &&
          warehouse.qty != 0
        ) {
          jsObj.post_qty = jsObj.post_qty || {};
          jsObj.post_qty[warehouse.wh_id] = {};
        }
      });
    });
    console.log("Data after postQty" + JSON.stringify(jsObj));

    data.forEach(element => {
      var orderToolID = element.ordered_tool_id;
      var warehouses = element.wh_data;
      warehouses.forEach(warehouse => {
        if (
          warehouse.qty != undefined &&
          warehouse.qty != "" &&
          warehouse.qty != 0
        ) {
          jsObj.post_qty[warehouse.wh_id][orderToolID] = warehouse.qty;
        }
      });
    });
    console.log(jsObj);
    console.log("*********convert logic***********");
    return jsObj;
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad StockOrderDetailsPage");
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
  }
}
