import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogStReq } from './log-st-req';

@NgModule({
  declarations: [
    LogStReq,
  ],
  imports: [
    IonicPageModule.forChild(LogStReq),
  ],
})
export class LogStReqModule {}
