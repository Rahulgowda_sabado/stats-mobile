import { Component } from '@angular/core';
import { Globals } from '../../app/Globals';
import { Storage } from '@ionic/storage';
import { ReturnToolPage } from '../../pages/return-tool/return-tool';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MessageServiceProvider } from '../../providers/message-service/message-service';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-fetoolreturntool',
  templateUrl: 'fetoolreturntool.html',
})
export class FetoolreturntoolPage {

  globals: Globals;
  tools: any = {};
  allSelect = "Select All";
  toolDetails = [];
  selected = [];
  ssoId = '';
  toolOrderId = '';
  initiateRTResponse: any;
  shownGroup = null;
  detailText = null;
  responseData: any;
  error_msg = '';
  success_msg = '';
  scannedCode = '';
  toolHealth = "";


  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, private storage: Storage, public messageService: MessageServiceProvider) {


    this.globals = Globals.getInstance();
    this.tools = navParams.get('tool');
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });


    console.log("the tool nav params");
    console.log(this.tools);


    this.scannedCode = navParams.get('scannedCode');
    console.log(" fe tool return tool the scannedCode ");
    console.log(this.scannedCode);

    console.log("fe tool return tool page");
    console.log(this.tools);

    console.log(this.tools.assets);
    console.log("fe tool return tool page");

    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.toolOrderId = this.tools.order_info.tool_order_id;
      console.log("*******tool order id*******");
      console.log(this.toolOrderId);
      console.log("*******tool order id*******");

      this.messageService.getReturnToolInitialize(this.ssoId, this.toolOrderId).then(data => {
        //var res =  JSON.stringify(data);
        console.log("initialite return tool response");
        console.log(data);
        console.log("initialite return tool response");
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        loading.dismiss();
        this.initiateRTResponse = JSON.parse(data);

        this.toolDetails = this.initiateRTResponse.return_parts;
        console.log("tool details");
        console.log(this.toolDetails);

        this.toolDetails.forEach(tool => {
          tool.main_data.btn = "Select";
          tool.main_data.return_status = 1;
          tool['health_data'].forEach(healthData => {
            healthData.health = healthData.asset_condition_id;
          });
        });

        console.log("data prepared by maruthi" + this.toolDetails);
        console.log("data prepared by maruthi" + JSON.stringify(this.toolDetails));

        /*
        this.toolDetails.forEach(tool => {
          tool.btn = "Select";
          if (tool.asset_status_id == 4 || tool.asset_status_id == 12) {
            tool.calibrationRequired = true;
            this.addElement(tool);
          } else {
            tool.calibrationRequired = false;
          }
        });
        */
        console.log(this.initiateRTResponse);
        console.log("initialite return tool response");
      }).catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(success => {
            this.messageService.getReturnToolInitialize(this.ssoId, this.toolOrderId).then(data => {
              console.log("initialite return tool response");
              console.log(data);
              loading.dismiss();
              console.log("initialite return tool response");
              this.initiateRTResponse = JSON.parse(data);
              this.toolDetails = this.initiateRTResponse.return_parts;
              console.log("tool details");
              console.log(this.toolDetails);
              this.toolDetails.forEach(tool => {
                tool.main_data.btn = "Select";
                tool.main_data.return_status = 1;
                tool['health_data'].forEach(healthData => {
                  healthData.health = healthData.asset_condition_id;
                });
              });
              console.log("data prepared by maruthi" + JSON.stringify(this.toolDetails));

              // modified by maruthi on 18th april'18


              /*

              this.toolDetails.forEach(tool => {
                tool.btn = "Select";
                if (tool.asset_status_id == 4 || tool.asset_status_id == 12) {
                  tool.calibrationRequired = true;
                  this.addElement(tool);
                } else {
                  tool.calibrationRequired = false;
                }
              });
              
              */
              console.log(this.initiateRTResponse);
              console.log("initialite return tool response");
            });
          }, (error) => {
            console.log("error at Calling the Refresh Token" + JSON.stringify(error));
            this.storage.set(this.globals.role_id, null);
            this.navCtrl.push(HomePage);
          });
        }
      }
      );

      // end

      // this.messageService.getReturnToolInitialize(this.ssoId, this.toolOrderId).subscribe(data => {
      //   console.log("initialite return tool response");
      //   console.log(data);
      //   console.log("initialite return tool response");
      //   this.initiateRTResponse = data;
      //   this.toolDetails = this.initiateRTResponse.return_parts;
      //   console.log("tool details");
      //   console.log(this.toolDetails);
      //   this.toolDetails.forEach(tool => {
      //     tool.main_data.btn = "Select";
      //   });
      //   /*this.toolDetails.forEach(tool => {
      //     tool.btn = "Select";
      //     if (tool.asset_status_id == 4 || tool.asset_status_id == 12) {
      //       tool.calibrationRequired = true;
      //       this.addElement(tool);
      //     } else {
      //       tool.calibrationRequired = false;
      //     }
      //   });*/
      //   console.log(this.initiateRTResponse);
      //   console.log("initialite return tool response");
      // });      
    });
  }

  selectAll() {

    console.log("the selected data in the select all :" + JSON.stringify(this.selected));

    this.toolDetails.forEach(tool => {
      if (this.allSelect == "Select All") {
        tool.main_data.isAdded = true;
        this.selected.push(tool);
        tool.main_data.btn = "Unselect";
      } else {
        tool.main_data.isAdded = false;
        this.selected = this.selected.filter(obj => obj !== tool);
        tool.main_data.btn = "Select";
      }
    });
    if (this.allSelect == "Select All") {
      this.allSelect = "Unselect All";
    } else {
      this.allSelect = "Select All";
    }
  }

  public addElement(tool) {

    console.log("the tool Details :" + JSON.stringify(this.toolDetails));
    console.log("the selected data check :" + JSON.stringify(this.selected));
    console.log("the tool object data:" + JSON.stringify(tool));
    console.log("the tool .main data" + JSON.stringify(tool.main_data));

    var main_data = tool.main_data;

    console.log("the main data :" + JSON.stringify(main_data));

    var contains = this.containsElement(main_data);
    if (contains) {
      this.allSelect = "Select All"
      main_data.isAdded = false;
      this.selected = this.selected.filter(obj => obj !== tool);
      console.log("the selected data in the add element" + JSON.stringify(this.selected));
      main_data.btn = "Select";
    } else {

      main_data.isAdded = true;
      this.selected.push(tool);
      if (this.selected.length == this.toolDetails.length)
        this.allSelect = "Unselect All"
      main_data.btn = "Unselect";
    }

    console.log("the tool object" + JSON.stringify(tool));
    console.log("the selected data in the add elemet" + JSON.stringify(this.selected));
  }

  /*
  
  var main_data = tool.main_data;
  var contains = this.containsElement(main_data);
  if (contains) {
    tool.main_data.isAdded = false;
    this.selected = this.selected.filter(obj => obj !== main_data);
    this.allSelect = "Select All";
    main_data.btn = "select";
  } else {
    main_data.isAdded = true;
    main_data.btn = "Unselect";
    this.selected.push(tool);
     if (this.selected.length == this.toolDetails.length)
      this.allSelect = "Unselect All"; 
  } 
  
  */

  public containsElement(mainData) {
    console.log("Selected Items ");
    console.log(this.selected);
    console.log("Selected Items ");
    console.log("Passed tool");
    console.log(mainData);
    console.log("Passed tool");
    var contains: boolean = false;
    this.selected.forEach(s1 => {
      if (s1.main_data.part_number == mainData.part_number) {
        contains = true;
      }
    });
    return contains;
  }

  continueReturn() {

    let toolHealth1: any = [];

    this.toolDetails.forEach(element => {
      console.log(element);

      if (
        (element.health_data[0].health == "2" &&
          element.health_data[0].remarks == null) ||
        (element.health_data[0].health == "2" &&
          element.health_data[0].remarks == "")
      ) {
        console.log("found defect without remarks");

        toolHealth1.push(element.health_data[0].part_number);
      }
    });

    if (toolHealth1.length != 0) {
      alert(
        "Please write the remarks for the following defective tools - " +
        JSON.stringify(toolHealth1)
      );
    } else {
      console.log(this.initiateRTResponse);

      this.navCtrl.push(ReturnToolPage, { selected: this.selected, returnToolResponse: this.initiateRTResponse });
    }
  }

  changedStatus(tool) {
    this.toolHealth = tool;
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService.generateRefreshToken().then(() => {
        resolve("succes");
        //return("succes");
      }).catch(e => {
        //return(e);
        reject(e);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FetoolreturntoolPage');
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      //this.detailText = "Show Tool Detail";
      this.shownGroup = null;
      //alert(this.detailText);
    } else {
      //this.detailText = "Hide Tool Details";
      this.shownGroup = group;
      //alert(this.detailText);
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };

}
