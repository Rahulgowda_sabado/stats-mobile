import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScanToolPage } from './scan-tool';

@NgModule({
  declarations: [
    ScanToolPage,
  ],
  imports: [
    IonicPageModule.forChild(ScanToolPage),
  ],
})
export class ScanToolPageModule {}
