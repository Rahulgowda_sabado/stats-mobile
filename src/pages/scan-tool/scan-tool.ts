import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { ReturnToolPage } from "../../pages/return-tool/return-tool";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { ScannedToolReturnPage } from "../scanned-tool-return/scanned-tool-return";
import { ScancodePage } from "../scancode/scancode";
//import { assert } from 'ionic-angular/util/util';

@IonicPage()
@Component({
  selector: "page-scan-tool",
  templateUrl: "scan-tool.html"
})
export class ScanToolPage {
  isAdded: Boolean;
  scanEnabled: any;
  globals: Globals;
  tools: any = {};
  allSelect = "Select All";
  toolDetails = [];
  nonScannedTools = [];
  db_tools = [];
  selected = [];
  sso_id = "";
  asset_number = "";
  toolOrderId = "";
  initiateRTResponse: any;
  shownGroup = null;
  detailText = null;
  responseData: any;
  error_msg = "";
  order = 1;
  country_id = "";
  success_msg = "";
  scannedtool = []; // list of tools which are scanned or selected earlier
  selected_code = [];
  initiateRTResponse_scancode: any;
  isenabled: boolean = false;
  nonScannedTools_code = [];
  tempScanTool = [];
  tempNonScanTool = [];
  notOwnedByYouError: any = 1;
  alreadyScannedError: any = 2;
  toolDetailsNav: any;
  breakVariable: boolean = true;
  loopElement: any;
  enablecontinue: any;
  loading: any;
  constructor(
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id = country_id;
    });
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.sso_id = sso_id;
      this.asset_number = navParams.get("scannedCode");
      this.scannedtool = navParams.get("scannedtool");
      console.log(
        "the scaned tool in condtructor from the nav params ************   " +
        JSON.stringify(this.scannedtool)
      );
      this.initiateRTResponse = navParams.get("initiateRTResponseNav");
      this.toolDetailsNav = navParams.get("toolDetails");
      if (
        this.toolDetailsNav == "undefined" ||
        this.toolDetailsNav == undefined
      ) {
        console.log("inside if");
        console.log("before tool details :" + JSON.stringify(this.toolDetails));
        this.toolDetails = this.toolDetails;
        console.log("after tool details :" + JSON.stringify(this.toolDetails));
      } else {
        console.log("inside else");
        console.log("before tool details :" + JSON.stringify(this.toolDetails));
        this.toolDetails = this.toolDetailsNav;
        console.log("after tool details :" + JSON.stringify(this.toolDetails));

        console.log("JSON string data    " + JSON.stringify(this.toolDetails));
        console.log(
          "length from the parameters           " + this.toolDetails.length
        );
      }

      //this.toolDetails =navParams.get('toolDetailsNav');

      this.tempNonScanTool = navParams.get("nonScannedTools");

      console.log("the scanned_asset");
      console.log(this.asset_number);
      // console.log("the scanned tool:"+this.scannedtool.length);

      if (this.scannedtool.length == 0) {
        // first time scanned
        this.messageService
          .getScannedData(this.sso_id, this.asset_number, this.country_id)
          .then(data => {
            this.loading.dismiss();
            if (data.status === 401) {
              throw new Error("Unauthorized");
            }
            this.initiateRTResponse = JSON.parse(data);
            console.log(
              "the scanned data for the first time scanned" +
              JSON.stringify(this.initiateRTResponse)
            );
            if (this.initiateRTResponse.transaction_status == 1) {
              console.log(
                "the scanned data for the first time scanned again again " +
                JSON.stringify(this.initiateRTResponse)
              );
              this.db_tools = this.initiateRTResponse.transaction_des.return_parts;
              console.log(
                "the scanned data for the db tools " +
                JSON.stringify(this.db_tools)
              );
              this.db_tools.forEach(sam => {
                if (sam.main_data.asset_number == this.asset_number) {
                  console.log("inside scan");
                  sam.main_data.return_status = 1;
                  sam.main_data.isAdded = true;
                  console.log("inside scan1");
                  // this.selected.push(sam);
                  console.log(
                    "the scanned tool in the scan tool == 0 case :" +
                    JSON.stringify(this.selected)
                  );
                  sam.main_data.btn = "UnSelect";
                  sam["health_data"].forEach(healthData => {
                    healthData.health = healthData.asset_condition_id;
                  });
                  console.log("inside scan3");
                  console.log(
                    "the selected data at the end in the if condition scantool==0 case :" +
                    JSON.stringify(this.selected)
                  );
                  this.scannedtool.push(sam);
                  console.log("inside scan4");
                  console.log(
                    "the scanned data for the first time scanned" +
                    JSON.stringify(this.scannedtool)
                  );
                } else {
                  console.log("inside scan5");
                  console.log(
                    "this.non scaneed tools" +
                    JSON.stringify(this.nonScannedTools)
                  );
                  sam.main_data.return_status = 1;
                  sam.main_data.isAdded = false;
                  // this.selected = this.selected.filter(obj => obj !== sam);
                  sam.main_data.btn = "Select";
                  sam["health_data"].forEach(healthData => {
                    healthData.health = healthData.asset_condition_id;
                  });
                  console.log("inside scan6");
                  this.nonScannedTools.push(sam);
                  console.log("inside scan7");
                  console.log(
                    "the non scanned data for first time  " +
                    JSON.stringify(this.nonScannedTools)
                  );
                }
              });

              console.log(
                "end of for loop scanned tool" +
                JSON.stringify(this.scannedtool)
              );
              console.log(
                "end of for loop non scanned tool" +
                JSON.stringify(this.nonScannedTools)
              );
              console.log(
                "the tool details before mapping :" +
                JSON.stringify(this.toolDetails)
              );
              const newArray = this.scannedtool
                .map(o => {
                  return { main_data: o.main_data, health_data: o.health_data };
                })
                .forEach(o => this.toolDetails.push(o));
              const newArray1 = this.nonScannedTools
                .map(p => {
                  return { main_data: p.main_data, health_data: p.health_data };
                })
                .forEach(o => this.toolDetails.push(o));
              console.log(
                "tool details after maping" + JSON.stringify(this.toolDetails)
              );
              this.fun(this.scannedtool, this.toolDetails);
            } else {
              alert(this.initiateRTResponse.transaction_des);
              this.navCtrl.push(HomePage);
            }
          })
          .catch((e: any) => {
            this.loading.dismiss();
            var error = e.message;
            // console.log("Error from http header"+error);
            // console.log("Error from http header status text"+error);

            if (error == "Unauthorized") {
              console.log("code matched and call the api");
              this.refreshPromise().then(
                success => {
                  this.messageService
                    .getScannedData(
                      this.sso_id,
                      this.asset_number,
                      this.country_id
                    )
                    .then(data => {
                      console.log("initialite return tool response");
                      console.log(data);
                      console.log("initialite return tool response");
                      this.initiateRTResponse = JSON.parse(data);

                      if (this.initiateRTResponse.transaction_status == 1) {
                        this.db_tools = this.initiateRTResponse.transaction_des.return_parts;
                        this.db_tools.forEach(sam => {
                          if (sam.main_data.asset_number == this.asset_number) {
                            console.log("inside scan");
                            sam.main_data.return_status = 1;
                            sam.main_data.isAdded = true;
                            console.log("inside scan1");
                            // this.selected.push(sam);
                            sam.main_data.btn = "UnSelect";
                            sam["health_data"].forEach(healthData => {
                              healthData.health = healthData.asset_condition_id;
                            });
                            console.log("inside scan3");
                            this.scannedtool.push(sam);
                            console.log("inside scan4");
                            console.log(
                              "the scanned data for the first time scanned" +
                              JSON.stringify(this.scannedtool)
                            );
                          } else {
                            console.log("inside scan5");
                            console.log(
                              "this.non scaneed tools" +
                              JSON.stringify(this.nonScannedTools)
                            );
                            sam.main_data.return_status = 1;
                            sam.main_data.isAdded = false;
                            // this.selected = this.selected.filter(obj => obj !== sam);
                            sam.main_data.btn = "Select";
                            sam["health_data"].forEach(healthData => {
                              healthData.health = healthData.asset_condition_id;
                            });
                            console.log("inside scan6");
                            this.nonScannedTools.push(sam);
                            console.log("inside scan7");
                            console.log(
                              "the non scanned data for first time  " +
                              JSON.stringify(this.nonScannedTools)
                            );
                          }
                        });
                        console.log(
                          "end of for loop scanned tool" +
                          JSON.stringify(this.scannedtool)
                        );
                        console.log(
                          "end of for loop non scanned tool" +
                          JSON.stringify(this.nonScannedTools)
                        );
                        console.log(
                          "the tool details before mapping :" +
                          JSON.stringify(this.toolDetails)
                        );
                        const newArray = this.scannedtool
                          .map(o => {
                            return {
                              main_data: o.main_data,
                              health_data: o.health_data
                            };
                          })
                          .forEach(o => this.toolDetails.push(o));
                        const newArray1 = this.nonScannedTools
                          .map(p => {
                            return {
                              main_data: p.main_data,
                              health_data: p.health_data
                            };
                          })
                          .forEach(o => this.toolDetails.push(o));
                        console.log(
                          "tool details after maping" +
                          JSON.stringify(this.toolDetails)
                        );
                        this.fun(this.scannedtool, this.toolDetails);
                        this.navCtrl.setRoot(HomePage);
                      } else {
                        this.error_msg = this.initiateRTResponse.transaction_des;
                        this.success_msg = "";
                      }

                      console.log("data prepared by maruthi" + this.db_tools);
                      console.log(this.initiateRTResponse);
                      console.log("initialite return tool response");
                    });
                },
                error => {
                  console.log(
                    "error at Calling the Refresh Token" + JSON.stringify(error)
                  );
                  this.storage.set(this.globals.role_id, null);
                  this.navCtrl.push(HomePage);
                }
              );
            }
          });

        this.fun(this.scannedtool, this.toolDetails); // extra added
      } // next time scanned
      else {
        //this.notOwnedByYouError = 2;
        this.loading.dismiss();
        console.log(
          "in else part scanned tool first " + JSON.stringify(this.scannedtool)
        );

        this.selected = [];

        this.selected = this.scannedtool;

        console.log(
          "the check of the selected data :" + JSON.stringify(this.selected)
        );

        console.log(
          "in the else partn non  scanned tool sirst " +
          JSON.stringify(this.tempNonScanTool)
        );

        this.nonScannedTools = [];

        console.log(
          "the check of the nonScannedTools:" +
          JSON.stringify(this.nonScannedTools)
        );

        this.scannedtool.forEach(san => {
          if (san.main_data.asset_number == this.asset_number) {
            console.log("the asset  is already scanned");
            alert("the asset  is already scanned");
            this.alreadyScannedError = 1;
            this.selected = this.scannedtool;
            this.nonScannedTools = this.tempNonScanTool;
            console.log(
              "already scanned error seleected" + JSON.stringify(this.selected)
            );
            console.log(
              "already scanned error tool details" +
              JSON.stringify(this.toolDetails)
            );
          }
        });
        if (this.alreadyScannedError == 2) {
          this.tempNonScanTool.forEach(non => {
            if (non.main_data.asset_number == this.asset_number) {
              this.notOwnedByYouError = 2;
              console.log("looop");
              non.main_data.return_status = 1;
              non.main_data.isAdded = true;
              this.selected.push(non);
              console.log(
                "the selected data in the tempNonscanTool :" +
                JSON.stringify(this.selected)
              );
              non.main_data.btn = "UnSelect";
              non["health_data"].forEach(healthData => {
                healthData.health = healthData.asset_condition_id;
              });
              // this.nonScannedTools = this.tempNonScanTool.filter(obj => obj !== non);
              //this.scannedtool.push(non);
              console.log(
                "before non pushed to scanned tool" +
                JSON.stringify(this.scannedtool)
              );
              console.log(
                "before non pushed to selected tool" +
                JSON.stringify(this.selected)
              );
              //this.scannedtool.push(non);
              this.scannedtool = [];
              this.scannedtool = this.selected;
              console.log(
                "after non pushed to scanned tool" +
                JSON.stringify(this.scannedtool)
              );
              console.log(
                "after non pushed to scanned tool" +
                JSON.stringify(this.selected)
              );
            } else {
              non.main_data.return_status = 1;
              non.main_data.isAdded = false;
              this.selected = this.selected.filter(obj => obj !== non);
              non.main_data.btn = "Select";
              non["health_data"].forEach(healthData => {
                healthData.health = healthData.asset_condition_id;
              });
              console.log("inside scan6");
              this.nonScannedTools.push(non);
              console.log("inside scan7");
              console.log(
                "the non scanned data for first time " +
                JSON.stringify(this.nonScannedTools)
              );
            }
          });
          console.log(
            "else scanned tool push from non scanned toll last " +
            JSON.stringify(this.scannedtool)
          );
          console.log(
            "else non scanned tool push from non scanned toll last" +
            JSON.stringify(this.selected)
          );
          console.log(
            "else non scanned data :" + JSON.stringify(this.nonScannedTools)
          );

          this.fun(this.scannedtool, this.toolDetails);

          if (this.notOwnedByYouError == 1) {
            alert("Sorry this" + this.asset_number + "is now owned by you");
            this.selected = this.scannedtool;
            this.nonScannedTools = this.tempNonScanTool;
            // this.fun(this.scannedtool,this.toolDetails);
          } else {
            console.log(
              "before else case tool details" + JSON.stringify(this.toolDetails)
            );
            this.toolDetails = [];
            console.log(
              "after empty case tool details" + JSON.stringify(this.toolDetails)
            );
            console.log(
              "else scanned tool push from non scanned toll last again  " +
              JSON.stringify(this.selected)
            );
            console.log(
              "else non scanned tool push from non scanned toll last again " +
              JSON.stringify(this.selected)
            );
            const Array = this.scannedtool
              .map(o => {
                return { main_data: o.main_data, health_data: o.health_data };
              })
              .forEach(o => this.toolDetails.push(o));
            const Array1 = this.nonScannedTools
              .map(p => {
                return { main_data: p.main_data, health_data: p.health_data };
              })
              .forEach(o => this.toolDetails.push(o));
            console.log(
              "the final tool detailss at the end of else case:" +
              JSON.stringify(this.toolDetails)
            );
          }
        }
      }

      this.fun(this.scannedtool, this.toolDetails);
    });
  }

  /*  selectAll() 
{ 

  console.log("check the empty array"+ JSON.stringify(this.scannedtool));

  this.scannedtool=[];

    this.toolDetails.forEach(tools => 
    { 
      if (this.allSelect == "Select All") 
      {
        tools.main_data.isAdded = true;
        this.scannedtool.push(tools);
        console.log("the scanned tool data in the if part"+JSON.stringify(this.scannedtool));
        tools.main_data.btn = "Unselect";
      } 
      else 
      {
        tools.main_data.isAdded = false;
        this.selected = this.selected.filter(obj => obj !== tools);
        console.log("the scanned tool data in the else part "+JSON.stringify(this.selected));
        tools.main_data.btn = "Select";
      }

    });



    this.fun(this.scannedtool,this.toolDetails);
    
    console.log("the tool details :"+JSON.stringify(this.toolDetails));

    console.log("after the code completed"+JSON.stringify(this.scannedtool));

    if (this.allSelect == "Select All")
    {
      this.allSelect = "Unselect All";
    } 
    else 
    {
      this.allSelect = "Select All";
    }

    
}
 */

  selectAll() {
    this.loopElement = 0;

    this.toolDetails.forEach(tools => {
      if (this.allSelect == "Select All") {
        tools.main_data.isAdded = true;

        if (this.loopElement == 0) {
          this.scannedtool = [];
          this.selected = [];
          this.nonScannedTools = [];
        }
        this.scannedtool.push(tools);
        this.selected.push(tools);

        console.log(
          "the non scanned tools in the select all button : " +
          JSON.stringify(this.nonScannedTools)
        );
        console.log(
          "the scanned tools in the select all button : " +
          JSON.stringify(this.scannedtool)
        );

        tools.main_data.btn = "Unselect";
      } else {
        tools.main_data.isAdded = false;
        this.selected.push(tools);

        if (this.loopElement == 0) {
          this.nonScannedTools = [];
          this.selected = [];
          this.scannedtool = [];
        }
        this.nonScannedTools.push(tools);

        console.log(
          "the scanned tools in the select all button : " +
          JSON.stringify(this.scannedtool)
        );
        console.log(
          "the non scanned tools in the select all button : " +
          JSON.stringify(this.nonScannedTools)
        );

        tools.main_data.btn = "Select";
      }
      this.loopElement++;
    });
    if (this.allSelect == "Select All") {
      this.allSelect = "Unselect All";
    } else {
      this.allSelect = "Select All";
    }

    this.fun(this.scannedtool, this.toolDetails);
  }

  public addElement(tool) {
    console.log("the selected data check :" + JSON.stringify(this.selected));

    var main_data = tool.main_data;

    var contains = this.containsElement(main_data);

    if (contains) {
      main_data.isAdded = false;
      this.nonScannedTools.push(tool);
      console.log("the tool obj before :" + JSON.stringify(tool));
      console.log(
        "the selected data contained in the add element before if part" +
        JSON.stringify(this.selected)
      );
      console.log(
        "the scanned data contained in the add element before if part" +
        JSON.stringify(this.scannedtool)
      );

      /*this.selected = this.selected.filter(obj=> obj !== tool);
    this.scannedtool=this.scannedtool.filter(obj=>obj !== tool); */

      this.scannedtool = this.selected.filter(obj => obj !== tool);

      console.log(
        "the selected data contained in the add element if part" +
        JSON.stringify(this.selected)
      );
      console.log(
        "the scanned data contained in the add element if part" +
        JSON.stringify(this.scannedtool)
      );
      console.log(
        "the nonscanned data contained in the addElement if part " +
        JSON.stringify(this.nonScannedTools)
      );

      // this.allSelect = "Unselect All";
      main_data.btn = "Select";
    } else {
      main_data.isAdded = true;
      this.scannedtool.push(tool);
      this.selected.push(tool);
      this.nonScannedTools = this.nonScannedTools.filter(obj => obj !== tool);
      console.log(
        "the nonscanned data contained in the addElement else part " +
        JSON.stringify(this.nonScannedTools)
      );
      console.log(
        "the scanned data contained in the add element else part " +
        JSON.stringify(this.scannedtool)
      );
      main_data.btn = "Unselect";
    }

    /*  if (this.allSelect == "Select All") {
       this.allSelect = "Unselect All";
     } else {
       this.allSelect = "Select All";
     } */

    console.log("the tool object" + JSON.stringify(tool));
    console.log(
      "the scanned data in the add elemet" + JSON.stringify(this.scannedtool)
    );
    console.log(
      "the selected data at the end  :" + JSON.stringify(this.selected)
    );
    console.log(
      "the tool Details data in the add element" +
      JSON.stringify(this.toolDetails)
    );

    this.fun(this.scannedtool, this.toolDetails);
  }

  public fun(selectdObject, toolDetailsObject) {
    console.log("diable scan block");
    console.log("the selected data :" + JSON.stringify(this.selected));
    console.log("the updated scan tool :" + JSON.stringify(this.scannedtool));
    console.log(
      "the updated non scanned tool" + JSON.stringify(this.nonScannedTools)
    );

    selectdObject = this.scannedtool;
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log(selectdObject.length);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@");

    toolDetailsObject = this.toolDetails;
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@");
    console.log(toolDetailsObject.length);
    console.log("@@@@@@@@@@@@@@@@@@@@@@@@");

    if (selectdObject.length == toolDetailsObject.length) {
      this.scanEnabled = 1;
    } else {
      this.scanEnabled = 2;
    }

    if (selectdObject.length > 0) {
      this.enablecontinue = 1;
    } else {
      this.enablecontinue = 2;
    }
  }

  public containsElement(mainData) {
    console.log("scanned data");
    console.log(this.scannedtool);

    console.log("selected data");
    console.log(this.selected);

    console.log("Passed tool");
    console.log(mainData);

    var contains: boolean = false;
    //console.log("the selected data in the contain :"+JSON.stringify(this.scannedtool));
    this.scannedtool.forEach(s1 => {
      if (s1.main_data.asset_number == mainData.asset_number) {
        console.log("the matching data");
        contains = true;
      }
    });
    return contains;
  }

  continueReturn() {

    let toolHealth1: any = [];
    this.toolDetails.forEach(element => {
      if (
        ((element.health_data[0].health == "3" && element.health_data[0].remarks == "") || (element.health_data[0].health == "3" && element.health_data[0].remarks == null))
        || ((element.health_data[0].health == "2" && element.health_data[0].remarks == "") || (element.health_data[0].health == "2" && element.health_data[0].remarks == null))) {
        console.log("found defect without remarks");
        toolHealth1.push(element.health_data[0].part_number);
      }
    });

    if (toolHealth1.length != 0) {
      alert(
        "Please write the remarks for the following tools - " +
        JSON.stringify(toolHealth1)
      );
    } else {
      console.log(
        "the selected values of the tool" + JSON.stringify(this.scannedtool)
      );
      this.navCtrl.push(ScannedToolReturnPage, {
        selected: this.scannedtool,
        returnToolResponse: this.initiateRTResponse
      });
    }
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  Scantool() {
    console.log("beforre sending to nav params all data");

    console.log(
      "beforre sending to nav params The scanned data" +
      JSON.stringify(this.selected)
    );

    console.log(
      "beforre sending to nav params rest api data" +
      JSON.stringify(this.initiateRTResponse)
    );

    console.log(
      "beforre sending to nav params the nonScannedTools" +
      JSON.stringify(this.nonScannedTools)
    );

    console.log(
      "beforre sending to nav params the html data" +
      JSON.stringify(this.toolDetails)
    );

    this.navCtrl.push(ScancodePage, {
      scannedtool: this.scannedtool,
      initiateRTResponse: this.initiateRTResponse,
      nonScannedTools: this.nonScannedTools,
      toolDetails: this.toolDetails
    });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      //this.detailText = "Show Tool Detail";
      this.shownGroup = null;
      //alert(this.detailText);
    } else {
      //this.detailText = "Hide Tool Details";  `x
      this.shownGroup = group;
      //alert(this.detailText);
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }
}
