import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { MenuPage } from "../menu/menu";

@IonicPage()
@Component({
  selector: "page-return-tool",
  templateUrl: "return-tool.html"
})
export class ReturnToolPage {
  globals: Globals;
  ssoId = "";
  systemId = "";
  install_base_id = "";
  warehouseId: string = "";
  addressChange = "";
  selected = [];
  address1 = "";
  address2 = "";
  city = "";
  state = "";
  pincode = "";
  error_msg = "";
  success_msg = "";
  pickupPoint: any;
  orderNum: any = "";
  returnType: any;
  returnRTMasterResponse: any;
  warehousesInfo: any;
  orderIdStatus: string = "";
  orderIdStatusColor: string = "";
  siteId: string = "";
  responseData: any;
  whId: any;
  country_id = "";
  order_info: any;
  click: any;
  db_pickupPoint: any;
  oahAndOrderedAssetId = {};
  whId1: any;
  shipByInfo: any;
  role_id: any;
  loading: any;
  newOrderNum: any;
  whInfo: any;

  main_address1 = "";
  main_address2 = "";
  main_city = "";
  main_state = "";
  main_pincode = "";
  main_siteId = "";
  main_install_base_id = "";
  selected_ordered_to_wh: any;
  pickUpWhs: any;
  finalSystemId = "";
  sysError = "";
  orderedAssetStatus = {};
  feOrderList: any = [];
  feOrderId: any = "";
  
  returnOrderPref:boolean = false;
  return_order_notes:any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider
  ) {
    console.log("nav params" + navParams);
    console.log("nav params" + JSON.stringify(navParams));
    this.pickupPoint = "1";
    this.globals = Globals.getInstance();
    this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id = country_id;
    });
    this.storage.get(this.globals.role_id).then(role_id => {
      this.role_id = role_id;
      // Hard coded values
      // this.role_id = "5";
    });
    this.click = "0";
    this.selected = navParams.get("selected");
    console.log("++++++++++1" + JSON.stringify(this.selected));
    this.returnRTMasterResponse = navParams.get("returnToolResponse");
    console.log("++++++++++" + JSON.stringify(this.returnRTMasterResponse));

    this.order_info = this.returnRTMasterResponse.order_info;
    // to select by default values of address from order address
    this.pickupPoint = this.order_info.order_delivery_type_id;

    this.pickUpWhs = this.returnRTMasterResponse.ship_by_data;

    this.address1 = this.order_info.address1;
    this.address2 = this.order_info.address2;
    this.city = this.order_info.address3;
    this.state = this.order_info.address4;
    this.pincode = this.order_info.pin_code;
    this.systemId = this.order_info.system_id;
    this.install_base_id = this.order_info.install_base_id;
    this.finalSystemId = this.order_info.system_id;

    this.siteId = this.order_info.site_id;

    this.main_address1 = this.order_info.address1;
    this.main_address2 = this.order_info.address2;
    this.main_city = this.order_info.address3;
    this.main_state = this.order_info.address4;
    this.main_pincode = this.order_info.pin_code;
    this.main_siteId = this.order_info.site_id;
    this.main_install_base_id = this.order_info.install_base_id;

    this.whId = this.returnRTMasterResponse.wh_id;
    console.log("whID:" + this.whId);

    this.warehousesInfo = this.returnRTMasterResponse.all_wh_data;
    this.shipByInfo = this.returnRTMasterResponse.ship_by_wh_data;
    this.whInfo = this.returnRTMasterResponse.wh_data;
    this.selected_ordered_to_wh = this.returnRTMasterResponse.selected_ordered_to_wh;
    this.whId1 = this.returnRTMasterResponse.selected_ordered_to_wh;
    this.returnOrderPref = this.returnRTMasterResponse.returnOrderPref;
    console.log("rteturnOrderap"+this.returnOrderPref); 

    /* console.log("return tools main page"); */
    /* console.log("this selected values");
    console.log(this.selected); */
    /*  console.log("this selected values");
     console.log("maruti response"); */
    /* console.log(this.returnRTMasterResponse); */
    /* console.log("maruti response"); */
    console.log(this.warehousesInfo);
    //console.log("return tools main page"); */
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
    });
  }

  /*  presentAlert(titleInfo: string, subTitleInfo: string) {
     const alert = this.alertCtrl.create({
       title: titleInfo,
       subTitle: subTitleInfo,
       buttons: ['OK']
     });
     alert.present();
     this.navCtrl.pop();
   }
  */
  onReturnTools() {
    this.error_msg = "";

    console.log("-----" + this.whId);

    if (this.pickupPoint === undefined || this.pickupPoint == "") {
      this.error_msg = "Please Select the Pickup Point";
    } else if (this.pickupPoint == "1" && this.systemId == "") {
      this.error_msg = "Please enter the system Id";
    } else if (
      (this.pickupPoint == "3" ||
        (this.pickupPoint == "1" && this.click == "1")) &&
      (this.pincode == "" ||
        this.pincode === undefined ||
        (this.state == "" || this.state === undefined) ||
        (this.city == "" || this.city === undefined) ||
        (this.address2 == "" || this.address2 === undefined) ||
        (this.address1 == "" || this.address1 === undefined))
    ) {
      this.error_msg = "Please fill all the details";
    } else if (this.returnType === undefined || this.returnType == "") {
      this.error_msg = "Please Select the return type";
    } else if (
      (this.returnType == "1" || this.returnType == "2") &&
      (this.whId === undefined || this.whId == "")
    ) {
      //alert(this.warehouseId+"--"+this.whId);
      this.error_msg = "Please Select the warehouse";
    } else if (
      this.role_id == "5" &&
      (this.pickupPoint == "1" || this.pickupPoint == "3") &&
      (this.whId1 === undefined || this.whId1 == "")
    ) {
      //alert(this.warehouseId+"--"+this.whId);
      this.error_msg = "Please Select ShipBy warehouse";
    } else if (this.feOrderId === false) {
      this.error_msg = "No Records Found"
    } else if (
      (this.returnType == "3" || this.returnType == "4") &&
      (this.orderNum == "" || this.orderNum == undefined)
    ) {
      this.error_msg = "Please Enter FE2 order number ";
    } else if (this.finalSystemId != this.systemId && this.pickupPoint == "1") {
      this.error_msg = "Please Validate the changed  system ID ";
    } else if (
      this.sysError == "Invalid system id" &&
      this.pickupPoint == "1"
    ) {
      this.error_msg = "Invalid system id";
    } else if (
      this.newOrderNum != this.orderNum &&
      (this.returnType == "3" || this.returnType == "4")
    ) {
      this.error_msg = "Please Validate the changed Order Number";
    } else if (
      this.orderIdStatus != "Success" &&
      (this.returnType == "3" || this.returnType == "4")
    ) {
      // this.error_msg = "Invalid Order Number";
    } else {
      console.log(
        "******************The selected from the return tools ******************"
      );
      console.log(this.selected);

      this.convertLogic();
    }
  }

  convertLogic() {
    this.clean();
    var order_status_id = this.returnRTMasterResponse.order_status_id;
    var tool_order_id = this.returnRTMasterResponse.tool_order_id;
    var owned_assets_count = this.returnRTMasterResponse.owned_asset_count;
    /*   var values =
      '{"order_status_id":"' +
      this.returnRTMasterResponse.order_status_id +
      '","tool_order_id":"' +
      tool_order_id +
      '","owned_assets_count":"' +
      owned_assets_count +
      '","checkAll":"' +
      "on" +
      '","oah_id":{},"assetAndOrderedAsset":{},"oah_condition_id":{},"oah_oa_health_id_part_id":{},"oa_health_id":{},"remarks":{},' +
      '"delivery_type_id":"' +
      this.pickupPoint +
      '","zonal_wh_id":"' +
      this.whId1 +
      '","system_id":"' +
      this.systemId +
      '","site_id":"' +
      this.siteId +
      '",' +
      '"check_address":"' +
      this.click +
      '","address_remarks":"",' +
      '"from_wh_id":"' +
      this.selected_ordered_to_wh +
      '","address_1":"' +
      this.address1.trim() +
      '",' +
      '"address_2":"' +
      this.address2.trim() +
      '","address_3":"' +
      this.city.trim() +
      '",' +
      '"address_4":"' +
      this.state.trim() +
      '","pin_code":"' +
      this.pincode.trim() +
      '",' +
      '"return_type_id":"' +
      this.returnType +
      '","to_wh_id":"' +
      this.whId +
      '",' +
      '"sso_id":"' +
      this.ssoId +
      '","order_number":"' +
      this.orderNum +
      '","submit_pickup":"1"' +
      "}"; */

    var values: any = {
      order_status_id: this.returnRTMasterResponse.order_status_id,

      tool_order_id: tool_order_id,

      owned_assets_count: owned_assets_count,

      checkAll: "on",

      oah_id: {},

      assetAndOrderedAsset: {},

      oah_condition_id: {},

      oah_oa_health_id_part_id: {},

      oa_health_id: {},

      remarks: {},

      delivery_type_id: this.pickupPoint,

      zonal_wh_id: this.whId1,

      system_id: this.systemId,
      install_base_id: this.install_base_id,

      site_id: this.siteId,

      check_address: this.click,

      address_remarks: "",

      from_wh_id: this.selected_ordered_to_wh,

      address_1: this.address1.trim(),

      address_2: this.address2.trim(),

      address_3: this.city.trim(),

      address_4: this.state.trim(),

      pin_code: this.pincode.trim(),

      return_type_id: this.returnType,

      to_wh_id: this.whId,

      sso_id: this.ssoId,

      order_number: this.feOrderId,
      device_type: this.globals.deviceName,

      submit_pickup: "1",
      return_order_notes:this.return_order_notes
    };
    var jsObj = values;
    //  var jsObj = JSON.parse(values);
    console.log("data" + jsObj);

    console.log("data" + JSON.stringify(jsObj));

    this.selected.forEach(element => {
      var healthObj = "";
      var mainData = element.main_data;
      var oahId = mainData.oah_id;
      var orderStatusId = mainData.ordered_asset_id;
      var assetId = mainData.asset_id;
      var orderedAssetId = mainData.ordered_asset_id;
      var returnStatus = mainData.return_status;
      jsObj.oah_id[oahId] = orderStatusId;
      jsObj.assetAndOrderedAsset[orderedAssetId] = orderedAssetId;
      jsObj.oah_condition_id[orderedAssetId] = returnStatus;
      jsObj.oah_oa_health_id_part_id[oahId] = {};
      jsObj.oa_health_id[oahId] = {};
      jsObj.remarks[oahId] = {};
      var healthData = element.health_data;
      console.log("Maruthi Health" + healthData);
      healthData.forEach(healthElement => {
        var oaHealthId = healthElement.oa_health_id;
        var partIdVal = healthElement.part_id;
        var remarkwa = healthElement.remark;
        var healthStatus = healthElement.health;
        jsObj.oah_oa_health_id_part_id[oahId][oaHealthId] = [];
        jsObj.oa_health_id[oahId][oaHealthId] = {};
        jsObj.remarks[oahId][oaHealthId] = {};
        jsObj.oa_health_id[oahId][oaHealthId][partIdVal] = healthStatus;
        jsObj.remarks[oahId][oaHealthId][partIdVal] =
          remarkwa == null ? "" : remarkwa;
        jsObj.oah_oa_health_id_part_id[oahId][oaHealthId].push(partIdVal);
      });
    });

    this.selected.forEach(element => {
      var mainData = element.main_data;
      var oahId = mainData.oah_id;
      jsObj.oah_condition_id;
    });

    console.log("the jsobj");
    console.log(jsObj);

    let alert1 = this.alertCtrl.create({
      title: "Submission of Return",
      message: "Are you sure you want to submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert1.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until Return Request gets displayed ....."
              });
              loading.present();
              this.messageService
                .returnToolRequest(jsObj)
                .then(data => {
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }

                  // loading.dismiss();
                  //var res = JSON.stringify(data);
                  this.responseData = JSON.parse(data);
                  if (this.responseData.transaction_status == 1) {
                    loading.dismiss().then(() => {
                      const alert = this.alertCtrl.create({
                        title: "Return Tools",
                        subTitle: this.responseData.transaction_des,
                        buttons: ["OK"]
                      });
                      alert.present();
                      // this.navCtrl.pop();
                      this.navCtrl.setRoot(MenuPage);
                    });
                  } else {
                    this.error_msg = this.responseData.transaction_des;
                    this.success_msg = "";
                  }
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error == "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .returnToolRequest(jsObj)
                          .then(data => {
                            // loading.dismiss();
                            this.responseData = JSON.parse(data);
                            if (this.responseData.transaction_status == 1) {
                              loading.dismiss().then(() => {
                                const alert = this.alertCtrl.create({
                                  title: "Return Tools",
                                  subTitle: this.responseData.transaction_des,
                                  buttons: ["OK"]
                                });
                                alert.present();
                                // this.navCtrl.pop();
                                this.navCtrl.setRoot(MenuPage);
                              });
                            } else {
                              loading.dismiss();
                              this.error_msg = this.responseData.transaction_des;
                              this.success_msg = "";
                            }
                          });
                      },
                      error => {
                        loading.dismiss();
                        console.log(
                          "error at Calling the Refresh Token" +
                          JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(MenuPage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert1.present();
    return false;
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  mainAddress() {
    for (var i = 0; i < this.shipByInfo.length; i++) {
      if (this.shipByInfo[i].wh_id == this.selected_ordered_to_wh) {
        this.address1 = this.shipByInfo[i].address1;
        this.address2 = this.shipByInfo[i].address2;
        this.city = this.shipByInfo[i].address3;
        this.state = this.shipByInfo[i].address4;
        this.pincode = this.shipByInfo[i].pin_code;
      }
    }

    /* this.address1 = this.main_address1;
    this.address2 = this.main_address2;
    this.city = this.main_city;
    this.state = this.main_state;
    this.siteId = this.main_siteId; */

    this.clean();
    console.log("main add");
  }

  mainAddress1() {
    this.address1 = this.main_address1;
    this.address2 = this.main_address2;
    this.city = this.main_city;
    this.state = this.main_state;
    this.siteId = this.main_siteId;
    this.clean();
  }

  clearForm() {
    this.error_msg = "";
    this.warehouseId = "";
    this.main_address1 = "";
    this.main_address2 = "";
    this.main_city = "";
    this.main_state = "";
    this.main_pincode = "";
    this.address1 = "";
    this.address2 = "";
    this.city = "";
    this.state = "";
    this.pincode = "";
    this.siteId = "";
  }

  clean() {
    this.error_msg = "";
  }

  pickPointChange(pickPointChange) {
    this.clean();
    if (pickPointChange == 2) {
      for (var i = 0; i < this.shipByInfo.length; i++) {
        if (this.shipByInfo[i].wh_id == this.selected_ordered_to_wh) {
          this.address1 = this.shipByInfo[i].address1;
          this.address2 = this.shipByInfo[i].address2;
          this.city = this.shipByInfo[i].address3;
          this.state = this.shipByInfo[i].address4;
          this.pincode = this.shipByInfo[i].pin_code;
        }
      }
      this.click = 0;
    }
    if (pickPointChange == 1 && this.order_info.order_delivery_type_id) {
      this.main_address1 = this.order_info.address1;
      this.main_address2 = this.order_info.address2;
      this.main_city = this.order_info.address3;
      this.main_state = this.order_info.address4;
      this.main_pincode = this.order_info.pin_code;
      // this.clearForm();
    } else if (pickPointChange == 1) {

      this.clearForm();

    }
    if (pickPointChange == 3) {

      this.clearForm();
      this.click = 0;
    }
    this.db_pickupPoint = this.order_info.order_delivery_type_id;
    if (pickPointChange == 1 || pickPointChange == 3) {
      if (this.db_pickupPoint == pickPointChange) {
        // var wh_data = this.returnRTMasterResponse.wh_data[0];
        // this.warehouseId = wh_data.wh_code + "-" + wh_data.name;
        this.address1 = this.order_info.address1;
        this.address2 = this.order_info.address2;
        this.city = this.order_info.address3;
        this.state = this.order_info.address4;
        this.pincode = this.order_info.pin_code;
        this.systemId = this.order_info.system_id;
        this.siteId = this.order_info.site_id;
        this.install_base_id = this.order_info.install_base_id;
      }
    }

    this.pickupPoint = pickPointChange;
  }

  getCustDet() {
    this.sysError = "";
    this.finalSystemId = this.systemId;
    this.clearForm();
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.messageService
      .getCustomerAvailability(this.systemId, this.ssoId, this.country_id)
      .then(status => {
        //var res = JSON.stringify(status);
        if (status.status === 401) {
          throw new Error("Unauthorized");
        }
        //this.sysErr='';
        this.responseData = JSON.parse(status);
        if (this.responseData == 1) {
          this.error_msg = "";
          this.messageService
            .getCustomerAddress(this.systemId, this.country_id)
            .then(res => {
              console.log("customer dis values");
              console.log("system" + JSON.stringify(system));
              //var res1 = JSON.stringify(res);
              if (res.status === 401) {
                throw new Error("Unauthorized");
              }
              var system = JSON.parse(res);

              this.address1 = system["address1"];
              this.address2 = system["address2"];
              this.city = system["address3"];
              this.state = system["address4"];
              this.pincode = system["zip_code"];
              this.siteId = system["site_id"];
              this.install_base_id = system["install_base_id"];

              this.main_address1 = system["address1"];
              this.main_address2 = system["address2"];
              this.main_city = system["address3"];
              this.main_state = system["address4"];
              this.main_pincode = system["zip_code"];
              this.main_siteId = system["site_id"];
              this.main_install_base_id = system["install_base_id"];
              this.loading.dismiss();

              console.log(system);
            })
            .catch((e: any) => {
              var error = e.message;
              console.log("Error from http header" + error);
              console.log("Error from http header status text" + error);
              if (error == "Unauthorized") {
                console.log("code matched and call the api");
                this.refreshPromise().then(
                  success => {
                    this.messageService
                      .getCustomerAddress(this.systemId, this.country_id)
                      .then(data => {
                        //var res =  JSON.stringify(data);
                        console.log("customer dis values");
                        var system = JSON.parse(data);
                        this.address1 = system["address1"];
                        this.address2 = system["address2"];
                        this.city = system["address3"];
                        this.state = system["address4"];
                        this.pincode = system["zip_code"];
                        this.siteId = system["site_id"];
                        this.install_base_id = system["install_base_id"];

                        this.main_address1 = system["address1"];
                        this.main_address2 = system["address2"];
                        this.main_city = system["address3"];
                        this.main_state = system["address4"];
                        this.main_pincode = system["zip_code"];
                        this.main_siteId = system["site_id"];
                        this.main_install_base_id = system["install_base_id"];

                        console.log(system);
                        this.loading.dismiss();
                      });
                  },
                  error => {
                    console.log(
                      "error at Calling the Refresh Token" +
                      JSON.stringify(error)
                    );
                    this.storage.set(this.globals.role_id, null);
                    this.loading.dismiss();
                    this.navCtrl.push(HomePage);
                  }
                );
              }
            });

          // end
        } else {
          this.error_msg = "Invalid system id";
          this.sysError = "Invalid system id";
          this.siteId = "";
          this.main_address1 = "";
          this.main_address2 = "";
          this.main_city = "";
          this.main_state = "";
          this.main_pincode = "";
          this.main_siteId = "";
          this.main_install_base_id = "";
          this.loading.dismiss();
        }
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error.statusText);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .getCustomerAvailability(
                  this.systemId,
                  this.ssoId,
                  this.country_id
                )
                .then(status => {
                  // var res =  JSON.stringify(status);

                  this.responseData = JSON.parse(status);
                  if (this.responseData == 1) {
                    this.error_msg = "";
                    this.messageService
                      .getCustomerAddress(this.systemId, this.country_id)
                      .then(data => {
                        console.log("customer dis values");
                        //var res = JSON.stringify(data);
                        if (data.status === 401) {
                          throw new Error("Unauthorized");
                        }
                        var system = JSON.parse(data);

                        this.address1 = system["address1"];
                        this.address2 = system["address2"];
                        this.city = system["address3"];
                        this.state = system["address4"];
                        this.pincode = system["zip_code"];
                        this.siteId = system["site_id"];
                        this.install_base_id = system["install_base_id"];

                        this.main_address1 = system["address1"];
                        this.main_address2 = system["address2"];
                        this.main_city = system["address3"];
                        this.main_state = system["address4"];
                        this.main_pincode = system["zip_code"];
                        this.main_siteId = system["site_id"];
                        this.main_install_base_id = system["install_base_id"];
                        this.loading.dismiss();

                        console.log(system);
                      })
                      .catch((e: any) => {
                        var error = e.message;
                        console.log("Error from http header" + error);
                        console.log(
                          "Error from http header status text" + error
                        );
                        if (error == "Unauthorized") {
                          console.log("code matched and call the api");
                          this.refreshPromise().then(
                            success => {
                              this.messageService
                                .getCustomerAddress(
                                  this.systemId,
                                  this.country_id
                                )
                                .then(data => {
                                  console.log("customer dis values");
                                  var system = JSON.parse(data);
                                  this.address1 = system["address1"];
                                  this.address2 = system["address2"];
                                  this.city = system["address3"];
                                  this.state = system["address4"];
                                  this.pincode = system["zip_code"];
                                  this.siteId = system["site_id"];
                                  this.install_base_id = system["install_base_id"];

                                  this.main_address1 = system["address1"];
                                  this.main_address2 = system["address2"];
                                  this.main_city = system["address3"];
                                  this.main_state = system["address4"];
                                  this.main_pincode = system["zip_code"];
                                  this.main_siteId = system["site_id"];
                                  this.main_install_base_id = system["install_base_id"];
                                  console.log(system);
                                  this.loading.dismiss();
                                });
                            },
                            error => {
                              console.log(
                                "error at Calling the Refresh Token" +
                                JSON.stringify(error)
                              );
                              this.storage.set(this.globals.role_id, null);
                              this.loading.dismiss();
                              this.navCtrl.push(HomePage);
                            }
                          );
                        }
                      });

                    // end
                  } else {
                    this.error_msg = "Invalid system id";
                    this.sysError = "Invalid system id";
                    this.siteId = "";
                    this.main_address1 = "";
                    this.main_address2 = "";
                    this.main_city = "";
                    this.main_state = "";
                    this.main_pincode = "";
                    this.main_siteId = "";
                    this.main_install_base_id = "";
                    this.loading.dismiss();
                  }
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.loading.dismiss();
              this.navCtrl.push(HomePage);
            }
          );
        }
      });

    // end

    // this.messageService.getCustomerAvailability(this.systemId).subscribe(status => {
    //   this.responseData = status;
    //   if (this.responseData == 1) {
    //     this.messageService.getCustomerAddress(this.systemId).subscribe(system => {
    //       console.log("customer dis values");
    //       this.address1 = system['address1'];
    //       this.address2 = system['address2'];
    //       this.city = system['address3'];
    //       this.state = system['address4'];
    //       this.pincode = system['zip_code'];
    //       this.siteId = system['site_id'];

    //       console.log(system);
    //     });
    //   } else {
    //     this.error_msg = "Invalid system id";
    //   }
    // });
  }

  getWareHouseDet() {
    // this.clearForm();
    var wh_data = this.returnRTMasterResponse.wh_data;
    console.log(wh_data);
  }

  checkOrderNum() {
    console.log(this.orderNum);
    console.log(this.orderNum);
    this.newOrderNum = this.orderNum;
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    console.log("-------------------" + JSON.stringify(this.selected));

    this.selected.forEach(element => {
      var mainData = element.main_data;
      var oahId = mainData.oah_id;
      var orderStatusId = mainData.ordered_asset_id;
      this.oahAndOrderedAssetId[oahId] = orderStatusId;

      let orderedAssetId1 = mainData.ordered_asset_id;
      var returnStatus = mainData.return_status;
      this.orderedAssetStatus[orderedAssetId1] = returnStatus;
    });

    console.log(
      "this.oahAndOrderedAssetId" + JSON.stringify(this.oahAndOrderedAssetId)
    );

    this.messageService
      .getReturnOrderIdAvailability(
        this.feOrderId,
        this.returnRTMasterResponse.tool_order_id,
        this.returnRTMasterResponse.owned_asset_count,
        this.oahAndOrderedAssetId,
        this.orderedAssetStatus,
        this.returnRTMasterResponse.fe1_order_number,
        this.returnType
      )
      .then(data => {
        //var res = JSON.stringify(data);
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.responseData = JSON.parse(data);
        console.log("res data" + this.responseData);
        console.log("res data" + JSON.stringify(this.responseData));
        // this.orderIdStatus = 'Success';
        if (this.responseData.transaction_status == 1) {
          this.orderIdStatus = "Success";
          this.orderIdStatusColor = "green";
          this.loading.dismiss();
        } else {
          this.orderIdStatus = this.responseData.transaction_des;
          this.orderIdStatusColor = "red";
          this.loading.dismiss();
        }
        console.log("res after data" + this.responseData);
        console.log("res after data" + JSON.stringify(this.responseData));
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .getReturnOrderIdAvailability(
                  this.feOrderId,
                  this.returnRTMasterResponse.tool_order_id,
                  this.returnRTMasterResponse.owned_asset_count,
                  this.oahAndOrderedAssetId,
                  this.orderedAssetStatus,
                  this.returnRTMasterResponse.fe1_order_number,
                  this.returnType
                )
                .then(data => {
                  this.responseData = JSON.parse(data);

                  if (this.responseData.transaction_status == 1) {
                    this.orderIdStatus = "Success";
                    this.orderIdStatusColor = "green";
                    this.loading.dismiss();
                  } else {
                    this.orderIdStatus = this.responseData.transaction_des;
                    this.orderIdStatusColor = "red";
                    this.loading.dismiss();
                  }
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.loading.dismiss();
              this.navCtrl.push(HomePage);
            }
          );
        }
        this.loading.dismiss();
        console.log("data" + this.orderIdStatus);
      });
  }

  reorderType() {
    if(this.returnType == "3"|| this.returnType == "4")
    { 

    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.selected.forEach(element => {
      var mainData = element.main_data;
      var oahId = mainData.oah_id;
      var orderStatusId = mainData.ordered_asset_id;
      this.oahAndOrderedAssetId[oahId] = orderStatusId;


    });
    //  this.orderId = "";
    this.messageService
      .getFeOrderListForReturn(
        this.returnRTMasterResponse.tool_order_id,
        this.oahAndOrderedAssetId,
    )
      .then(
        data => {
          //var res = JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);

          if (this.responseData.transaction_status == 1) {
            this.loading.dismiss()
            this.feOrderList = this.responseData.transaction_des;
          } else if (this.responseData.transaction_status == 2) {
            this.feOrderList = false;
            this.error_msg = this.responseData.transaction_des;
            this.loading.dismiss();

          }
        },
        (e: any) => {
          var error = e.message;

          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getFeOrderListForReturn(
                    this.returnRTMasterResponse.tool_order_id,
                    this.oahAndOrderedAssetId,

                )
                  .then(data => {
                    this.responseData = JSON.parse(data);
                    if (this.responseData.transaction_status == 1) {
                      this.loading.dismiss()
                      this.feOrderList = this.responseData.transaction_des;
                    } else if (this.responseData.transaction_status == 2) {
                      this.feOrderList = false;
                      this.error_msg = this.responseData.transaction_des;
                      this.loading.dismiss();

                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.loading.dismiss();
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );
    }
    else
      {
        console.log("elseeeeee");
        
        this.feOrderId  = "";
      }


  }

  onFeOrderChange(order) {
    console.log(this.feOrderList);
    console.log(this.orderNum);

    for (var i = 0; i < this.feOrderList.length; i++) {
      console.log(this.feOrderList[i].o_data.trim());
      if (this.feOrderList[i].o_data.trim() == this.orderNum.trim()) {
        this.feOrderId = this.feOrderList[i].o_number
      }
    }
    /* this.feOrderList.forEach(j => {
      if (this.orderNum == j.o_data) {
        console.log("enter");

        this.feOrderId = j.o_number;
      }
    }) */


    console.log((this.feOrderId));

  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ReturnToolPage");
  }
}
