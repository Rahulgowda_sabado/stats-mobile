import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";

@IonicPage()
@Component({
  selector: "page-address-change",
  templateUrl: "address-change.html"
})
export class AddressChangePage {
  globals: Globals;
  addressList = [];
  ssoId = "";
  err_msg = "";
  success_msg = "";
  itemExist = true;
  responseData: any;

  country_id = "";
  countries_list = "";
  countries_list_data = "";
  loading: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.addressListData();
    });
  }

  addressListData() {
    this.messageService
      .addressChange(this.ssoId, this.country_id)
      .then(data => {
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.responseData = JSON.parse(data);

        this.addressList = this.responseData.final_data;
        this.countries_list = this.responseData.countries_list;
        console.log("list" + this.countries_list);
        this.countries_list_data = this.responseData.countries_list_data;
        //this.addressList = data;
        console.log(this.addressList);
        if (this.addressList.length == 0) {
          this.itemExist = false;
        }
        this.loading.dismiss();
      })
      .catch((e: any) => {
        let error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .addressChange(this.ssoId, this.country_id)
                .then(data => {
                  this.responseData = JSON.parse(data);
                  this.addressList = this.responseData.final_data;
                  this.countries_list = this.responseData.countries_list;
                  console.log("list" + this.countries_list);
                  this.countries_list_data = this.responseData.countries_list_data;

                  this.addressList = this.responseData;
                  //this.addressList = data;
                  console.log(this.addressList);
                  if (this.addressList.length == 0) {
                    this.itemExist = false;
                  }
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      });
  }

  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(AddressChangePage, { country_id: country_id });
  }

  onClick(status, address) {
    var jsonData = "";
    var url = "";

    if (address.order_info.return_order_id === undefined) {
      url = "submit_order_address_changeM";
      jsonData = JSON.stringify({
        sso_id: this.ssoId,
        location_id: address.order_info.location_id,
        tool_order_id: address.order_info.tool_order_id,
        order_number: address.order_info.order_number,
        site_id: address.order_info.site_id,
        address1: address.order_info.address1,
        address2: address.order_info.address2,
        address3: address.order_info.address3,
        address4: address.order_info.address4,
        pin_code: address.order_info.pin_code,
        remarks: address.order_info.remarks,
        ac_submit: status,
        customer_site_id: address.customer_info.customer_site_id,
        device_type: this.globals.deviceName
      });
    } else {
      url = "submit_return_address_changeM";
      jsonData = JSON.stringify({
        sso_id: this.ssoId,
        location_id: address.order_info.location_id,
        return_order_id: address.order_info.return_order_id,
        return_number: address.order_info.return_number,
        site_id: address.order_info.site_id,
        address1: address.order_info.address1,
        address2: address.order_info.address2,
        address3: address.order_info.address3,
        address4: address.order_info.address4,
        pin_code: address.order_info.zip_code,
        remarks: address.order_info.remarks,
        ac_submit: status,
        customer_site_id: address.customer_info.customer_site_id,
        device_type: this.globals.deviceName
      });
    }

    let alert = this.alertCtrl.create({
      title: "Submission of Address Change Approval",
      message: "Are you sure you want to submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the order gets displayed ....."
              });
              loading.present();
              this.messageService
                .addressUpdate(jsonData, url)
                .then(data => {
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }
                  this.responseData = JSON.parse(data);

                  //let data = this.responseData;
                  loading.dismiss();
                  if (this.responseData.transaction_status == 1) {
                    this.success_msg = this.responseData.transaction_des;
                    this.presentAlert(
                      "Address Change",
                      this.responseData.transaction_des
                    );
                  } else {
                    this.err_msg = this.responseData.transaction_des;
                    this.presentAlert(
                      "Address Change",
                      this.responseData.transaction_des
                    );
                  }
                })
                .catch((e: any) => {
                  let error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error == "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .addressUpdate(jsonData, url)
                          .then(data => {
                            this.responseData = JSON.parse(data);
                            //let data = this.responseData;
                            loading.dismiss();
                            if (this.responseData.transaction_status == 1) {
                              this.success_msg = this.responseData.transaction_des;
                              this.presentAlert(
                                "Address Change",
                                this.responseData.transaction_des
                              );
                            } else {
                              this.err_msg = this.responseData.transaction_des;
                              this.presentAlert(
                                "Address Change",
                                this.responseData.transaction_des
                              );
                            }
                          });
                      },
                      error => {
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AddressChangePage");
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
    //this.navCtrl.push(ExtendDatePage,{});
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }
}
