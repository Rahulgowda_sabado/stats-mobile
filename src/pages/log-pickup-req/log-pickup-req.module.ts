import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogPickupReq } from './log-pickup-req';

@NgModule({
  declarations: [
    LogPickupReq,
  ],
  imports: [
    IonicPageModule.forChild(LogPickupReq),
  ],
})
export class LogPickupReqModule {}
