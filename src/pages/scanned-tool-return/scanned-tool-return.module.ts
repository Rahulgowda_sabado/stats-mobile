import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScannedToolReturnPage } from './scanned-tool-return';

@NgModule({
  declarations: [
    ScannedToolReturnPage,
  ],
  imports: [
    IonicPageModule.forChild(ScannedToolReturnPage),
  ],
})
export class ScannedToolReturnPageModule {}
