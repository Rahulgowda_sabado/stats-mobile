import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogCalibrationReq } from './log-calibration-req';

@NgModule({
  declarations: [
    LogCalibrationReq,
  ],
  imports: [
    IonicPageModule.forChild(LogCalibrationReq),
  ],
})
export class LogCalibrationReqModule {}
