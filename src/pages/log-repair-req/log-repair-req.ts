import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";

@IonicPage()
@Component({
  selector: "page-log-repair-req",
  templateUrl: "log-repair-req.html"
})
export class LogRepairReq {
  globals: Globals;
  ssoId = "";
  shownGroup = null;

  alertResult = [];
  alertType = 0;
  alertName = "";
  error_msg = "";
  responseData: any;

  wh_id = "";
  whs_list = "";
  whs_list_data = "";
  loading: any;
  jsonData: any;
  myInput: any = "";

  constructor(
    private loadingCtrl: LoadingController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    //this.alertType = navParams.get('alert');
    //var role = navParams.get('role');

    this.wh_id = this.navParams.get("wh_id");
    console.log("wh_id" + this.wh_id);
    console.log("***alertName*****");
    console.log("alertType:" + this.alertType);
    console.log(this.alertName);
    console.log("***************");

    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.jsonData = JSON.stringify({
        sso_id: sso_id,
        segment: 0,
        wh_id: this.wh_id
      });
      this.viewWhDataList();
    });
  }

  viewWhDataList() {
    this.jsonData = JSON.parse(this.jsonData);
    this.jsonData.serial_no = this.myInput;
    this.jsonData = JSON.stringify(this.jsonData);
    this.messageService.getPendingRepairRequest(this.jsonData).then(
      data => {
        // console.log(data);
        this.responseData = JSON.parse(data);
        console.log("data:::\n", this.responseData);
        this.alertResult = this.responseData.final_data;
        this.whs_list = this.responseData.whs_list;
        this.whs_list_data = this.responseData.whs_list_data;
        console.log("alerts results" + JSON.stringify(this.alertResult));
        console.log("whs list" + JSON.stringify(this.whs_list));
        console.log("whs list data" + JSON.stringify(this.whs_list_data));
        this.loading.dismiss();
      },
      (error: HttpErrorResponse) => {
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error.statusText);
        if (error.statusText == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .getPendingRepairRequest(this.jsonData)
                .then(data => {
                  console.log(data);
                  this.responseData = JSON.parse(data);
                  this.alertResult = this.responseData.final_data;
                  this.whs_list = this.responseData.whs_list;
                  this.whs_list_data = this.responseData.whs_list_data;
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      }
    );
  }

  viewWhData(wh_id) {
    this.wh_id = wh_id;
    this.navCtrl.push(LogRepairReq, { wh_id: wh_id });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      var jsonData = JSON.stringify({
        sso_id: this.ssoId,
        segment: this.alertResult.length,
        wh_id: this.wh_id,
        serial_no: this.myInput
      });

      this.messageService.getPendingRepairRequest(jsonData).then(
        dataList => {
          this.responseData = JSON.parse(dataList);
          if (this.responseData.whs_list != undefined)
            this.whs_list = this.responseData.whs_list;
          this.whs_list_data = this.responseData.whs_list_data;
          this.responseData = this.responseData.final_data;
          if (this.responseData == undefined) {
            this.error_msg = "No more items to display";
          } else {
            this.error_msg = "";
            this.responseData.forEach(data => {
              this.alertResult.push(data);
            });
          }
        },
        (error: HttpErrorResponse) => {
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error.statusText);
          if (error.statusText == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getPendingRepairRequest(jsonData)
                  .then(dataList => {
                    this.responseData = JSON.parse(dataList);
                    if (this.responseData.whs_list != undefined)
                      this.whs_list = this.responseData.whs_list;
                    this.whs_list_data = this.responseData.whs_list_data;
                    if (this.responseData == undefined) {
                      this.error_msg = "No more items to display";
                    } else {
                      this.error_msg = "";
                      this.responseData.forEach(data => {
                        this.alertResult.push(data);
                      });
                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );
      infiniteScroll.complete();
    }, 500);
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LogisticsAlertPage");
  }
}
