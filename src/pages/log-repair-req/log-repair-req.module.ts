import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogRepairReq } from './log-repair-req';

@NgModule({
  declarations: [
    LogRepairReq,
  ],
  imports: [
    IonicPageModule.forChild(LogRepairReq),
  ],
})
export class LogRepairReqModule {}
