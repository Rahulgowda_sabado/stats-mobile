import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { ShoppinglistPage } from "../../pages/shoppinglist/shoppinglist";
import { Tool } from "../../pages/model/tool.model";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { ToolAvailabiltiyInWhPage } from "../tool-availabiltiy-in-wh/tool-availabiltiy-in-wh";

@IonicPage()
@Component({
  selector: "page-feorders",
  templateUrl: "feorders.html"
})
export class FeordersPage {
  globals: Globals;
  myInput: any = "";
  tools: any = [];

  totalProduct: number = 0;
  qtd: any[];
  cart: any = { items: [] };
  itemss: Tool[] = [];
  newQuant: number;
  ssoId = "";
  error_msg = "";
  country_id = "";
  responseData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public messageService: MessageServiceProvider,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) {
    this.globals = Globals.getInstance();
    if (
      this.globals.transaction_country == "" ||
      this.globals.transaction_country == undefined
    ) {
      this.storage.get(this.globals.country_id).then(country_id => {
        this.country_id = country_id;
      });
    } else {
      this.country_id = this.globals.transaction_country.location_id;
    }
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.searchTools();
    });
  }

  ionViewDidEnter() {
    console.log("this.itemss" + JSON.stringify(this.itemss));
    console.log("this.itemss.length" + this.itemss.length);
    this.itemss.forEach(items => {
      console.log("inside of foreach");
      let found: boolean = false;
      this.globals.selectedTools.forEach(selected => {
        if (selected.tool_id == items.tool_id) {
          found = true;
        }
      });
      if (!found) {
        console.log("inside !found");
        //this.removeSingleElement(items);
        console.log("inside item" + JSON.stringify(items));
        items.isAdded = false;
        items.isSelected = false;
        console.log(this.itemss);
        let foo_items = items;
        this.itemss = this.itemss.filter(obj => obj !== foo_items);
        console.log("inside items" + JSON.stringify(this.itemss));
        this.totalProduct = this.itemss.length;
        items.tool_quant = 0;
        this.totalProduct = this.itemss.length;
      }
    });
  }

  searchTools() {
    this.error_msg = "";

    this.messageService
      .getToolsList(this.ssoId, this.country_id, this.myInput, 0)
      .then(data => {
        //var res =  JSON.stringify(data);
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.responseData = JSON.parse(data);
        this.tools = this.responseData.toolResults;
        this.tools = this.responseData.toolResults;
        /*this.tools.forEach(tool => {
        tool.qty = 5;
      });*/
        console.log(this.globals.selectedTools);
        console.log("***data list****");
        console.log(this.tools.length);
        console.log("*****************");
        if (this.tools.length == 0) {
          this.error_msg = "Search item not found";
        }
      })
      .catch((e: any) => {
        //alert('error'+e);
        var error = e.message;
        // alert("count:"+JSON.stringify(data))
        //alert('alerrrt'+error);
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .getToolsList(this.ssoId, this.country_id, this.myInput, 0)
                .then(data => {
                  this.responseData = JSON.parse(data);
                  this.tools = this.responseData.toolResults;
                  this.tools = this.responseData.toolResults;
                  if (this.tools.length == 0) {
                    this.error_msg = "Search item not found";
                  }
                });
            },
            error => {
              //alert('errorrr');
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      });

    // end
  }

  qtyChange(tool) {
    if (tool.tool_quant > 0) {
      this.addSingleElement(tool);
    } else {
      this.removeSingleElement(tool);
    }
  }

  createRange(qty) {
    console.log(qty);
    var items: number[] = [];
    for (var i = 0; i <= qty; i++) {
      items.push(i);
    }
    return items;
  }

  addToCart(tool: Tool) {
    tool.modality;
    this.updateQuantity(tool);
  }

  getTotalProduct() {
    this.totalProduct = this.itemss.length;
  }

  public containsElement(tool: Tool) {
    var contains: boolean = false;
    this.itemss.forEach(s1 => {
      if (s1.tool_number == tool.tool_number) {
        contains = true;
        return contains;
      }
    });
    return contains;
  }

  public addSingleElement(tool: Tool) {
    tool.isAdded = true;
    var contains = this.containsElement(tool);

    /*if (tool.tool_quant == tool.qty) {
      return;
    }*/

    if (tool.tool_quant === undefined || tool.tool_quant == 0)
      tool.tool_quant = 1;

    if (!contains) {
      this.itemss.push(tool);
    } else {
      this.updateQuantity(tool);
    }
    this.getTotalProduct();
  }

  public removeSingleElement(tool: Tool) {
    console.log("remove single element called");
    if (tool.tool_quant == undefined) {
    } else {
      this.reduceQuantity(tool);
    }
  }

  public addElement(tool: Tool) {
    tool.isAdded = true;
    var contains = this.containsElement(tool);
    if (tool.tool_quant === undefined) tool.tool_quant = 1;
    if (!contains) {
      this.itemss.push(tool);
    } else {
      this.updateQuantity(tool);
    }
    this.getTotalProduct();
  }

  public updateQuantity(tool: Tool) {
    tool.tool_quant = +tool.tool_quant + 1;
    this.itemss.forEach(s1 => {
      if (
        s1.tool_number === tool.tool_number &&
        s1.tool_quant != tool.tool_quant
      ) {
        s1.tool_quant = +s1.tool_quant + +tool.tool_quant;
      }
    });
  }

  public reduceQuantity(tool: Tool) {
    // tool.tool_quant = 0 ;
    // tool.isAdded = false;
    // let foo_items = tool;
    // this.itemss = this.itemss.filter(obj => obj !== foo_items);
    // this.totalProduct = this.itemss.length;
    console.log(tool.tool_quant);
    if (tool.tool_quant == 1) {
      tool.isAdded = false;
      console.log(this.itemss);
      let foo_items = tool;
      this.itemss = this.itemss.filter(obj => obj !== foo_items);
      this.totalProduct = this.itemss.length;
    }
    if (tool.tool_quant > 0) {
      tool.tool_quant = tool.tool_quant - 1;
      this.totalProduct = this.itemss.length;
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.messageService
        .getToolsList(
          this.ssoId,
          this.country_id,
          this.myInput,
          this.tools.length
        )
        .then(dataList => {
          //var res =  JSON.stringify(dataList);
          if (dataList.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(dataList);
          if (this.responseData.toolResults.length == 0) {
            this.error_msg = "No more Tools to display";
          } else {
            this.error_msg = "";
            this.responseData.toolResults.forEach(data => {
              this.tools.push(data);
            });
          }
        })
        .catch((e: any) => {
          var error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getToolsList(
                    this.ssoId,
                    this.country_id,
                    this.myInput,
                    this.tools.length
                  )
                  .then(dataList => {
                    this.responseData = JSON.parse(dataList);
                    if (this.responseData.toolResults.length == 0) {
                      this.error_msg = "No more Tools to display";
                    } else {
                      this.error_msg = "";
                      this.responseData.toolResults.forEach(data => {
                        this.tools.push(data);
                      });
                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });

      // end

      // this.messageService.getToolsList(this.ssoId, this.myInput, this.tools.length).subscribe(dataList => {
      //   this.responseData = dataList;
      //   if (this.responseData.toolResults.length == 0) {
      //     this.error_msg = 'No more items to display';
      //   } else {
      //     this.error_msg = '';
      //     this.responseData.toolResults.forEach(data => {
      //       this.tools.push(data);
      //     });
      //   }

      // });

      infiniteScroll.complete();
    }, 500);
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  navigateToShopping() {
    this.navCtrl.push(ShoppinglistPage, { items: this.itemss });
  }

  checkAvail(tool) {
    /*  let alert = this.alertCtrl.create({
       title: 'Tool Availability in WH',
        subTitle: '10% of battery remaining',

      buttons: ['Dismiss']
    });
    alert.present();
 */

    let profileModal = this.modalCtrl.create(ToolAvailabiltiyInWhPage, tool);
    profileModal.present();
  }
}
