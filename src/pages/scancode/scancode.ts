import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { HomePage } from "../home/home";
import { FetoolreturntoolPage } from "../fetoolreturntool/fetoolreturntool";
import { ScanToolPage } from "../scan-tool/scan-tool";
import { MenuPage } from "../menu/menu";

@Component({
  selector: "page-scancode",
  templateUrl: "scancode.html"
})
export class ScancodePage {
  qrData = null;
  createdCode = null;

  //scannedCode='INDMR03380000037';
  //scannedCode1='INDMR03380000037';
  //scannedCode1='INDLS01080100022';

  scannedCode: string;
  scannedtool = [];
  selected = [];
  initiateRTResponse: any;
  ScannedTools = [];
  nonScannedTools = [];
  toolDetails = [];

  constructor(
    private barcodeScanner: BarcodeScanner,
    public nav: NavController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.scannedtool = navParams.get("scannedtool");
    this.initiateRTResponse = navParams.get("initiateRTResponse");
    this.nonScannedTools = navParams.get("nonScannedTools");
    this.toolDetails = navParams.get("toolDetails");

    if (this.scannedtool == undefined) this.scannedtool = [];

    console.log(
      "#############################################################"
    );
    console.log("The scanned data    " + JSON.stringify(this.scannedtool));

    console.log(
      "#############################################################"
    );
    console.log("rest api data" + JSON.stringify(this.initiateRTResponse));

    console.log(
      "#############################################################"
    );
    console.log("the nonScannedTools" + JSON.stringify(this.nonScannedTools));

    console.log(
      "#############################################################"
    );
    console.log("the html data" + JSON.stringify(this.toolDetails));

    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        console.log(barcodeData);
        /*  if (barcodeData.cancelled == true) {
          this.navCtrl.setRoot(MenuPage);
        } */
        this.scannedCode = barcodeData.text;
        console.log("the scanned barcode :" + this.scannedCode);
        if (this.scannedtool.length == 0) {
          console.log(
            "the new scanned code from the scan tool page  " + this.scannedCode
          );

          if (barcodeData.cancelled == true) {
            this.navCtrl.setRoot(HomePage);
          } else {
            this.nav.setRoot(ScanToolPage, {
              scannedCode: this.scannedCode,
              scannedtool: this.scannedtool
            });
          }
        } else {
          console.log(
            "hi i am else part and the scanned code 1 = " + this.scannedCode
          );
          this.nav.setRoot(ScanToolPage, {
            scannedCode: this.scannedCode,
            scannedtool: this.scannedtool,
            initiateRTResponseNav: this.initiateRTResponse,
            nonScannedTools: this.nonScannedTools,
            toolDetails: this.toolDetails
          });
        }
      })
      .catch(err => {
        alert(err);
        this.navCtrl.setRoot(MenuPage)
      });
  }
}
