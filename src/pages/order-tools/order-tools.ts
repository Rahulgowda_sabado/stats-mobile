import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { MenuPage } from "../menu/menu";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { parseDate } from "ionic-angular/util/datetime-util";

@IonicPage()
@Component({
  selector: "page-order-tools",
  templateUrl: "order-tools.html"
})
export class OrderToolsPage {
  ten = 10;
  globals: Globals;
  servicetypelist = [];
  customerlist = [];
  warehouseDet: any = {};
  items = [];
  itemIdWithQty = {};
  ssoId: any;
  //addressChange: any;
  curDate: String;
  address1 = "";
  address2 = "";
  city = "";
  state = "";
  pincode = "";
  serviceType: any;
  neededDate: any;
  requestedDate: any;
  returnedDate: any;
  systemId = "";
  deliveryPoint: any;
  deliveryPointId: any;
  sebealSrNum: any;
  readiness: string;
  click: any;
  siteId = "";
  install_base_id = "";
  site_remarks: string;
  error_msg = "";
  success_msg = "";
  orderId: string;
  orderType: string;
  other_remarks: string;
  orderIdStatus: string = "";
  orderIdStatusColor: string = "";
  responseData: any;
  value: number;
  authorization_message: any;
  err: any;
  sysErr: any = "Invalid System ID";
  fe_tool: any;
  conditionRole: any;
  country_id = "";
  country_id1 = "";
  loading: any;
  finalSystemId = "";
  finalOrderId = "";

  main_address1 = "";
  main_address2 = "";
  main_city = "";
  main_state = "";
  main_pincode = "";
  main_siteId = "";
  main_install_base_id = "";

  employees: any = [];
  wh_id: any;
  shipBy: any = [];
  whList: any = [];
  fe_to_wh_id: any;
  role_id: any;
  systemIdColor: any;
  systemIdStatus: any;
  feOrderList: any = [];
  feOrderId: any = "";

  menu_display: boolean = false;

  notesPreference:boolean = false;
  notes:any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.globals = Globals.getInstance();
    this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id1 = country_id;
    });

    this.storage.get(this.globals.role_id).then(role_id => {
      this.role_id = role_id;
      // Hard coded values
      // this.role_id = "5";
    });

    if (
      this.globals.transaction_country.location_id == "" ||
      this.globals.transaction_country.location_id == undefined
    ) {
      this.storage.get(this.globals.country_id).then(country_id => {
        this.country_id = country_id;
      });
    } else {
      this.country_id = this.globals.transaction_country.location_id;
    }

    this.orderType = "0";
    this.readiness = "1";
    this.click = "0";
    this.deliveryPoint = "1";

    let loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loading.present();

    this.items = navParams.get("items");
    this.requestedDate = new Date().toISOString();
    this.items.forEach(s1 => {
      this.itemIdWithQty[s1.tool_id] = s1.tool_quant;
    });

    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      var jsonData = JSON.stringify({
        sso_id: sso_id,
        toolIdsWithQty: this.itemIdWithQty,
        transactionCountry: this.country_id
      });

      this.messageService
        .confirmTools(jsonData)
        .then(data => {
          //var res = JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);
          this.shipBy = this.responseData.ship_by_wh_data;
          this.whList = this.responseData.wh_data;
          this.fe_to_wh_id = this.responseData.wh_id;
          this.wh_id = this.responseData.wh_id;

          this.servicetypelist = this.responseData.service_type;
          this.warehouseDet = this.responseData.wh_data[0];
          this.fe_tool = this.responseData.fe_tool;
          this.conditionRole = this.responseData.conditionRole;
          console.log(this.warehouseDet);
          console.log("Notes Before Flag"+this.notesPreference);
          this.notesPreference = this.responseData.notesPreference;
          console.log("Notes Preference Flag"+this.notesPreference);

          loading.dismiss();
        })
        .catch((e: any) => {
          var error = e.message;
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService.confirmTools(jsonData).then(data => {
                  this.responseData = JSON.parse(data);

                  this.shipBy = this.responseData.ship_by_wh_data;
                  this.whList = this.responseData.wh_data;
                  this.fe_to_wh_id = this.responseData.wh_id;
                  this.wh_id = this.responseData.wh_id;

                  this.servicetypelist = this.responseData.service_type;
                  this.warehouseDet = this.responseData.wh_data[0];
                  this.fe_tool = this.responseData.fe_tool;
                  this.conditionRole = this.responseData.conditionRole;

                  this.notesPreference = this.responseData.notesPreference;
                  console.log(this.notesPreference);
                  console.log(this.warehouseDet);
                  loading.dismiss();
                });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });
    });
  }

  orderChange(order) {
    console.log(order);
    this.feOrderList.forEach(item => {
      if (item.o_data == this.orderId)
        this.feOrderId = item.o_number;
    })
    console.log((this.feOrderId));

  }

  checkOrderId() {
    console.log(this.orderId);
    this.finalOrderId = this.orderId;
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.messageService
      .getOrderIdAvailability(
        this.feOrderId,
        this.ssoId,
        this.country_id,
        "raise_order",
        this.itemIdWithQty
      )
      .then(
        data => {
          //var res = JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);

          if (this.responseData.transaction_status == 1) {
            this.error_msg = "";
            this.orderIdStatus = "Success";
            this.orderIdStatusColor = "green";
            this.loading.dismiss();
          } else {
            this.orderIdStatus = this.responseData.transaction_des;
            this.orderIdStatusColor = "red";
            this.loading.dismiss();
          }
        },
        (e: any) => {
          var error = e.message;

          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getOrderIdAvailability(
                    this.feOrderId,
                    this.ssoId,
                    this.country_id,
                    "raise_order",
                    this.itemIdWithQty
                  )
                  .then(data => {
                    this.responseData = JSON.parse(data);
                    if (this.responseData.transaction_status == 1) {
                      this.error_msg = "";
                      this.orderIdStatus = "Success";
                      this.orderIdStatusColor = "green";
                      this.loading.dismiss();
                    } else {
                      this.orderIdStatus = this.responseData.transaction_des;
                      this.orderIdStatusColor = "red";
                      this.loading.dismiss();
                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.loading.dismiss();
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );

    // end

    // this.messageService.getOrderIdAvailability(this.orderId).subscribe(data => {
    //   this.responseData = data;
    //   if (this.responseData == 1) {
    //     this.orderIdStatus = 'Success';
    //     this.orderIdStatusColor = 'green';
    //   } else {
    //     this.orderIdStatus = 'Failed';
    //     this.orderIdStatusColor = 'red';
    //   }

    // });
  }

  reorderType() {
    if(this.orderType == "1")
    { 
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.orderId = "";
    this.messageService
      .getFeOrderList(
        this.ssoId,
        this.country_id,
        this.itemIdWithQty
      )
      .then(
        data => {
          //var res = JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);

          if (this.responseData.transaction_status == 1) {
            this.loading.dismiss()
            this.feOrderList = this.responseData.transaction_des;
          } else if (this.responseData.transaction_status == 2) {
            this.feOrderId = false;
            this.error_msg = this.responseData.transaction_des;
            this.loading.dismiss()
          }
        },
        (e: any) => {
          var error = e.message;

          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getFeOrderList(
                    this.ssoId,
                    this.country_id,
                    this.itemIdWithQty
                  )
                  .then(data => {
                    this.responseData = JSON.parse(data);
                    if (this.responseData.transaction_status == 1) {
                      this.loading.dismiss()
                      this.feOrderList = this.responseData.transaction_des;
                    }
                    else if (this.responseData.transaction_status == 2) {
                      this.feOrderId = false;
                      this.error_msg = this.responseData.transaction_des;
                      this.loading.dismiss()
                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.loading.dismiss();
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );
      console.log("iffffffff");
      }
      else
      {
        console.log("elseeeeee");
        
        this.feOrderId  = "";
      }

  }

  resetAddress() {
    this.clearForm();
    this.address1 = "";
    this.address2 = "";
    this.city = "";
    this.state = "";
    this.pincode = "";
    this.deliveryPointId = "";

    if (this.deliveryPoint == "1") {
      this.getCustDet();
    } else if (this.deliveryPoint == "2") {
      this.getWhDet();
    } else {
      this.clean();
      this.click = 0;
    }
  }
  mainAddress() {
    // this.address1 = "";
    // this.address2 = "";
    // this.city = "";
    // this.state = "";
    // this.pincode = "";
    this.clearForm();
    console.log("main add");
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  clean() {
    this.error_msg = "";
  }
  clearForm() {
    this.error_msg = "";
    /* this.address1 = '';
    this.address2 = '';
    this.city = '';
    this.state = '';
    this.pincode = ''; */
  }

  getCustDet() {
    this.clearForm();
    this.finalSystemId = this.systemId;
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.messageService
      .getCustomerAvailability(this.systemId, this.ssoId, this.country_id1)
      .then(
        status => {
          //var res = JSON.stringify(status);
          //alert(JSON.stringify(status))
          if (status.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(status);
          this.sysErr = "";
          if (this.responseData == 1) {
            this.err = false;
            //this.sysErr='';
            console.log("before1" + this.sysErr);
            this.sysErr = "";
            this.systemIdColor = "";
            this.systemIdStatus = "";
            console.log("after" + this.sysErr);
            this.messageService
              .getCustomerAddress(this.systemId, this.country_id1)
              .then(res => {
                this.sysErr = "";
                this.systemIdColor = "";
                this.systemIdStatus = "";
                //var res1 = JSON.stringify(res);
                //console.log(system);
                //alert("address:"+JSON.stringify(res))
                if (res.status === 401) {
                  throw new Error("Unauthorized");
                }

                var system = JSON.parse(res);

                this.address1 = system["address1"];
                this.address2 = system["address2"];
                this.city = system["address3"];
                this.state = system["address4"];
                this.pincode = system["zip_code"];
                this.siteId = system["site_id"];
                this.install_base_id = system['install_base_id'];

                this.main_address1 = system["address1"];
                this.main_address2 = system["address2"];
                this.main_city = system["address3"];
                this.main_state = system["address4"];
                this.main_pincode = system["zip_code"];
                this.main_siteId = system["site_id"];
                this.main_install_base_id = system['install_base_id'];
                /*  this.addressChange = 0; */
                this.click = 0;
                this.loading.dismiss();
              })
              .catch((e: any) => {
                var error = e.message;
                console.log("Error from http header" + error);
                console.log("Error from http header status text" + error);
                if (error == "Unauthorized") {
                  console.log("code matched and call the api");
                  this.refreshPromise().then(
                    success => {
                      this.messageService
                        .getCustomerAddress(this.systemId, this.country_id1)
                        .then(res => {
                          console.log(system);
                          this.sysErr = "";
                          this.systemIdColor = "";
                          this.systemIdStatus = "";
                          var system = JSON.parse(res);

                          this.address1 = system["address1"];
                          this.address2 = system["address2"];
                          this.city = system["address3"];
                          this.state = system["address4"];
                          this.pincode = system["zip_code"];
                          this.siteId = system["site_id"];
                          this.install_base_id = system['install_base_id'];

                          this.main_address1 = system["address1"];
                          this.main_address2 = system["address2"];
                          this.main_city = system["address3"];
                          this.main_state = system["address4"];
                          this.main_pincode = system["zip_code"];
                          this.main_siteId = system["site_id"];
                          this.main_install_base_id = system['install_base_id'];

                          /* this.addressChange = 0; */

                          this.click = 0;
                          this.loading.dismiss();
                        });
                    },
                    error => {
                      console.log(
                        "error at Calling the Refresh Token" +
                        JSON.stringify(error)
                      );
                      this.storage.set(this.globals.role_id, null);
                      this.loading.dismiss();
                      this.navCtrl.push(HomePage);
                    }
                  );
                }
              });
          } else {
            this.sysErr = "Invalid System ID";
            this.systemIdColor = "red";
            this.systemIdStatus = "Invalid System ID";
            //this.clearForm();
            console.log("else foirst" + this.sysErr);
            //this.sysErr='';
            console.log("else after" + this.sysErr);
            this.main_address1 = "";
            this.main_address2 = "";
            this.main_city = "";
            this.main_state = "";
            this.main_pincode = "";
            this.main_siteId = "";
            this.siteId = "";
            this.install_base_id = "";
            this.loading.dismiss();
          }
        },
        (e: any) => {
          var error = e.message;

          console.log("Error from http header" + error);

          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .getCustomerAvailability(
                    this.systemId,
                    this.ssoId,
                    this.country_id1
                  )
                  .then(status => {
                    console.log("status =>" + status);

                    this.responseData = JSON.parse(status);
                    if (this.responseData == 1) {
                      console.log("second before" + this.sysErr);
                      this.sysErr = "";
                      this.systemIdColor = "";
                      this.systemIdStatus = "";
                      console.log("after second" + this.sysErr);
                      this.messageService
                        .getCustomerAddress(this.systemId, this.country_id1)
                        .then(
                          res => {
                            console.log(system);
                            //var res1 = JSON.stringify(res);
                            if (res.status === 401) {
                              throw new Error("Unauthorized");
                            }
                            var system = JSON.parse(res);

                            this.address1 = system["address1"];
                            this.address2 = system["address2"];
                            this.city = system["address3"];
                            this.state = system["address4"];
                            this.pincode = system["zip_code"];
                            this.siteId = system["site_id"];
                            this.install_base_id = system['install_base_id'];

                            this.main_address1 = system["address1"];
                            this.main_address2 = system["address2"];
                            this.main_city = system["address3"];
                            this.main_state = system["address4"];
                            this.main_pincode = system["zip_code"];
                            this.main_siteId = system["site_id"];
                            this.main_install_base_id = system['install_base_id'];

                            /*  this.addressChange = 0; */
                            this.click = 0;
                            this.loading.dismiss();
                            /*  this.error_msg = 'Please fill ';
                 this.success_msg=''; */
                            /*  loading.dismiss(); */
                          },
                          (e: any) => {
                            var error = e.message;
                            console.log("Error from http header" + error);
                            console.log(
                              "Error from http header status text" + error
                            );
                            if (error == "Unauthorized") {
                              console.log("code matched and call the api");
                              this.refreshPromise().then(
                                success => {
                                  this.messageService
                                    .getCustomerAddress(
                                      this.systemId,
                                      this.country_id1
                                    )
                                    .then(data => {
                                      console.log(system);
                                      this.sysErr = "";
                                      this.systemIdColor = "";
                                      this.systemIdStatus = "";
                                      var system = JSON.parse(data);
                                      this.address1 = system["address1"];
                                      this.address2 = system["address2"];
                                      this.city = system["address3"];
                                      this.state = system["address4"];
                                      this.pincode = system["zip_code"];
                                      this.siteId = system["site_id"];
                                      this.install_base_id = system['install_base_id'];

                                      this.main_address1 = system["address1"];
                                      this.main_address2 = system["address2"];
                                      this.main_city = system["address3"];
                                      this.main_state = system["address4"];
                                      this.main_pincode = system["zip_code"];
                                      this.main_siteId = system["site_id"];
                                      this.main_install_base_id = system['install_base_id'];

                                      /* this.addressChange = 0; */
                                      this.click = 0;
                                      this.loading.dismiss();
                                      /* this.error_msg = 'Please fill ';
                      this.success_msg=''; */
                                      /* loading.dismiss(); */
                                    });
                                },
                                error => {
                                  //var error = e.message;

                                  console.log(
                                    "error at Calling the Refresh Token" +
                                    JSON.stringify(error)
                                  );
                                  this.storage.set(this.globals.role_id, null);
                                  this.loading.dismiss();
                                  this.navCtrl.push(HomePage);
                                }
                              );
                            }
                          }
                        );

                      // end
                    } else {
                      this.sysErr = "Invalid System ID";
                      this.systemIdColor = "red";
                      this.systemIdStatus = "Invalid System ID";
                      this.main_address1 = "";
                      this.main_address2 = "";
                      this.main_city = "";
                      this.main_state = "";
                      this.main_pincode = "";
                      this.main_siteId = "";
                      this.siteId = "";
                      this.install_base_id = "";
                      //loading.dismiss();
                      console.log("final sys" + this.sysErr);

                      console.log("after final sys" + this.sysErr);
                      this.loading.dismiss();
                    }
                  });
              },
              error => {
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.loading.dismiss();
                this.navCtrl.push(HomePage);
              }
            );
          }
        }
      );
  }

  getWhDet() {
    this.clearForm();
    /*  this.address1 = this.warehouseDet.address1;
    this.address2 = this.warehouseDet.address2;
    this.city = this.warehouseDet.address3;
    this.state = this.warehouseDet.address4;
    this.pincode = this.warehouseDet.pin_code; */

    for (var i = 0; i < this.whList.length; i++) {
      if (this.whList[i].wh_id == this.fe_to_wh_id) {
        this.address1 = this.whList[i].address1;
        this.address2 = this.whList[i].address2;
        this.city = this.whList[i].address3;
        this.state = this.whList[i].address4;
        this.pincode = this.whList[i].pin_code;
      }
    }

    /* this.addressChange = 0; */
    this.click = 0;
  }

  openChangeAddress() {
    //1-Yes 0-No
    //this.addressChange = 1;

    this.click = 1;
  }

  /* refreshChangeAddress() { //1-Yes 0-No
    this.addressChange = 0;
  } */
  dropdt: any;
  pickdt: any;

  public calculatedate() {
    this.clearForm();
    this.dropdt = new Date(this.neededDate);
    this.pickdt = new Date(this.returnedDate);
    var days = mstotimes(this.pickdt - this.dropdt);
    var day = days.toString();
    this.value = parseInt(day);

    function mstotimes(s) {
      var ms = s % 1000;
      s = (s - ms) / 1000;
      var secs = s % 60;
      s = (s - secs) / 60;
      var mins = s % 60;
      var hrs = (s - mins) / 60;
      if (hrs == 0 && mins == 0) return "just a monent ago";
      else if (hrs == 0) return mins + "mins";
      else if (hrs < 24) return hrs + "hours";
      else return Math.floor(hrs / 24) + "days";
    }
  }
  dateValidation() {
    var dataValidation: boolean = false;
    if (this.value >= 10) {
      var fe_tools_in_items_count: any = 0;
      this.items.forEach(s1 => {
        // looping selected items
        this.fe_tool.forEach(fe_tool_id => {
          // looping fe tools
          if (fe_tool_id.tool_id == s1.tool_id) {
            fe_tools_in_items_count++;
          }
        });
      });
      console.log("this.items.length" + this.items.length);
      console.log("fe tools in items count" + fe_tools_in_items_count);
      if (this.items.length == fe_tools_in_items_count) {
        if (this.value > 365) {
          dataValidation = true;
          this.error_msg =
            "Maximum gap between need date and return date should be 1 Year";
        }
      } else {
        dataValidation = true;
        this.error_msg =
          "Maximum gap between need date and return date should be 10 days";
      }
    }
    return dataValidation;
  }

  onOrderTools() {
    //alert(this.error_msg)
    //this.sebealSrNum = this.sebealSrNum.replace("-", "");
    var fe_to_wh_id = "";
    var system_id = "";
    if (this.deliveryPoint == "1") {
      system_id = this.systemId;
    } else if (this.deliveryPoint == "2") {
      fe_to_wh_id = this.fe_to_wh_id;
      //this.clean();
    }
    //alert(this.error_msg)

    if (this.readiness === undefined) this.readiness = "1";
    if (this.click === undefined) this.click = "0";
    if (this.site_remarks === undefined) this.site_remarks = "";
    if (this.siteId === undefined) this.siteId = "";

    console.log("submit1" + this.sysErr);
    console.log("this.sysErro" + this.sysErr + "deliv" + this.deliveryPoint);
    console.log("siebel sr" + this.sebealSrNum);

    if (
      this.deliveryPoint == "" ||
      (this.deliveryPoint === undefined &&
        (this.sebealSrNum == "" ||
          (this.sebealSrNum === undefined &&
            (this.returnedDate == "" ||
              (this.returnedDate === undefined &&
                (this.neededDate == "" ||
                  (this.neededDate === undefined &&
                    (this.serviceType == "" ||
                      (this.serviceType === undefined &&
                        (this.orderType === undefined ||
                          (this.orderType == "1" &&
                            (this.orderId == "" ||
                              this.orderId === undefined))))))))))))
    ) {
      this.error_msg = "Please enter all the details";
    } else if (
      this.orderType === undefined ||
      (this.orderType == "1" &&
        (this.orderId == "" || this.orderId === undefined))
    ) {
      this.error_msg = "Please select FE1 Order Number";
    } else if (this.feOrderId === false) {
      console.log(this.feOrderId+"asdfghjsdfghjxdfghjksaaa");
      
      this.error_msg = "No Records Found"
    } else if (this.finalOrderId != this.orderId && this.orderType == "1") {
      this.error_msg = "Please Validate the changed Order Number";
    } else if (this.orderIdStatus != "Success" && this.orderType == "1") {
      this.error_msg = "Please Enter Valid FE1 Order Number";
    } else if (this.serviceType == "" || this.serviceType === undefined) {
      this.error_msg = "Please Enter Service Type";
    } else if (this.neededDate === undefined || this.neededDate == "") {
      this.error_msg = " Please Enter Need Date";
    } else if (this.returnedDate === undefined || this.returnedDate == "") {
      this.error_msg = " Please Enter Return Date";
    } else if (this.dateValidation()) {
    } else if (this.sebealSrNum === undefined || this.sebealSrNum == "") {
      this.error_msg = "Please Enter Siebel SR Number";
    } else if (this.deliveryPoint === undefined || this.deliveryPoint == "") {
      this.error_msg = "Please Enter delivery Point";
    } else if (this.deliveryPoint == "1" && this.systemId == "") {
      this.error_msg = "Please Enter System Id";
    } else if (
      this.systemId != this.finalSystemId &&
      this.deliveryPoint == "1"
    ) {
      this.error_msg = "Please Validate the changed System ID";
    } else if (
      this.sysErr === "Invalid System ID" &&
      this.deliveryPoint == "1"
    ) {
      this.error_msg = "Please Enter valid system Id";

    } else if (
      (this.deliveryPoint == "3" ||
        (this.deliveryPoint == "1" && this.click == "1")) &&
      (this.pincode == "" ||
        this.pincode === undefined ||
        (this.state == "" || this.state === undefined) ||
        (this.city == "" || this.city === undefined) ||
        (this.address2 == "" || this.address2 === undefined) ||
        (this.address1 == "" || this.address1 === undefined))
    ) {
      this.error_msg = "Please fill all the details";
    } else {
      if (this.click == 0 && this.deliveryPoint == 1) {
        this.address1 = this.main_address1;
        this.address2 = this.main_address2;
        this.city = this.main_city;
        this.state = this.main_state;
        this.siteId = this.main_siteId;
      }

      this.clean();
      var jsonData = JSON.stringify({
        sso_id: this.ssoId,
        final_sso_id: this.ssoId,
        conditionRole: this.conditionRole,
        fe1_order_number: this.feOrderId,
        toolIdsWithQty: this.itemIdWithQty,
        fe_check: this.orderType,
        service_type_id: this.serviceType,
        service_type_remarks: this.other_remarks,
        site_readiness: this.readiness,
        //click:this.click,
        site_remarks: this.site_remarks,
        srn_number: this.sebealSrNum,
        deploy_date: this.requestedDate,
        request_date: this.neededDate,
        return_date: this.returnedDate,
        delivery_type_id: this.deliveryPoint,

        system_id: system_id,
        install_base_id: this.install_base_id,
        check_address: this.click,
        address_remarks: "",
        fe_to_wh_id: fe_to_wh_id,
        site_id: this.siteId,
        address1: this.address1,
        address2: this.address2,
        city: this.city,
        state: this.state,
        pin_code: this.pincode,
        wh_id: this.wh_id,
        transactionCountry: this.country_id,
        device_type: this.globals.deviceName,
        notes:this.notes
      });
      console.log("the jsobj");
      console.log(jsonData);
      let alert = this.alertCtrl.create({
        title: "Submission of Order",
        message: "Are you sure you want to submit",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Submit",
            handler: () => {
              console.log("Submit clicked");
              alert.dismiss().then(() => {
                let loading = this.loadingCtrl.create({
                  content: "Please wait until the order gets displayed ....."
                });
                loading.present();
                this.messageService.orderTools(jsonData).then(
                  data => {
                    if (data.status === 401) {
                      throw new Error("Unauthorized");
                    }

                    // loading.dismiss();
                    //var res = JSON.stringify(data);
                    this.responseData = JSON.parse(data);
                    if (this.responseData.transaction_status == 1) {
                      //this.err_msg = "";
                      //this.success_msg = data.transaction_des;
                      loading.dismiss().then(() => {
                        const alert = this.alertCtrl.create({
                          title: "Order Tools",

                          subTitle: this.responseData.transaction_des,

                          buttons: ["OK"]
                        });

                        alert.present();

                        this.navCtrl.setRoot(MenuPage);
                      });
                    } else {
                      this.error_msg = this.responseData.transaction_des;
                      this.success_msg = "";
                      loading.dismiss();
                    }
                  },
                  (e: any) => {
                    var error = e.message;

                    console.log("Error from http header" + error);
                    console.log("Error from http header status text" + error);
                    if (error == "Unauthorized") {
                      console.log("code matched and call the api");
                      this.refreshPromise().then(
                        success => {
                          this.messageService
                            .orderTools(jsonData)
                            .then(data => {
                              // loading.dismiss();
                              console.log(JSON.stringify(data));
                              this.responseData = JSON.parse(data);
                              if (this.responseData.transaction_status == 1) {
                                //this.err_msg = "";
                                //this.success_msg = data.transaction_des;
                                loading.dismiss().then(() => {
                                  const alert = this.alertCtrl.create({
                                    title: "Order Tools",
                                    subTitle: this.responseData.transaction_des,
                                    buttons: ["OK"]
                                  });
                                  alert.present();
                                  // this.navCtrl.pop();
                                  this.navCtrl.setRoot(MenuPage);
                                });
                              } else {
                                this.error_msg = this.responseData.transaction_des;
                                this.success_msg = "";
                                loading.dismiss();
                              }
                            });
                        },
                        error => {
                          console.log(
                            "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                          );
                          loading.dismiss();
                          this.storage.set(this.globals.role_id, null);
                          this.navCtrl.setRoot(MenuPage);
                        }
                      );
                    }
                  }
                );
              });
            }
          }
        ]
      });
      alert.present();
      return false;
    }

    /*  this.messageService.orderTools(jsonData).subscribe(data => {
               this.responseData = data;
               if (this.responseData.transaction_status == 1) {
                 //this.err_msg = "";
                 //this.success_msg = data.transaction_des;
                 const alert = this.alertCtrl.create({
                   title: 'Order Tools',
                   subTitle: this.responseData.transaction_des,
                   buttons: ['OK']
                 });
                 alert.present();
                 // this.navCtrl.pop();
                 this.navCtrl.setRoot(MenuPage);
               } else {
                 this.error_msg = this.responseData.transaction_des;
                 this.success_msg = "";
               }         
       },(error: HttpErrorResponse) => {
           console.log("Error from http header"+error);
           console.log("Error from http header status text"+error.statusText);        
           if(error.statusText == "Unauthorized")
           {
             console.log("code matched and call the api");
             this.refreshPromise().then(success => {             
                   this.messageService.orderTools(jsonData).subscribe(data => {
                     this.responseData = data;
                     if (this.responseData.transaction_status == 1) {
                       //this.err_msg = "";
                       //this.success_msg = data.transaction_des;
                       const alert = this.alertCtrl.create({
                         title: 'Order Tools',
                         subTitle: this.responseData.transaction_des,
                         buttons: ['OK']
                       });
                       alert.present();
                       // this.navCtrl.pop();
                       this.navCtrl.setRoot(MenuPage);
                     } else {
                       this.error_msg = this.responseData.transaction_des;
                       this.success_msg = "";
                     }
                 });
               },(error) => {              
                   console.log("error at Calling the Refresh Token"+JSON.stringify(error));                       
                   this.storage.set(this.globals.role_id, null);              
                   this.navCtrl.push(HomePage);
             });                        
           }
         },
         () => {
           console.log("() block");        
         }      
     ); */
  }
  // end
  // this.messageService.orderTools(jsonData).subscribe(data => {
  //   this.responseData = data;
  //   if (this.responseData.transaction_status == 1) {
  //     //this.err_msg = "";
  //     //this.success_msg = data.transaction_des;
  //     const alert = this.alertCtrl.create({
  //       title: 'Order Tools',
  //       subTitle: this.responseData.transaction_des,
  //       buttons: ['OK']
  //     });
  //     alert.present();
  //     // this.navCtrl.pop();
  //     this.navCtrl.setRoot(MenuPage);
  //   } else {
  //     this.error_msg = this.responseData.transaction_des;
  //     this.success_msg = "";
  //   }
  // });

  ionViewDidLoad() {
    console.log("ionViewDidLoad OrderToolsPage");
  }

  getWhDetails(whs) {
    for (var i = 0; i < this.whList.length; i++) {
      if (this.whList[i].wh_id == whs) {
        this.address1 = this.whList[i].address1;
        this.address2 = this.whList[i].address2;
        this.city = this.whList[i].address3;
        this.state = this.whList[i].address4;
        this.pincode = this.whList[i].pin_code;
      }
    }
  }
}
