import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import * as moment from "moment";

@IonicPage()
@Component({
  selector: "page-extend-date",
  templateUrl: "extend-date.html"
})
export class ExtendDatePage {
  globals: Globals;
  tool = {};
  order_no = {};
  tool_order_id = {};

  returnDate: any;
  extendedDate: any;

  returnDateForRequest: any;
  extendDateRequest: any;

  remarks: string;
  ssoId = "";
  error_msg: string;
  min: any;

  responseData: any;
  value: number;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.globals = Globals.getInstance();
    var order_no = navParams.get("toolInfo");
    this.min = new Date().toISOString();

    this.order_no = order_no.order_info.order_number;
    this.tool_order_id = order_no.order_info.tool_order_id;
    console.log("1");
    this.returnDateForRequest = order_no.order_info.return_date;
    console.log("2");

    this.returnDate = order_no.order_info.return_date;
    this.returnDate = this.formatDate(this.returnDate);
    console.log("3");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      console.log("4");
      this.ssoId = sso_id;
    });
  }

  formatDate(date) {
    var d = new Date(date);
    var month = "" + (d.getMonth() + 1);
    var day = "" + d.getDate();
    var year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    return [year, month, day].join("-");
  }

  extendDate() {
    // console.log(this.returnDate.slice(0,10));

    /*  if(this.returnDate>this.extendedDate){
      console.log("success ahead")
    } */
    console.log("remarks" + this.remarks);
    if (this.extendedDate === undefined || this.extendedDate == "") {
      this.error_msg = "Invalid Extended Date or Please fill extend date ";
    } else if (this.remarks == "" || this.remarks === undefined) {
      this.error_msg = "Please enter Remarks";
    } else {
      this.error_msg = "";
      var jsonData = JSON.stringify({
        sso_id: this.ssoId,
        tool_order_id: this.tool_order_id,
        order_number: this.order_no,
        request_date: this.returnDateForRequest,
        new_return_date: this.extendedDate,
        remarks: this.remarks,
        ac_submit: "1",
        device_type: this.globals.deviceName
      });
      console.log(jsonData);

      let alert = this.alertCtrl.create({
        title: "Submission of Tool Extend Date",
        message: "Are you sure you want to submit",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Submit",
            handler: () => {
              console.log("Submit clicked");
              alert.dismiss().then(() => {
                let loading = this.loadingCtrl.create({
                  content: "Please wait until the order gets displayed ....."
                });
                loading.present();
                this.messageService
                  .extendDate(jsonData)
                  .then(res => {
                    console.log("*****extend date response data********");
                    console.log(data);
                    if (res.status === 401) {
                      throw new Error("Unauthorized");
                    }
                    var data = JSON.parse(res);
                    loading.dismiss();
                    this.presentAlert("Extend Date", data["transaction_des"]);
                    console.log("*****extend date request data********");
                  })
                  .catch((e: any) => {
                    let error = e.message;
                    console.log("Error from http header" + error);
                    console.log("Error from http header status text" + error);
                    if (error == "Unauthorized") {
                      console.log("code matched and call the api");
                      this.refreshPromise().then(
                        success => {
                          this.messageService.extendDate(jsonData).then(res => {
                            console.log(
                              "*****extend date response data********"
                            );
                            console.log(data);
                            var data = JSON.parse(res);
                            this.presentAlert(
                              "Extend Date",
                              data["transaction_des"]
                            );
                            loading.dismiss();
                            console.log(
                              "*****extend date request data********"
                            );
                          });
                        },
                        error => {
                          console.log(
                            "error at Calling the Refresh Token" +
                              JSON.stringify(error)
                          );
                          this.storage.set(this.globals.role_id, null);
                          this.navCtrl.push(HomePage);
                        }
                      );
                    }
                  });
              });
            }
          }
        ]
      });
      alert.present();
    }
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.setRoot(HomePage);
    // this.navCtrl.pop();
    //this.navCtrl.push(ExtendDatePage,{});
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad ExtendDatePage");
  }
}
