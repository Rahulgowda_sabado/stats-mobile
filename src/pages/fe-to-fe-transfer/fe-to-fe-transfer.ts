import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { MenuPage } from "../menu/menu";

@IonicPage()
@Component({
  selector: "page-fe-to-fe-transfer",
  templateUrl: "fe-to-fe-transfer.html"
})
export class FeToFeTransferPage {
  shownGroup = null;
  globals: Globals;
  ssoId = "";
  feToFeDetails = [];
  isItemPresent = true;
  responseData: any;

  country_id = "";
  countries_list = "";
  countries_list_data = "";
  loading: any;
  normalData: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.Fe2FeData();
    });
  }
  Fe2FeData() {
    this.messageService
      .feToFeTransfer(this.ssoId, this.country_id)
      .then(data => {
        //var res =  JSON.stringify(data);
        if (data.status === 401) {
          throw new Error("Unauthorized");
        }
        this.normalData = JSON.parse(data);
        this.responseData = this.normalData.response_data;
        this.countries_list = this.normalData.countries_list;
        console.log("list" + this.countries_list);
        this.countries_list_data = this.normalData.countries_list_data;

        // this.feToFeDetails = this.feToFeDetails;
        console.log("fe to fe transfer:" + JSON.stringify(this.responseData));
        if (this.responseData.length == 0) {
          this.isItemPresent = false;
        }
        console.log(this.feToFeDetails);
        console.log("fe to fe transfer");
        this.loading.dismiss();
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .feToFeTransfer(this.ssoId, this.country_id)
                .then(data => {
                  this.normalData = JSON.parse(data);
                  this.responseData = this.normalData.response_data;
                  this.countries_list = this.normalData.countries_list;
                  console.log("list" + this.countries_list);
                  this.countries_list_data = this.normalData.countries_list_data;

                  console.log("fe to fe transfer");
                  if (this.responseData.length == 0) {
                    this.isItemPresent = false;
                  }
                  console.log(this.responseData);
                  console.log("fe to fe transfer");
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(MenuPage);
            }
          );
        }
      });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad FeToFeTransferPage");
  }
  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  onClick(status, feToFe: any) {
    let alert = this.alertCtrl.create({
      title: "Submission of FE to FE Transfer Request",
      message: "Are you sure you want to submit",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the Request gets displayed ....."
              });
              loading.present();
              this.messageService
                .feToFeTransferPost(jsonData)
                .then(data => {
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }

                  console.log(JSON.stringify(data));
                  //var res=JSON.parse(data)
                  this.responseData = JSON.parse(data);
                  if (this.responseData.transaction_status == 1) {
                    loading.dismiss();
                    this.presentAlert(
                      "FE to FE Transfer Request",
                      "Initialized Succesfully"
                    );
                  } else if (this.responseData.transaction_status == 2) {
                    loading.dismiss();
                    this.presentAlert(
                      "FE to FE Transfer Request",
                      "Transfer Declined"
                    );
                  }
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error === "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .feToFeTransferPost(jsonData)
                          .then(data => {
                            this.responseData = JSON.parse(data);
                            if (this.responseData.transaction_status == 1) {
                              loading.dismiss();
                              this.presentAlert(
                                "FE to FE Transfer Request",
                                "Initialized Succesfully"
                              );
                            } else if (
                              this.responseData.transaction_status == 2
                            ) {
                              loading.dismiss();
                              this.presentAlert(
                                "FE to FE Transfer Request",
                                "Transfer Declined"
                              );
                            }
                          });
                      },
                      error => {
                        loading.dismiss();
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                });
            });
          }
        }
      ]
    });
    alert.present();
    console.log(status);
    console.log(feToFe);
    status = "" + status;

    var jsonData = JSON.stringify({
      sso_id: this.ssoId,
      return_type_id: feToFe.return_type_id,
      return_order_id: feToFe.return_order_id,
      approve: status,
      rto_id: feToFe.rto_id,
      admin_remarks: "NA",
      fe1_tool_order_id: feToFe.fe1_tool_order_id,
      order_status_id: feToFe.order_status_id,
      fe2_tool_order_id: feToFe.fe2_tool_order_id,
      device_type: this.globals.deviceName
    });
  }

  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(FeToFeTransferPage, { country_id: country_id });
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.push(HomePage);
  }
}
