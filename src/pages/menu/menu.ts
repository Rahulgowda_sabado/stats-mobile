import { Component, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  MenuController,
  Platform
} from "ionic-angular";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { Response } from "@angular/http";

import "rxjs/add/operator/map";
import { ShippingPage } from "../shipping/shipping";
import { FeordersPage } from "../feorders/feorders";
import { FemyordersPage } from "../femyorders/femyorders";
import { AdminApprovalPage } from "../admin-approval/admin-approval";
import { AcknowledgementPage } from "../acknowledgement/acknowledgement";
import { NotificationPage } from "../notification/notification";
import { FereturntoolPage } from "../fereturntool/fereturntool";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { ScancodePage } from "../scancode/scancode";
import { TransactionCountryPage } from "../transaction-country/transaction-country";

@Component({
  selector: "page-menu",
  templateUrl: "menu.html"
})
export class MenuPage {
  globals: Globals;
  role: string;
  alerts: any;
  alert: any;
  sso: string;
  loop_data: any;
  authorization_val: string;
  refresh_token: string;
  menu_items: string[];
  imagePath: string;
  userInfo = [];
  userSSOId: any = "";
  userName: any = "";
  menu_sso_id: any = "";
  menu_sso_name: any = "";
  showUserDetails: boolean = true;
  noramlData: any;
  responseData: any;
  scannedtool = [];
  country_id: any;
  ssoId: any;
  //hard_coded_access_token ='p0DRWzBDjbOmwRTaZgs9ClNkgUTF' ;
  //hard_coded_refresh_token = 'xLI5y4R4NzSXRiZDkMJvUeodu8d1wTtZD0rtQuTEf3' ;
  constructor(
    public menu: MenuController,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private http: HttpClient,
    public navParams: NavParams,
    public navCtrl: NavController,
    public messageService: MessageServiceProvider,
    private platform: Platform
  ) {
    let loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loading.present();

    this.http = http;
    this.globals = Globals.getInstance();
    this.userInfo = this.navParams.get("userInfo");

    console.log("USer Final Info" + this.userInfo);
    // console.log("Inside menu bar");
    // console.log(this.userInfo);
    // alert("hello"+this.userInfo);
    // alert("user info is not null before");
    // console.log("Length of Object"+Object.keys(this.userInfo).length);
    // alert("Length of Object alert"+Object.keys(this.userInfo).length);
    //console.log(this.userInfo);



    // if (this.userInfo == null) {
    //   console.log("user info is null");
    //   this.showUserDetails = false;
    // } else {
    //alert("user info is not null");
    console.log("user info is not null");
    this.storage.get(this.globals.sso_id).then(ssoid => {
      this.menu_sso_id = ssoid;
      console.log("SSO ID For View Page" + ssoid);
    });
    this.storage.get(this.globals.sso_name).then(ssoname => {
      this.menu_sso_name = ssoname;
      console.log("SSO Name For View Page" + ssoname);
    });
    this.storage.get(this.globals.authorization_token).then(authorization => {
      this.authorization_val = authorization;
      // tesing access token expired by uncomenting the hard coded access value and below 1 line
      //this.globals.setAccess(this.hard_coded_access_token);
      this.globals.setAccess(this.authorization_val);
      console.log("authorization_val ID at Final" + this.authorization_val);
    });
    this.storage.get(this.globals.refresh_token).then(refresh => {
      this.refresh_token = refresh;
      console.log("Refresh ID at Final" + this.refresh_token);
      this.globals.setRefresh(this.refresh_token);
    });

    this.storage.get(this.globals.country_id).then(country_id => {
      this.country_id = country_id;
      console.log("Country ID at Final" + this.country_id);
      this.globals.setRefresh(this.country_id);
    });
    // }
    this.storage.get(this.globals.role_id).then(roleid => {
      this.role = roleid;
      console.log("role ID at imagePath" + this.role);
      //alert("Role Id at image path alert -"+this.role);
      this.imagePath = this.globals.getImagePath(this.role);
      this.menu_items = this.globals.getMenuItems(this.role);
      console.log(this.menu_items);
    });

    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.messageService
        .notifCount(sso_id)
        .then(res => {
          if (res.status === 401) {
            throw new Error("Unauthorized");
          }
          loading.dismiss();
          let data = JSON.parse(res);
          this.globals.setNotifCount(data["alert_count"]);
        })
        .catch((e: any) => {
          let error = e.message;
          if (error === "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService.notifCount(sso_id).then((data: any) => {
                  loading.dismiss();

                  let data1 = JSON.parse(data);
                  let count = data1["alert_count"];
                  this.globals.setNotifCount(count);
                });
              },
              error => {
                loading.dismiss();
                //alert('errorrr');
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                //alert("error at Calling the Refresh Token"+JSON.stringify(error));
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });

      // end
    });
  }
  ionViewWillEnter() {
    // 3 is logistic
    // 2 is fe
    // 1 is admin
    this.storage.get(this.globals.role_id).then(roleid => {
      if (roleid == 1 || roleid == 4) {
        this.menu.enable(true, "menu1");
        this.menu.enable(false, "menu2");
        this.menu.enable(false, "menu3");
      } else if (roleid == 2 || roleid == 5) {
        this.menu.enable(false, "menu1");
        this.menu.enable(true, "menu2");
        this.menu.enable(false, "menu3");
      } else {
        this.menu.enable(false, "menu1");
        this.menu.enable(false, "menu2");
        this.menu.enable(true, "menu3");
      }
    });
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  itemClick(item) {
    console.log(item.id);
    if (item.id == 1) {
      let loading = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loading.present();
      this.storage.get(this.globals.sso_id).then(sso_id => {
        this.ssoId = sso_id;
        var jsonData = JSON.stringify({
          sso_id: sso_id
        });
        this.messageService
          .get_countries_by_sso_idM(jsonData)
          .then(data => {
            if (data.status === 401) {
              throw new Error("Unauthorized");
            }
            this.responseData = JSON.parse(data);
            if (this.responseData.transaction_status == "1") {
              loading.dismiss();
              this.navCtrl.push(
                TransactionCountryPage,
                this.responseData.transaction_des
              );
            } else {
              loading.dismiss();
              this.globals.transaction_country = "";
              this.navCtrl.push(FeordersPage);
            }
          })
          .catch((e: any) => {
            var error = e.message;
            console.log("Error from http header" + error);
            console.log("Error from http header status text" + error);
            if (error == "Unauthorized") {
              console.log("code matched and call the api");
              this.refreshPromise().then(
                success => {
                  this.messageService.confirmTools(jsonData).then(data => {
                    this.responseData = JSON.parse(data);
                  });
                },
                error => {
                  console.log(
                    "error at Calling the Refresh Token" + JSON.stringify(error)
                  );
                  this.storage.set(this.globals.role_id, null);
                  this.navCtrl.push(HomePage);
                  if (this.responseData.transaction_status == "1") {
                    loading.dismiss();
                    this.navCtrl.push(
                      TransactionCountryPage,
                      this.responseData.transaction_des
                    );
                  } else {
                    loading.dismiss();
                    this.globals.transaction_country = "";
                    this.navCtrl.push(FeordersPage);
                  }
                }
              );
            }
          });
      });
      /*  let transaction_status = "1";
      if (transaction_status == "1") this.navCtrl.push(TransactionCountryPage);
      else this.navCtrl.push(FeordersPage); */
    } else if (item.id == 2) {
      this.navCtrl.push(AcknowledgementPage, { alertType: 0 });
    } else if (item.id == 3) {
      this.navCtrl.push(FereturntoolPage, { alertType: 3 });
    } else if (item.id == 4) {
      this.navCtrl.push(FemyordersPage);
    } else if (item.id == 5) {
      // this.navCtrl.push(ShippingPage,{item:item});
      this.navCtrl.push(NotificationPage);
    } else if (item.id == 6) {
      this.navCtrl.push(ScancodePage, { scannedtool: this.scannedtool });
    } else if (item.id == 7) {
      this.navCtrl.push(AdminApprovalPage);
    }
  }
  // gotoErrorScreen(){
  //   //alert("");
  //   console.log("Sorry! You are not the STATS User.Please Contact Administrator");
  //   this.responseData = "Sorry! You are not the STATS User.Please Contact Administrator";
  //   //this.navCtrl.setRoot(MenuPage,{userInfo:this.userInfo});
  // }
}
