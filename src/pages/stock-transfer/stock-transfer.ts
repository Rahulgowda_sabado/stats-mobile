import { Component } from "@angular/core";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from "ionic-angular";
import { StockOrderDetailsPage } from "../stock-order-details/stock-order-details";
import { WtwStockTransferPage } from "../wtw-stock-transfer/wtw-stock-transfer";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse
} from "@angular/common/http";
import { HomePage } from "../home/home";
import { PartialStockOrderDetailsPage } from "../partial-stock-order-details/partial-stock-order-details";

@IonicPage()
@Component({
  selector: "page-stock-transfer",
  templateUrl: "stock-transfer.html"
})
export class StockTransferPage {
  globals: Globals;
  responseData: any;
  stockTransferList = [];
  ssoId = "";
  refreshResponse: any = {};

  country_id = "";
  loading: any;
  countries_list: any;
  countries_list_data: any;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public messageService: MessageServiceProvider
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.stockTransferData();
    });
  }
  stockTransferData() {
    this.messageService
      .stockTransferList(this.ssoId, this.country_id)
      .then(res => {
        console.log("stocktransfer");
        //var res1 =  JSON.stringify(res);
        if (res.status === 401) {
          throw new Error("Unauthorized");
        }
        var data = JSON.parse(res);

        this.stockTransferList = data["orderResults"];
        this.countries_list = data["countries_list"];
        this.countries_list_data = data["countries_list_data"];
        console.log(this.stockTransferList);
        if (this.stockTransferList.length == 0) {
          console.log("stock transfer is empty");
          //this.anyItem = false;
        }
        console.log("stocktransfer");
        this.loading.dismiss();
      })
      .catch((e: any) => {
        var error = e.message;
        console.log("Error from http header" + error);
        console.log("Error from http header status text" + error);
        if (error == "Unauthorized") {
          console.log("code matched and call the api");
          this.refreshPromise().then(
            success => {
              this.messageService
                .stockTransferList(this.ssoId, this.country_id)
                .then(res => {
                  console.log("stocktransfer");
                  var data = JSON.parse(res);
                  this.stockTransferList = data["orderResults"];
                  this.countries_list = data["countries_list"];
                  //console.log("list"+this.countries_list);
                  this.countries_list_data = data["countries_list_data"];
                  console.log(this.stockTransferList);
                  if (this.stockTransferList.length == 0) {
                    console.log("stock transfer is empty");
                    //this.anyItem = false;
                  }
                  console.log("stocktransfer");
                  this.loading.dismiss();
                });
            },
            error => {
              console.log(
                "error at Calling the Refresh Token" + JSON.stringify(error)
              );
              this.storage.set(this.globals.role_id, null);
              this.navCtrl.push(HomePage);
            }
          );
        }
      });
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }

  orderDetails(stockTransfer) {
    this.navCtrl.push(StockOrderDetailsPage, {
      tool_order_id: stockTransfer.tool_order_id
    });
  }

  refreshOrder(stocktransfer) {
    //console.log("stocktransfer"+stocktransfer);
    console.log("stocktransfer" + JSON.stringify(stocktransfer));
    let alert = this.alertCtrl.create({
      title: "Submission of Tool Availability",
      message: "Are you sure you want to recheck",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Submit",
          handler: () => {
            console.log("Submit clicked");
            alert.dismiss().then(() => {
              let loading = this.loadingCtrl.create({
                content: "Please wait until the order gets displayed ....."
              });
              loading.present();
              this.messageService
                .refreshForStockTransferOrder(
                  this.ssoId,
                  stocktransfer.tool_order_id
                )
                .then(data => {
                  if (data.status === 401) {
                    throw new Error("Unauthorized");
                  }
                  loading.dismiss();
                  this.responseData = JSON.parse(data);
                  if (this.responseData.transaction_status == 1) {
                    //this.err_msg = "";
                    //this.success_msg = data.transaction_des;
                    const alert = this.alertCtrl.create({
                      title: "Order Tools",
                      subTitle: this.responseData.transaction_des,
                      buttons: ["OK"]
                    });
                    alert.present();
                    // this.navCtrl.pop();
                    this.navCtrl.setRoot(HomePage);
                  }
                })
                .catch((e: any) => {
                  var error = e.message;
                  console.log("Error from http header" + error);
                  console.log("Error from http header status text" + error);
                  if (error === "Unauthorized") {
                    console.log("code matched and call the api");
                    this.refreshPromise().then(
                      success => {
                        this.messageService
                          .refreshForStockTransferOrder(
                            this.ssoId,
                            stocktransfer.tool_order_id
                          )
                          .then(data => {
                            loading.dismiss();
                            this.responseData = data;
                            if (this.responseData.transaction_status == 1) {
                              //this.err_msg = "";
                              //this.success_msg = data.transaction_des;
                              const alert = this.alertCtrl.create({
                                title: "Order Tools",
                                subTitle: this.responseData.transaction_des,
                                buttons: ["OK"]
                              });
                              alert.present();
                              // this.navCtrl.pop();
                              this.navCtrl.setRoot(HomePage);
                            }
                          });
                      },
                      error => {
                        console.log(
                          "error at Calling the Refresh Token" +
                            JSON.stringify(error)
                        );
                        this.storage.set(this.globals.role_id, null);
                        this.navCtrl.push(HomePage);
                      }
                    );
                  }
                }); // catach block end
            });
          }
        }
      ]
    });
    alert.present();
  }

  whDetails(stockTransfer) {
    console.log("for wh");
    this.navCtrl.push(WtwStockTransferPage, {
      tool_order_id: stockTransfer.tool_order_id
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad StockTransferPage");
  }

  viewCountryData(country_id) {
    this.country_id = country_id;
    this.navCtrl.push(StockTransferPage, { country_id: country_id });
  }

  presentAlert(titleInfo: string, subTitleInfo: string) {
    const alert = this.alertCtrl.create({
      title: titleInfo,
      subTitle: subTitleInfo,
      buttons: ["OK"]
    });
    alert.present();
    this.navCtrl.pop();
    this.navCtrl.push(StockTransferPage, {});
  }

  initiatePS(stockTransfer) {
    this.navCtrl.push(PartialStockOrderDetailsPage, {
      tool_order_id: stockTransfer.tool_order_id
    });
  }
}
