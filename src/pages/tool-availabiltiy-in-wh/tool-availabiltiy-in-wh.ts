import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { MessageServiceProvider } from "../../providers/message-service/message-service";
import { Globals } from "../../app/Globals";
import { Storage } from "@ionic/storage";
import { HomePage } from "../home/home";

@Component({
  selector: "page-tool-availabiltiy-in-wh",
  templateUrl: "tool-availabiltiy-in-wh.html"
})
export class ToolAvailabiltiyInWhPage {
  globals: Globals;
  warehouse: any = [];
  ssoId = "";
  error_msg = "";
  country_id = "";
  responseData: any;
  toolData: any;
  inventory: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public messageService: MessageServiceProvider,
    private storage: Storage
  ) {
    this.toolData = this.navParams.data;
    this.globals = Globals.getInstance();
    if (
      this.globals.transaction_country == "" ||
      this.globals.transaction_country == undefined
    ) {
      this.storage.get(this.globals.country_id).then(country_id => {
        this.country_id = country_id;
      });
    } else {
      this.country_id = this.globals.transaction_country.location_id;
    }
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.ssoId = sso_id;
      this.messageService
        .get_inventory_tools_availabilityM(
          this.toolData.tool_id,
          this.ssoId,
          this.country_id
        )
        .then(data => {
          //var res =  JSON.stringify(data);
          if (data.status === 401) {
            throw new Error("Unauthorized");
          }
          this.responseData = JSON.parse(data);
          this.inventory = this.responseData;
          /*this.tools.forEach(tool => {
      tool.qty = 5;
    });*/
          console.log(this.globals.selectedTools);
          console.log("***data list****");
          console.log(this.inventory.length);
          console.log("*****************");
          if (this.inventory.length == 0) {
            this.error_msg = "No quantity available";
          }
        })
        .catch((e: any) => {
          //alert('error'+e);
          var error = e.message;
          // alert("count:"+JSON.stringify(data))
          //alert('alerrrt'+error);
          console.log("Error from http header" + error);
          console.log("Error from http header status text" + error);
          if (error == "Unauthorized") {
            console.log("code matched and call the api");
            this.refreshPromise().then(
              success => {
                this.messageService
                  .get_inventory_tools_availabilityM(
                    this.toolData.tool_id,
                    this.ssoId,
                    this.country_id
                  )
                  .then(data => {
                    this.responseData = JSON.parse(data);
                    this.inventory = this.responseData;
                    if (this.inventory.length == 0) {
                      this.error_msg = "Search item not found";
                    }
                  });
              },
              error => {
                //alert('errorrr');
                console.log(
                  "error at Calling the Refresh Token" + JSON.stringify(error)
                );
                this.storage.set(this.globals.role_id, null);
                this.navCtrl.push(HomePage);
              }
            );
          }
        });
      //this.searchTools();
    });
    console.log(this.navParams);

    this.warehouse = [
      { name: "one", qty: "1" },
      { name: "two", qty: "2" },
      { name: "three", qty: "3" },
      { name: "four", qty: "4" },
      { name: "five", qty: "5" }
    ];
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ToolAvailabiltiyInWhPage");
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  public refreshPromise() {
    var self = this;
    return new Promise((resolve, reject) => {
      this.responseData = this.messageService
        .generateRefreshToken()
        .then(() => {
          resolve("succes");
          //return("succes");
        })
        .catch(e => {
          //return(e);
          reject(e);
        });
    });
  }
}
