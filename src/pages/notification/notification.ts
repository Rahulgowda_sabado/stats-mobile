import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { Globals } from '../../app/Globals';
import { Storage } from '@ionic/storage';
import { MessageServiceProvider } from '../../providers/message-service/message-service';
import { Alert } from '../model/alert.model';
import { AcknowledgementPage } from '../../pages/acknowledgement/acknowledgement';
import { FereturntoolPage } from '../../pages/fereturntool/fereturntool';
import { LogisticsAlertPage } from '../../pages/logistics-alert/logistics-alert';
import { StockTransferPage } from '../../pages/stock-transfer/stock-transfer';
import { FeToFeTransferPage } from '../../pages/fe-to-fe-transfer/fe-to-fe-transfer';
import { AddressChangePage } from '../../pages/address-change/address-change';
import { ExtendedDateApprovalPage } from '../../pages/extended-date-approval/extended-date-approval';
import { DueReturnPage } from '../../pages/due-return/due-return';
import { DueInDeliveriesPage } from '../../pages/due-in-deliveries/due-in-deliveries';
import { HomePage } from '../home/home';
import { HttpClient, HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';


import { calRequiredTools } from '../cal-required-tools/cal-required-tools';
import { LogFeReq } from '../log-fe-req/log-fe-req';
import { LogStReq } from '../log-st-req/log-st-req';
import { LogPickupReq } from '../log-pickup-req/log-pickup-req';
import { LogCalibrationReq } from '../log-calibration-req/log-calibration-req';
import { LogRepairReq } from '../log-repair-req/log-repair-req';
import { LogAckFeToolReq } from '../log-ackfetool-req/log-ackfetool-req';
import { LogAckWhToolReq } from '../log-ackwhtool-req/log-ackwhtool-req';



@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  globals: Globals;
  alertList: Alert[] = [];
  userid = "";
  role: string;
  roleType: String;
  normalData:any;
  menu_items = [];
  responseData:any;
  errMsg:any=false;
  constructor( private storage: Storage, public navCtrl: NavController,
    public loadingCtrl: LoadingController,
     public navParams: NavParams, public messageService: MessageServiceProvider) {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
    this.globals = Globals.getInstance();
    
    
    this.globals = Globals.getInstance();
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.messageService.notifDetails(sso_id).then(res =>{           
        loading.dismiss();   
        if(res.status === 401)
        {
          throw new Error("Unauthorized");
        }
        var data = JSON.parse(res);        
        let sample=data['alerts_list']
        let count=0;
        for(var i=0;i<Object.keys(sample).length;i++){
          if(sample[i].alert_count==0){
          }
          else{
            count++;
          }
        }
        if(count==0){
          this.errMsg=true;
        }        
        this.alertList = data['alerts_list'];          
        }).catch((e: any) => {
          var error = e.message;
            console.log("Error from http header"+error);                   
            if(error === "Unauthorized")
            {
              console.log("code matched and call the api");
              this.refreshPromise().then(success => {             
                  this.messageService.notifDetails(sso_id).then(res =>{  
                    var data = JSON.parse(res);  
                    this.alertList = data['alerts_list'];                        
                  });
                },(error) => {              
                    console.log("error at Calling the Refresh Token"+JSON.stringify(error));                       
                    this.storage.set(this.globals.role_id, null);              
                    this.navCtrl.push(HomePage);
              });                        
            }
          }     
      ); 
  })
}
public refreshPromise(){
  var self = this;
  return new Promise((resolve, reject) => {
     this.responseData = this.messageService.generateRefreshToken().then(()=>{
      resolve("succes");
      //return("succes");
     }).catch(e=>{
      //return(e);
      reject(e);
    });
  });
}

  clickedEvent(val) {
    console.log("clickedEvent hit");
    console.log(val);

    this.storage.get(this.globals.role_id).then((roleid) => {
      this.role = roleid;
      this.roleType = this.globals.getRoleByRoleId(this.role);
      console.log("Role Type ==> " + this.roleType);
      switch (val.alert_type) {
        case 1:
          if (this.roleType == "fe") {
            this.navCtrl.push(AcknowledgementPage, { alertType: 1 });
          } else if (this.roleType == "admin") {
            //should redirect to stock transfer page
            this.navCtrl.push(StockTransferPage, {});
          } else if (this.roleType == "log") {
            console.log("*****alert name*****");
            console.log(val.alert_name);
            console.log("**********");
            this.navCtrl.push(LogFeReq);
          }
          break;
        case 2:
          if (this.roleType == "fe") {
            this.navCtrl.push(AcknowledgementPage, { alertType: 2 });
          } else if (this.roleType == "admin") {
            //redirect to fe to fe request page
            this.navCtrl.push(FeToFeTransferPage);
          } else if (this.roleType == "log") {
            //this.navCtrl.push(LogisticsAlertPage, { alert: val.alert_type, headerMessage: val.alert_name });
            console.log("at case2");
            this.navCtrl.push(LogStReq);
          }
          break;
        case 3:
          if (this.roleType == "fe") {
            this.navCtrl.push(FereturntoolPage, { alertType: 3 });
          } else if (this.roleType == "admin") {
            //navigate to extend date approval page
            this.navCtrl.push(ExtendedDateApprovalPage);
          } else if (this.roleType == "log") {
            //this.navCtrl.push(LogisticsAlertPage, { alert: val.alert_type, headerMessage: val.alert_name });
            this.navCtrl.push(LogPickupReq);
          }
          break;
        case 4:
          if (this.roleType == "fe") {
            this.navCtrl.push(calRequiredTools, { alertType: 4 });
          } else if (this.roleType == "admin") {
            //navigate to address change page
            this.navCtrl.push(AddressChangePage);
          } else if (this.roleType == "log") {
            this.navCtrl.push(LogCalibrationReq);
          }
          break;
        case 5:
          //navigate to due in return tool
          if (this.roleType == "fe") {
            this.navCtrl.push(DueReturnPage);
          } else if (this.roleType == "admin") {
            //navigate to due in return tool
            this.navCtrl.push(DueReturnPage);
            //this.navCtrl.push(AdminAlertsPage, { alert: val.alert_type });
          } else if (this.roleType == "log") {
            this.navCtrl.push(LogRepairReq);
          }
          break;
        case 6:
          if (this.roleType == "fe") {
            
          } else if (this.roleType == "admin") {
            //navigate to due in deliveries page
            this.navCtrl.push(DueInDeliveriesPage);
          } else if (this.roleType == "log") {
            this.navCtrl.push(LogAckFeToolReq);
          }
          break;
        case 7:
          if (this.roleType == "fe") {

          } else if (this.roleType == "admin") {
            console.log("role type is admin");
          } else if (this.roleType == "log") {
            this.navCtrl.push(LogAckWhToolReq);
          }
          break;
      }


    });
  }
}

