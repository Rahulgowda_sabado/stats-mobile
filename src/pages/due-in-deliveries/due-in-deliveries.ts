import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { Globals } from '../../app/Globals';
import { Storage } from '@ionic/storage';
import { MessageServiceProvider } from '../../providers/message-service/message-service';
import { HttpClient, HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import { HomePage } from '../home/home';




@IonicPage()
@Component({
  selector: 'page-due-in-deliveries',
  templateUrl: 'due-in-deliveries.html',
})
export class DueInDeliveriesPage {

  shownGroup = null;
  globals: Globals;
  orders = [];
  pageType:string;
  responseData:any;

  country_id = '';
  countries_list = '';
  countries_list_data = '';
  loading:any;
  normalData:any;
  sso_id:any;

  constructor(public loadingCtrl:LoadingController,public navCtrl: NavController,private storage: Storage,public navParams: NavParams,public messageService: MessageServiceProvider) {
    this.loading = this.loadingCtrl.create({
      content:'Please wait...'
    });
    this.loading.present();
    this.globals = Globals.getInstance();
    this.country_id = this.navParams.get("country_id");
    this.storage.get(this.globals.sso_id).then(sso_id => {
      this.sso_id = sso_id;
      this.dueInDeliveries();
    })
  }
  dueInDeliveries()
  {
    this.messageService.dueInDelivery(this.sso_id,this.country_id).then(data => {
      //var res = JSON.stringify(data);
      if(data.status === 401)
      {               
        throw new Error("Unauthorized");         
      } 
    this.normalData = JSON.parse(data);
    this.responseData = this.normalData.response_data;
        this.countries_list = this.normalData.countries_list;
        this.countries_list_data = this.normalData.countries_list_data;
    

    console.log(" data for due in deliveries");
    console.log(data);
    console.log(" data for due in deliveries");
    this.orders = this.responseData;
    this.loading.dismiss();
   }    
    ).catch((e : any) =>{

    let error = e.message;
    console.log("Error from http header"+error);
    console.log("Error from http header status text"+error);        
    if(error == "Unauthorized")
    {
    console.log("code matched and call the api");
    this.refreshPromise().then(success => {             
        this.messageService.dueInDelivery(this.sso_id,this.country_id).then(data => {
          this.normalData = JSON.parse(data);
        this.responseData = this.normalData.response_data;
            this.countries_list = this.normalData.countries_list;
            this.countries_list_data = this.normalData.countries_list_data;
        
          console.log(" data for due in deliveries");
          console.log(data);
          console.log(" data for due in deliveries");
          this.orders = this.responseData;
          this.loading.dismiss();
        });
      },(error) => {              
          console.log("error at Calling the Refresh Token"+JSON.stringify(error));                       
          this.storage.set(this.globals.role_id, null);              
          this.navCtrl.push(HomePage);
    });                        
    }
    } );
  }

  viewCountryData(country_id)
  {
    this.country_id = country_id;
    console.log(country_id);
    this.navCtrl.push(DueInDeliveriesPage,{country_id:country_id});
  }

  public refreshPromise(){
    var self = this;
    return new Promise((resolve, reject) => {
       this.responseData = this.messageService.generateRefreshToken().then(()=>{
        resolve("succes");
        //return("succes");
       }).catch(e=>{
        //return(e);
        reject(e);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DueInDeliveriesPage');
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };


}
